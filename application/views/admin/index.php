
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Página de inicio</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Página de inicio</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3><?php echo $total_vh; ?></h3>

                  <p>Vehículos a la venta</p>
                </div>
                <div class="icon">
                  <i class="ion ion-model-s"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3><?php echo $total_vt_ap; ?></h3>

                  <p>Ventas realizadas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-cart"></i>
                </div>
                <a href="<?php echo base_url();?>/admin/venta" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3><?php echo $total_con; ?></h3>

                  <p>Concesionarios</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url();?>admin/con" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3><?php echo $total_vt_no_ap; ?></h3>

                  <p>Ventas canceladas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?php echo base_url();?>/admin/venta" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de vehículos publicados</h3>
              <div class="card-tools">
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-striped">
                <thead>
                <tr>
                  <th>Imagen</th>
                  <th>Marca</th>
                  <th>Modelo</th>                  
                  <th>Año</th>
                  <th>Kilometraje</th>
                  <th>Vendedor</th>
                  <th>Celular</th>
                  <th>Clientes interesados</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($listado_vh as $v): ?>
                    <?php if($v->EST == 1): ?>
                      <tr>
                        <td>
                          <img src="<?php echo base_url().$v->IMG;?>" alt="" width="100px">
                        </td>
                        <td><?php echo $v->MAR; ?></td>
                        <td><?php echo $v->MO; ?></td>
                        <td><?php echo $v->A; ?></td>
                        <td><?php echo $v->K; ?></td>
                        <td>
                          <?php echo $v->NOM." - ".$v->APE; ?>
                        </td>
                        <td><?php echo $v->TEL; ?></td>
                        <td>
                          <?php 
                            if($v->COU <= 0 && $v->NOTIF <= 0){ echo '<a href="#" class="badge bg-danger">No hay clientes interesados</a>';}else{ echo '<a href="#" class="badge bg-success"> Clientes interesados </a>';}
                           ?>
                        </td>
                        <td>
                          <a target="framename" href="<?php echo base_url();?>admin/con/car/perfil?id=<?php echo $v->ID; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                          -
                          <a href="#" onclick="modal_list_ofert('<?php echo $v->ID; ?>')" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Generar venta" ><i class="fas fa-shopping-cart"></i></a>
                          -
                          <a href="#" class="btn btn-warning" onclick="modal_des_act_p('<?php echo $v->ID; ?>')" data-toggle="tooltip" data-placement="top" title="Desactivar Publicación" ><i class="fas fa-window-close"></i></a>
                        </td>
                      </tr>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de vehículos registrados</h3>
              <div class="card-tools">
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>Imagen</th>
                  <th>Marca</th>
                  <th>Modelo</th>                  
                  <th>Año</th>
                  <th>Kilometraje</th>
                  <th>Vendedor</th>
                  <th>Celular</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($listado_vh as $v): ?>
                    <?php if($v->EST == 0): ?>
                      <tr>
                        <td>
                          <img src="<?php echo base_url().$v->IMG;?>" alt="" width="100px">
                        </td>
                        <td><?php echo $v->MAR; ?></td>
                        <td><?php echo $v->MO; ?></td>
                        <td><?php echo $v->A; ?></td>
                        <td><?php echo $v->K; ?></td>
                        <td>
                          <?php echo $v->NOM." - ".$v->APE; ?>
                        </td>
                        <td><?php echo $v->TEL; ?></td>
                        <td>
                          <a target="framename" href="<?php echo base_url();?>admin/con/car/perfil?id=<?php echo $v->ID; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                          -
                          <a href="#" class="btn btn-success" onclick="fun_act_p('<?php echo $v->ID; ?>')" data-toggle="tooltip" data-placement="top" title="Activar" ><i class="fas fa-check-square"></i></a>
                          -
                          <a href="#" class="btn btn-success" onclick="fun_oferta_G('<?php echo $v->ID; ?>','<?php echo $v->TEL; ?>','<?php echo $v->TEL; ?>','4','N/A')" data-toggle="tooltip" data-placement="top" title="Notificar vía whatsapp" ><i class="fab fa-whatsapp"></i></a>
                          -
                          <a href="#" class="btn btn-danger" onclick="modal_del_p('<?php echo $v->ID; ?>')" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fas fa-trash-alt"></i></a>
                        </td>
                      </tr>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
              
            </div>
            <!-- /.card-body -->
          </div>

        </div><!-- /.container-fluid -->
        
      </div>
      
      <!-- /.content -->   

      <div class="modal fade" id="list-ofert">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Listado de ofertas</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="card">
                  <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">Listado de ofertas</h3>
                    <div class="card-tools">
                    </div>
                  </div>
                  <div class="card-body">

                      <input type="hidden" name="auxT" id="auxT" value="1">                      

                      <form action="<?php echo base_url()?>admin/oferta/reg" method="post" id="form_anonimo">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputDocumento">Documento</label>
                                <input type="number" class="form-control" name="inputDocumento" id="inputDocumentoW" placeholder="Documento" required>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputNombre">Nombre</label>
                                <input type="text" class="form-control" name="inputNombre" id="inputNombreW" placeholder="Nombre" required>
                              </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputApellido">Apellido</label>
                                <input type="text" class="form-control" name="inputApellido" id="inputApellidoW" placeholder="Apellido">
                              </div></div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputCelular">Celular</label>
                                <input type="number" class="form-control" name="inputCelular" id="inputCelularW" placeholder="Celular" required>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputCiudad">Ciudad</label>
                                <input type="text" class="form-control" name="inputCiudad" id="inputCiudadW" placeholder="Ciudad">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputEmail">Email</label>
                                <input type="N/A" class="form-control" name="inputEmail" id="inputEmailW" placeholder="Email" required>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="valor">Valor de venta acordado</label>
                                <input type="number" class="form-control" name="valor" id="valor" placeholder="Valor de venta acordado" required>
                              </div>
                            </div>                            
                          </div>
                          <input type="hidden" name="vh" id="vh">
                          <br>
                          <button type="submit" class="btn btn-success">Generar venta</button>
                        </form>
                  </div>
                </div>                  
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 

      <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Desactivación de publicación</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <center>
                    <img src="<?php echo base_url();?>assets/public/logos/error.svg" height="150px" alt="" srcset="">
                  </center>
              
              <h3>Esta apunto de desactivar la publicación del vehículo, esta operación dejara de mostrar el vehículo y pasara a la lista de vehículos registrados, pero No publicados</h3>
              <center>
                <h3>¿está seguro de continuar?</h3>
              </center>
              <input type="hidden" name="id_v" id="id_v">
            </div>
            <div class="modal-footer justify-content-between">
                <a href="#" onclick="fun_des_p()" class="btn btn-info">Desactivar</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 

      <div class="modal fade" id="modal-danger">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar publicación</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="id_v" id="id_v">
              <h4>Se eliminará todos los datos e información concerniente a esta publicación, recuerde que esta operación no podrá se desecha una vez completada.</h4>
              <h5>¿Desea continuar? </h5>
              <a href="#" onclick="fun_del_p()" class="btn btn-danger">Eliminar</a>
            </div>
            <div class="modal-footer justify-content-between">            
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->      