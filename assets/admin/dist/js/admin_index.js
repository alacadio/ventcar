var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $("#form_anonimo").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando la venta, por favor espere un momento.",
      showConfirmButton: false,
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#example1").DataTable({
    responsive: true,
  });

  $("#example2").DataTable({
    responsive: true,
  });
});


function modal_list_ofert(id) {
  $("#vh").val(id);
  $("#list-ofert").modal();
}

function modal_des_act_p(id) {
  $("#id_v").val(id);
  $("#modal-lg").modal();
}

function modal_del_p(id) {
  $("#id_v").val(id);
  $("#modal-danger").modal();
}

function fun_act_p(id) {
  Swal.fire({
    icon: "info",
    title: "Activar",
    text:
      "Se está procesando la acción de activación  la publicación, por favor espere un momento",
      showConfirmButton: false,
  });
  $.ajax({
    url: base_url + "admin/post_act",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Activar",
          text: data.Mens,
          showConfirmButton: false,
        });

        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_oferta_G(nit, id, tel, url, e, nom) {
  var mens;
  url += "admin/con/car/perfil?id=" + id;
  if (e == 1) {
    mens =
      "Hola ventcar, he decidido aceptar la oferta ofrecida por el concesionario " +
      nom +
      " al vehículo " +
      url +
      ", por favor comuníquese con mi persona por este medio para ultimar detalles";
  } else if (e == 2) {
    mens =
      "¡¡¡Felicidades!!!, su oferta realizada al vehículo " +
      url +
      ", fue aceptada por el vendedor del vehículo, por favor comuníquese con nosotros por este medio para ultimar detalles";
  } else if (e == 3) {
    mens =
      "Hola ventcar, este vehículo me interesa " +
      url +
      ", ¡¡Agendemos una cita para checar el vehículo!!";
  } else if (e == 4) {
    mens =
      "Hola, para completar el registro, agendemos una cita para chequear el vehículo";
  }
  var whatsapp =
    "https://api.whatsapp.com/send?phone=" + 57 + tel + "&text=" + mens;
  window.open(whatsapp, "_blank");
  return false;
}

function fun_del_p() {
  const id = $("#id_v").val();
  Swal.fire({
    icon: "info",
    title: "Eliminar",
    text:
      "Se está procesando la eliminación de esta publicación, por favor espere un momento",
      showConfirmButton: false,
  });
  $.ajax({
    url: base_url + "admin/cons/del_pos",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Eliminar",
          text: data.Mens,
        });

        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}