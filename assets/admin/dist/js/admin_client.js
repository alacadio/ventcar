var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $(".select2bs4").select2({
    theme: "bootstrap4",
  });

  $("#regs_us_admin").submit(function (ev) {
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#act_us_amd").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text:
        "Se está actualizando la información del cliente, por favor espere...",
        showConfirmButton: false,
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

   $("#act_pos_cli").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text:
        "Se está actualizando la información del vehículo, por favor espere...",
        showConfirmButton: false,
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#example1").DataTable({
    responsive: true,
  });

  $("#example2").DataTable({
    responsive: true,
  });

  fun_get_info_db();

 $("#Año").change(function () {
    const año = $("#Año").val();
    if (año == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/marca",
      data: { fecha: año },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Marca").empty();
          $("#Marca").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Modelo").empty();
          $("#Modelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Marca, function (i, item) {
            var marca = item.F;
            $("#Marca").append(
              '<option value="' + marca + '">' + marca + "</option>"
            );
          });
          $("#Marca").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Marca").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/modelo",
      data: { fecha: año, marca: marca },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Modelo").empty();
          $("#Modelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Modelo, function (i, item) {
            $("#Modelo").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#Modelo").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Modelo").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    const modelo = $("#Modelo").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/version",
      data: { fecha: año, marca: marca, modelo: modelo },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Version, function (i, item) {
            $("#Version").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#Version").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Version").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    const modelo = $("#Modelo").val();
    const version = $("#Version").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    if (version == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/valor",
      data: { fecha: año, marca: marca, modelo: modelo, version: version },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          alert(data.Valor)
          $("#inputValorpromediodeventa").val(
            new Intl.NumberFormat("es-MX").format(data.Valor)
          );
          $("#inputMenorValorVenta").val(
            new Intl.NumberFormat("es-MX").format(data.menos)
          );
          $("#inputMejorvalordeventa").val(
            new Intl.NumberFormat("es-MX").format(data.mas)
          );
          $("#pro").val(data.Valor);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });



});

function modal_act_car(id) {
  $("#modal_act_car").modal();
  $.ajax({
    url: base_url + "admin/cons/info/car",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      if (!data.error) {
        let dato = [data.Mens.A, data.Mens.MA, data.Mens.MO, data.Mens.V];
        let input = ["Marca", "Modelo", "Version"];
        let url = ["list/marca", "list/modelo", "list/version"];
        fun_datos_año(data.Mens.A);
        for (let i = 0; i < 3; i++) {
          fun_set_datos_select(input[i], dato, url[i], i);
        }
        fun_datos_año(data.Mens.A);
        $("#inputKilometraje").val(data.Mens.K);
        $("#inputPlaca").val(data.Mens.P);

        $("#inputValorpromediodeventa").val(
          new Intl.NumberFormat("es-MX").format(data.Mens.VA)
        );
        $("#inputMenorValorVenta").val(
          new Intl.NumberFormat("es-MX").format(data.Mens.VA)
        );
        $("#inputMejorvalordeventa").val(
          new Intl.NumberFormat("es-MX").format(data.Mens.VA)
        );

        $("#pro").val(data.Valor);

        $("#inputValorVenta").val(data.Mens.VA);
        $("#id_v").val(data.Mens.ID);
        $("#radio_1_" + data.Mens.PI).prop("checked", true);
        $("#radio_2_" + data.Mens.IN_T).prop("checked", true);
        $("#radio_3_" + data.Mens.ME).prop("checked", true);
        $("#radio_4_" + data.Mens.CHO).prop("checked", true);
        $("#radio_5_" + data.Mens.COM).prop("checked", true);
        $("#radio_6_" + data.Mens.ACC).prop("checked", true);
        $("#inputDesc").val(data.Mens.DES);

        $("#id_img").empty();

        $.each(data.img, function (i, item) {
          $("#id_img").append(
            '<div class="col-sm-2 product-image-thumbs"><div class="post-thumbnail product-image-thumb"><a href="' +
              base_url +
              item.IMG +
              '" class="with-caption image-link"><img src="' +
              base_url +
              item.IMG +
              '" alt="" width="250px"></a></div></div>'
          );
        });

        $(".with-caption").magnificPopup({
          type: "image",
          closeBtnInside: false,
          mainClass: "mfp-with-zoom mfp-img-mobile",

          image: {
            verticalFit: true,
            titleSrc: function (item) {
              var caption = "Ventcar";

              return (
                caption +
                ' <img src="' +
                base_url +
                'assets/public/logos/originalhorizontalblanco.svg" width="50" height="28" />'
              );
            },
          },

          gallery: {
            enabled: true,
          },

          callbacks: {
            open: function () {
              this.wrap.on("click.pinhandler", ".pin-it", function (e) {
                // This part of code doesn't work on CodePen, as it blocks window.open
                // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
                /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);
            
                                
                                return false;*/
              });
            },
            beforeClose: function () {
              //this.wrap.off('click.pinhandler');
            },
          },
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function modal_act_us(id) {
  $('#modal-act').modal();
}

function new_client() {
  $("#modal-xl").modal();
}

function modal_del_cons(id) {
  $.ajax({
    url: base_url + "admin/cons/id",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        $("#id_con").val(data.Mens.DOC);
        $("#modal-danger").modal();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}


function fun_datos_año(param) {
  $.ajax({
    type: "GET",
    url: base_url + "admin/import/list",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#Año").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Fecha, function (i, item) {
          if (item.F == param) {
            $("#Año").append(
              "<option value=" + item.F + " selected>" + item.F + "</option>"
            );
          } else {
            $("#Año").append(
              "<option value=" + item.F + ">" + item.F + "</option>"
            );
          }
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_set_datos_select(input, dato, url, ii) {
  $.ajax({
    type: "GET",
    url: base_url + url,
    data: { fecha: dato[0], marca: dato[1], modelo: dato[2] },
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        console.log(dato[ii]);
        var d = "#" + input;
        $(d).append('<option value="0">- Seleccione una opción -</option>');
        if (ii == 0) {
          $.each(data.Marca, function (i, item) {
            fun_pintar(item.F, dato[1], d);
          });
        } else if (ii == 1) {
          $.each(data.Modelo, function (i, item) {
            fun_pintar(item.F, dato[2], d);
          });
        } else if (ii == 2) {
          $.each(data.Version, function (i, item) {
            fun_pintar(item.F, dato[3], d);
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_pintar(paramA, paramB, input) {
  if (paramA == paramB) {
    $(input).append(
      '<option value="' + paramA + '" selected>' + paramA + "</option>"
    );
  } else {
    $(input).append('<option value="' + paramA + '">' + paramA + "</option>");
  }
}

function fun_get_info_db() {
  $.ajax({
    type: "GET",
    url: base_url + "admin/import/list",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputVersion")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputMarca")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputModelo")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputAño").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Fecha, function (i, item) {
          $("#inputAño").append(
            "<option value=" + item.F + ">" + item.F + "</option>"
          );
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}