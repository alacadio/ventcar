<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

    /**
     * Listado concesionarios
     * 
     * @var array
     */
    protected $list_con;

    /**
     * Listado de vehiculos
     * 
     * @var array
     */
    protected $list_vh;

    /**
     * Listado ventas aprobadas
     * 
     * @var array
     */
    protected $list_vt_ap;

    /**
     * Listado ventas no aprobadas
     * 
     * @var array
     */
    protected $list_vt_no_ap;

    /**
     * datos del admin
     * 
     * @var object
     */
    protected $datos_admin;

    protected $cons;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('UserModel');
        $this->load->model('ConModel');
        $this->load->library('email');
        $this->load->library('excel');
        $this->load->helper(array('form', 'url'));

        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') != 1){
                redirect(base_url().'ingreso');
            }
        } else {
            redirect(base_url().'ingreso');
        }

        $this->list_con = $this->UserModel->get_list_con();
        $this->list_vh = $this->ConModel->get_list_to_sell();
        $this->list_vt_ap = $this->ConModel->get_list_invoce_complet();
        $this->list_vt_no_ap = $this->ConModel->get_list_vt_no_ap();
        $this->datos_admin = $this->UserModel->get_user_by_token($this->session->userdata('token'));
    }


    public function index()
    {
        $total_con = count($this->list_con);
        $total_vh = count($this->list_vh);
        $total_vt_ap = count($this->list_vt_ap);
        $total_vt_no_ap = count($this->list_vt_no_ap);

        $data = array(
            'total_con' =>  $total_con,
            'total_vh' => $total_vh,
            'total_vt_ap' => $total_vt_ap,
            'total_vt_no_ap' => $total_vt_no_ap,
            'listado_vh' => $this->list_vh,
            'my_datos' => $this->datos_admin,
            'script' => 'admin_index.js'
        );
        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/index");
        $this->load->view("layout/admin/footer");
    }

    public function concesionario()
    {

        $data = array(
            'my_datos' => $this->datos_admin,
            'list_con' => $this->list_con,
            'script' => 'admin_con.js'
        );

        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/concesionario");
        $this->load->view("layout/admin/footer");
    }

    public function concesionario_perfil()
    {
        $id = $this->input->get("id");

        $cons = $this->UserModel->get_user_by_document($id);
        $list_int = $this->ConModel->get_list_car_int($id);
        $list_vnt = $this->ConModel->get_list_car_com($id);
        $list_car = $this->ConModel->get_list_car_us($id);

        $data = array(
            'id' => $id, 
            'nombre' => $cons->NOM,
            'razon' => $cons->RAZON,
            'asesor' => $cons->RES,
            'co' => $cons,
            'my_datos' => $this->datos_admin,
            'list_int' => $list_int,
            'list_vnt' => $list_vnt,
            'cant_int' => count($list_int),
            'cant_vnt' => count($list_vnt),
            'list_car' => $list_car,
            'script' => 'admin_con_perfil.js'
        );

        $this->load->view("layout/admin/header", $data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/perfil_cons");
        $this->load->view("layout/admin/footer");
    }

    public function concesionario_carr()
    {
        $id = $this->input->get("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        $interes = $this->ConModel->get_list_con_inst($id);

        $data = array(
            'my_datos' => $this->datos_admin,
            'car' => $car,
            'img' => $img,
            'inte' => $interes,
            'script' => 'app.js'
        );

        $this->load->view("layout/admin/header", $data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/perfil_car");
        $this->load->view("layout/admin/footer");
    }

    public function cliente()
    {
        $lista_cliente = $this->UserModel->get_list_ven();
        $data = array(            
            'my_datos' => $this->datos_admin,
            'list_cliente' => $lista_cliente,
            'script' => 'admin_client.js'
        );

        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/cliente");
        $this->load->view("layout/admin/footer");
    }

    public function cliente_perfil()
    {
        $id = $this->input->get("id");

        $usuario = $this->UserModel->get_user_by_document($id);

        $car = $this->ConModel->get_list_car_us($usuario->DOC);

        $count_ven = 0;

        $count_p = 0;

        foreach ($car as $v) {
            
            if($v->EST == 2){
                $count_ven = $count_ven + 1;
            }

            if($v->EST == 1){
                $count_p = $count_p + 1;
            }

        }

        $data = array(
            'id' => $id, 
            'nombre' => $usuario->NOM,
            'apellido' => $usuario->APE,
            'ema' => $usuario->EMA,
            'cell' => $usuario->TEL,
            'car' => $car,
            'publicado' => $count_p,
            'vendido' => $count_ven,
            'my_datos' => $this->datos_admin,
            'script' => 'admin_client.js'
        );

        $this->load->view("layout/admin/header", $data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/perfil_cliente");
        $this->load->view("layout/admin/footer");
    }

    public function perfil()
    {
        $count_total_m = count($this->ConModel->get_list_vehiculo_marca());
        $count_total_v = count($this->ConModel->get_list_vehiculo());
        $count_total_ve = count($this->ConModel->get_list_vehiculo_version());
        $count_total_mo = count($this->ConModel->get_list_vehiculo_modelo());
        $list_pur = $this->ConModel->get_list_purg();
        $list_mens = $this->ConModel->get_list_mens();

        $data = array(            
            'my_datos' => $this->datos_admin,
            'vh' => $count_total_v,
            'ma' => $count_total_m,
            've' => $count_total_ve,
            'mo' => $count_total_mo,
            'list_mens' => $list_mens,
            'list_pur' => $list_pur,
            'script' => 'app.js'
        );

        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/configuracion");
        $this->load->view("layout/admin/footer");
    }

    public function venta()
    {
        $data = array(            
            'my_datos' => $this->datos_admin,
            'list_vent' => $this->ConModel->get_list_invoce_complet(),
            'list_cancel' => $this->ConModel->get_list_invoce_cancel(),
            'script' => 'admin_invoice.js'
        );

        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/venta");
        $this->load->view("layout/admin/footer");
    }

    public function invoice()
    {
        $id = $this->input->get('id');
        $admin = $this->UserModel->get_admin_by_invoice();
        $invoice = $this->ConModel->get_invoce_by_cod($id);
        $iva = $invoice->VAL * 0.19;
        $data = array(            
            'my_datos' => $this->datos_admin,
            'date_invo' => $admin,
            'invoice' => $invoice,
            'iva' => $iva,
            'script' => 'invoice.js'
        );

        $this->load->view("layout/admin/header",$data);
        $this->load->view("layout/admin/sidebar");
        $this->load->view("admin/invoice");
        $this->load->view("layout/admin/footer");
    }

    // --------------------------------------------------------
    // Funciones
    // --------------------------------------------------------

    public function get_info_user()
    {
        $doc = $this->input->post('doc');

        $user = $this->UserModel->get_user_by_document($doc);

        if($user){
            $data = array('error' => false, 'Mens' => $user);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Data');
            echo json_encode($data);
        }
    }

    public function get_info_car()
    {
        $id = $this->input->post("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        if($car){
            $data = array('error' => false, 'Mens' => $car, 'img' => $img);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe pero se presento un error al tratar de cargar la información, por favor trate más tarde');
            echo json_encode($data);
        }
    }

    public function reg_con()
    {
        $inputNIT = $this->input->post("inputNIT");
        $inputNombreConsecionaria = $this->input->post("inputNombreConsecionaria");
        $inputRazon = $this->input->post("inputRazon");
        $inputAsesor = $this->input->post("inputAsesor");
        $inputDireccion = $this->input->post("inputDireccion");
        $inputCiudad = $this->input->post("inputCiudad");
        $inputTelefono = $this->input->post("inputTelefono");
        $inputEmail = $this->input->post("inputEmail");
        $pass = $this->claveAleatoria();

        $result = $this->UserModel->get_user_by_email($inputEmail);

        $documento = $this->UserModel->get_user_by_document($inputNIT);

        if($documento){
            $data = array('error' => true, 'Mens' => 'El NIT ya existe, por favor verifique el numero ingresado');
            echo json_encode($data);
            return;
        }

        if($result){
            $data = array('error' => true, 'Mens' => 'La dirección de email ya se encuentra registrada, pro favor pruebe con otra');
            echo json_encode($data);
            return;
        }

        $data = array(
            'documento'     => $inputNIT, 
            'razon'         => $inputRazon,
            'nombre'        => $inputNombreConsecionaria,
            'responsable'   => $inputAsesor,
            'telefono'      => $inputTelefono,
            'email'         => $inputEmail,
            'direccion'     => $inputDireccion,
            'ciudad'        => $inputCiudad,
            'token'         => $this->get_token(),
            'tipo_us'       => 2,
            'estado'        => 1
        );

        $login = array(
            'id_login'  => $inputNIT, 
            'usuario'   => $inputNIT,
            'pass'      => md5($pass)
        );
                
        $result_set = $this->UserModel->set_registro($data);

        if($result_set){

            $result_set_login = $this->UserModel->set_insert_login($login);

            if($result_set_login){

                $bodyEmail = array('nombre' => $inputNombreConsecionaria, 'apellido' => '', 'contraseña' => $pass );

                $body = $this->load->view('emails/email_registro',$bodyEmail, true);

                $sendMAil = $this->send_email($inputEmail,$body,'Registro exitoso');

                if($sendMAil){
                    $data = array('error' => false, 'Mens' => 'El usuario ha sido registrado de forma exitosa, en breve recibirá un correo electrónico');
                    echo json_encode($data);
                }else{
                    $data = array('error' => false, 'Mens' => 'El usuario ha sido registrado de forma exitosa, en breve recibirá un correo electrónico'.$pass);
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => 'El usuario ha sido registrado de forma exitosa, pero los datos de ingreso no, por favor comuníquese con webmaster@ventcar.com');
                echo json_encode($data);
            }

        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo registrar el usuario en estos momentos, por favor vuelva a intentar más tarde');
            echo json_encode($data);
        }
    }

    public function act_cons()
    {
        $inputNIT2 = $this->input->post('inputNIT2');
        $inputNombreConsecionaria2 = $this->input->post('inputNombreConsecionaria2');
        $inputRazon2 = $this->input->post('inputRazon2');
        $inputAsesor2 = $this->input->post('inputAsesor2');
        $inputDireccion2 = $this->input->post('inputDireccion2');
        $inputCiudad2 = $this->input->post('inputCiudad2');
        $inputTelefono2 = $this->input->post('inputTelefono2');
        $inputEmail2 = $this->input->post('inputEmail2');

        $data = array(
            'razon'         => $inputRazon2,
            'nombre'        => $inputNombreConsecionaria2,
            'responsable'   => $inputAsesor2,
            'telefono'      => $inputTelefono2,  
            'email'         => $inputEmail2,
            'direccion'     => $inputDireccion2,
            'ciudad'        => $inputCiudad2
        );

        $result = $this->UserModel->set_update_user($inputNIT2,$data);

        if($result){
            $bodyEmail = array('nombre' => $inputNombreConsecionaria2);

            $body = $this->load->view('emails/email_datos_actualizados',$bodyEmail, true);

            $sendMAil = $this->send_email($inputEmail2,$body,'Datos actualizados');

            if($sendMAil){
                $data = array('error' => false, 'Mens' => '¡¡¡Datos actualizados de forma correcta!!! ');
                echo json_encode($data);
            }else{
                $data = array('error' => false, 'Mens' => '¡¡¡Datos actualizados de forma correcta!!! ');
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe, pero se presentó un error al tratar de actualizar los datos, por favor trate más tarde');
            echo json_encode($data);
        }

    }

    public function del_const()
    {
        $id = $this->input->post('id');
        $pa = $this->input->post('pa');

        $user = $this->UserModel->get_user_by_vh($id);
        
        if($pa == 1){
            $result = $this->UserModel->set_delete_user($id);
            if($result){

                $bodyEmail = array('nombre' => $user->NOM.' '. $user->APE);

                $body = $this->load->view('emails/emial_cuenta_eliminada',$bodyEmail, true);

                $sendMAil = $this->send_email($user->EMA,$body,'Cuenta eliminada');

                if($sendMAil){
                    $data = array('error' => false, 'Mens' => '¡¡¡Concesionario eliminado de forma correcta!!!');
                    echo json_encode($data);
                }else{
                    $data = array('error' => false, 'Mens' => '¡¡¡Concesionario eliminado de forma correcta!!!');
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => 'Disculpe, pero se presentó un error al tratar de eliminar los datos, por favor trate más tarde');
                echo json_encode($data);
            }
        }else{
            $datos = array('estado' => 0 );
            $result = $this->UserModel->set_update_user($id,$datos);
            if($result){
                $bodyEmail = array('nombre' => $user->NOM.' '. $user->APE);

                $body = $this->load->view('emails/email_cuenta_deshabilitada',$bodyEmail, true);

                $sendMAil = $this->send_email($user->EMA,$body,'Cuenta deshabilitada');

                $data = array('error' => false, 'Mens' => '¡¡¡Concesionario deshabilitado de forma correcta!!! ');
                echo json_encode($data);
            }else{
                $data = array('error' => true, 'Mens' => 'Disculpe, pero se presentó un error al tratar de deshabilitar los datos, por favor trate más tarde');
                echo json_encode($data);
            }
        }
    }

    public function act_us()
    {
        $doc = $this->input->post('doc');

        $datos = array('estado' => 1 );

        $result = $this->UserModel->set_update_user($doc,$datos);

        $user = $this->UserModel->get_user_by_document($doc);

        $bodyEmail = array('nombre' => $user->NOM.' '.$user->APE);

        $body = $this->load->view('emails/email_activado_usuario',$bodyEmail, true);

        $sendMAil = $this->send_email($user->EMA,$body,'Cuenta activada');

        if($result){
            if($sendMAil){
                $data = array('error' => false, 'Mens' => '¡¡¡Usuario habilitado de forma exitosa!!!');
                echo json_encode($data);
            }else{
                $data = array('error' => false, 'Mens' => '¡¡¡Usuario habilitado de forma exitosa!!!');
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe, pero se presentó un error al tratar de habilitar el usuario, por favor trate más tarde');
            echo json_encode($data);
        }
    }

    public function us_act()
    {
        $inputDocumento = $this->input->post('inputDocumento');
        $inputNombre = $this->input->post('inputNombre');
        $inputApellido = $this->input->post('inputApellido');
        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');
        $inputUser = $this->input->post('inputUser');
        $inputPass = $this->input->post('inputPass');

        $data = array(
           'nombre' => $inputNombre,
           'apellido' => $inputApellido,
           'telefono' => $inputCelular,
           'direccion' => $inputDireccion,
           'ciudad' => $inputCiudad,
           'email' => $inputEmail
        );

        $login = array();

        $user = $this->UserModel->get_user_by_document($inputDocumento);

        $result = $this->UserModel->set_update_user($inputDocumento, $data);

        if($result){
            if(!empty($_POST['inputPass'])){
                $inputPass = md5($inputPass);
                $login = array(
                    'usuario' => $inputUser,
                    'pass' => $inputPass
                );
            }else{
                $login = array(
                    'usuario' => $inputUser
                );
            }
            $result_l = $this->UserModel->set_update_login($inputDocumento, $login);
            if($result_l){
                $bodyEmail = array('nombre' => $inputNombre.' '.$inputApellido);

                $body = $this->load->view('emails/email_datos_actualizados',$bodyEmail, true);

                $sendMAil = $this->send_email($inputEmail2,$body,'Datos actualizados');

                if($sendMAil){
                    $data = array('error' => false, 'Mens' => "¡¡Datos del usuario actualizados de forma exitosa!!");
                    echo json_encode($data);
                }else{
                    $data = array('error' => false, 'Mens' => "¡¡Datos del usuario actualizados de forma exitosa!!");
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => "Se presente un error al actualizar los datos de ingreso al sistema");
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => "Disculpe, se presento un error al tratar de actualizar los datos, por favor intente más tarde");
            echo json_encode($data);
        }
    }

    public function amd_act()
    {
                                                
        $inputNit = $this->input->post('inputNit');
        $inputEmpresa = $this->input->post('inputEmpresa');
        $inputResponsable = $this->input->post('inputResponsable');
        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');
        $Razon = $this->input->post('Razon');
        $inputUser = $this->input->post('inputUser');
        $inputPass = $this->input->post('inputPass');

        $inputNitAct = $this->input->post('nit_act');

        $data = array(
           'nombre' => $inputEmpresa,
           'responsable' => $inputResponsable,
           'telefono' => $inputCelular,
           'direccion' => $inputDireccion,
           'ciudad' => $inputCiudad,
           'email' => $inputEmail,
           'documento' => $inputNit,
           'razon' => $Razon
        );

        $login = array();

        $user = $this->UserModel->get_user_by_document($inputNitAct);

        $result = $this->UserModel->set_update_amd($data);

        if($result){
            if(!empty($_POST['inputPass'])){
                $inputPass = md5($inputPass);
                $login = array(
                    'usuario' => $inputUser,
                    'pass' => $inputPass
                );
            }else{
                $login = array(
                    'usuario' => $inputUser
                );
            }
            $result_l = $this->UserModel->set_update_login($inputNit, $login);
            if($result_l){
                $data = array('error' => false, 'Mens' => "¡¡Datos del usuario actualizados de forma exitosa!!");
                echo json_encode($data);
            }else{
                $data = array('error' => true, 'Mens' => "Se presente un error al actualizar los datos de ingreso al sistema");
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => "Disculpe, se presento un error al tratar de actualizar los datos, por favor intente más tarde");
            echo json_encode($data);
        }
    }

    public function reg_client()
    {
        $inputDocumento = $this->input->post('inputDocumento');
        $inputNombre = $this->input->post('inputNombre');
        $inputApellido = $this->input->post('inputApellido');
        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta = $this->input->post('inputValorVenta');
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');
        $inputFile1 = $this->input->post('inputFile1');;

        $data = array(
            'documento' => $inputDocumento,
            'nombre' => $inputNombre,
            'apellido' => $inputApellido,
            'telefono' => $inputCelular,
            'direccion' => $inputDireccion,
            'ciudad' => $inputCiudad,
            'email' => $inputEmail,
            'tipo_us' => 3,
            'token' => $this->get_token(),
            'estado' => 1
        );


        $doc = $this->UserModel->get_user_by_emadoc($inputDocumento,$inputEmail);

        if($doc){
            if($doc->EMA == $inputEmail){
                $dato = array('error' => true, 'Mens' => 'La dirección de email ya se encuentra registrada, por favor trate con otra');
                echo json_encode($dato);
                return;
            }
            if($doc->DOC == $inputDocumento){
                $dato = array('error' => true, 'Mens' => 'El documento de usuario ya se encuentra registrado, por favor pruebe con otro');
                echo json_encode($dato);
                return;
            }
        }

        $result = $this->UserModel->set_registro($data);

        if($result){

            $clave = $this->claveAleatoria();

            $login = array(
                'id_login' => $inputDocumento, 
                'usuario' => $inputDocumento, 
                'pass' => md5($clave)
            );

            $emailData = array('nombre' => $inputNombre, 'apellido' => $inputApellido, 'contraseña' => $clave );

            $login = $this->UserModel->set_insert_login($login);

            if($login){

                $op_valor = 0;

                if($inputValorVenta != 0 || $inputValorVenta != null){
                    $op_valor = 1;
                }else{
                    $inputValorVenta = $inputValorpromediodeventa;
                }

                $vh = array(
                    'marca' => $inputMarca,
                    'modelo' => $inputModelo,
                    'version' => $inputVersion,
                    'year' => $inputAño,
                    'kilometraje' => $inputKilometraje,
                    'placa' => $inputPlaca,
                    'valor_venta' => $inputValorVenta,
                    'op_valor' => $op_valor,
                    'documento' => $inputDocumento,
                    'activo' => 1,
                    'pintura' => $radio1,
                    'interior' => $radio2,
                    'mecanico' => $radio3,
                    'accidente' => $radio6,
                    'choque' => $radio4,
                    'combustible' => $radio5,
                    'descripcion' => $inputDesc
                );

                $carro = $this->ConModel->set_register_car($vh);

                if($carro){
                    $ruta = "./assets/public/img/";

                    $this->load->library('upload');
                    
                    $uploadData = array();
                    $config = array();
                    $config['upload_path']          = $ruta;
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['overwrite']     = FALSE;

                    $count_file = count($_FILES['inputFile1']['name']);

                    for ($i=0; $i < $count_file; $i++) { 
                        
                        $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                        $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                        $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                        $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                        $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('userFile'))
                        {

                        }else{
                            $fileData = $this->upload->data();
                            $uploadData[$i]['name'] = $fileData['file_name'];
                            $uploadData[$i]['path'] = $fileData['file_path'];
                            $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                            $uploadData[$i]['id_vh'] = $carro;
                        }
                    }        

                    if(empty($uploadData)){
                        //Insert file information into the database
                        $dato = array('error' => false, 'Mens' => $uploadData);
                        echo json_encode($dato);
                        return;
                    }

                    if($count_file != count($uploadData)){
                        $dato = array('error' => false, 'Mens' => $uploadData);
                        echo json_encode($dato);
                        return;
                    }

                    $cont = 0;

                    $img = array();

                    for ($i=0; $i < count($uploadData); $i++) { 
                        $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                        $insert_img = $this->ConModel->set_register_img($img);
                        $cont = $cont + 1;
                    }

                    if($cont == count($uploadData)){
                        $body = $this->load->view('emails/email_registro',$emailData,true);

                        $sendEmail = $this->send_email($inputEmail,$body,'Registro exitoso');

                        if($sendEmail){
                            $dato = array('error' => false, 'Mens' => '¡¡¡Usuario registrado de forma exitosa!!! En breve recibirá un email.');
                            echo json_encode($dato);
                        }else{
                            $dato = array('error' => false, 'Mens' => '¡¡¡Usuario registrado de forma exitosa!!! En breve recibirá un email.');
                            echo json_encode($dato);
                        }
                    }else{
                        $dato = array('error' => false, 'Mens' => 'El usuario y el vehículo fueron registrado de forma exitosa, pero se presento un error al registrar las imágenes, por favor trate de ingresarlas al actualizar la información.');
                        echo json_encode($dato);
                    }
                }else{
                    $dato = array('error' => true, 'Mens' => 'El usuario fue registrado de forma exitosa, pero el vehículo NO, por favor proceda a eliminar el nuevo usuario y trate de registrar nuevamente o trate completar el registro del vehículo en la ventana de actualización información de la publicación. => '.$clave);
                    echo json_encode($dato);
                }

            }else{
                $dato = array('error' => true, 'Mens' => 'El usuario fue registrado de forma exitosa, pero no los datos de login ni del vehículo, por favor trate de eliminar el usuario y proceda a realizar el registro nuevamente, tenga en cuenta la información a registrar para no volver a presentar este error.');
                echo json_encode($dato);
            }

        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de registrar el usuario, por favor verifique la información suministrada y/o la conexión a internet.');
            echo json_encode($dato);
        }        

        /*
        if (!$this->upload->do_upload('inputFile1'))
        {
            $error = array('error' => $this->upload->display_errors());

            $dato = array('error' => false, 'Mens' => $error);
            echo json_encode($dato);
            return;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

            $dato = array('error' => false, 'Mens' => $data);
            echo json_encode($dato);
            return;
        }*/
    }

    public function post_des_act()
    {
        $id = $this->input->post('id');

        $data = array('activo' => 0);

        $result = $this->ConModel->set_update_car($id,$data);

        if($result){
            $envio = true;
            $dato = array('error' => false, 'Mens' => 'La publicación a sido desactivar exitosamente, se enviará un email al cliente para notificarle así mismo como un mensaje vía whatsapp');
            echo json_encode($dato);
        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, pero no se ha podido procesar la operación, por favor trate más tarde');
            echo json_encode($dato);
        }
    }

    public function post_act()
    {
        $id = $this->input->post('id');

        $data = array('activo' => 1);

        $result = $this->ConModel->set_update_car($id,$data);

        if($result){
            $envio = true;
            $dato = array('error' => false, 'Mens' => 'La publicación a sido activada exitosamente, se enviará un email al cliente para notificarle así mismo como un mensaje vía whatsapp');
            echo json_encode($dato);
        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, pero no se ha podido procesar la operación, por favor trate más tarde');
            echo json_encode($dato);
        }
    }

    public function delete_post()
    {
        $id = $this->input->post('id');

        $list_img = $this->ConModel->get_list_img_by_car($id);

        $count_img = count($list_img);

        $cont = 0;

        foreach ($list_img as $v) {
            if(!unlink($v->IMG)){                
                $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                echo json_encode($dato);
                return;
                break;
            }
            $cont = $cont + 1;
        }

        if($cont != $count_img){
            $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
            echo json_encode($dato);
            return;
        }

        $delete = $this->ConModel->set_delete_car($id);

        if($delete){
            $dato = array('error' => false, 'Mens' => 'Vehículo eliminado de forma exitosa, se enviará un email y un whatsapp al cliente para ser notificado');
            echo json_encode($dato);
        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de eliminar la información del vehículo, pero las imágenes de este han sido eliminadas');
            echo json_encode($dato);
        }
    }

    public function act_post()
    {
        $id = $this->input->post('id_v');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta = $this->input->post('inputValorVenta');
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');

        $data = array(
            'marca' => $inputMarca,
            'modelo' => $inputModelo,
            'version' => $inputVersion,
            'year' => $inputAño,
            'kilometraje' => $inputKilometraje,
            'placa' => $inputPlaca,
            'valor_venta' => $inputValorVenta,
            'pintura' => $radio1,
            'interior' => $radio2,
            'mecanico' => $radio3,
            'accidente' => $radio6,
            'choque' => $radio4,
            'combustible' => $radio5,
            'descripcion' => $inputDesc
        );

        $img = $this->ConModel->get_list_img_by_car($id);

        $ext = $_FILES['inputFile1']['name'][0];

        if($ext != null){
            $count_img = count($img);

            $cont = 0;

            foreach ($img as $v) {
                if(!unlink($v->IMG)){                
                    $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                    echo json_encode($dato);
                    return;
                    break;
                }
                $cont = $cont + 1;
            }

            $delete_img = $this->ConModel->set_detele_img($id);

            if($delete_img){
                $ruta = "./assets/public/img/";

                $this->load->library('upload');
                
                $uploadData = array();
                $config = array();
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']     = FALSE;

                $count_file = count($_FILES['inputFile1']['name']);

                for ($i=0; $i < $count_file; $i++) { 
                    
                    $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile'))
                    {

                    }else{
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['path'] = $fileData['file_path'];
                        $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                        $uploadData[$i]['id_vh'] = $id;
                    }
                }        

                if(empty($uploadData)){
                    //Insert file information into the database
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                if($count_file != count($uploadData)){
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                $cont = 0;

                $img = array();

                for ($i=0; $i < count($uploadData); $i++) { 
                    $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                    $insert_img = $this->ConModel->set_register_img($img);
                    $cont = $cont + 1;
                }

                $result = $this->ConModel->set_update_car($id,$data);

                if($result){

                    $user = $this->UserModel->get_user_by_vh($id);

                    $bodyEmail = array('nombre' => $user->NOM.' '.$user->APE);

                    $body = $this->load->view('emails/email_actualizacion_publicacion',$bodyEmail, true);

                    $sendMAil = $this->send_email($user->email,$body,'Publicacion actualizada');

                    $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                    echo json_encode($dato);
                }
            }
        }else{
            $result = $this->ConModel->set_update_car($id,$data);

            if($result){
                $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                echo json_encode($dato);
            }else{
                $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                echo json_encode($dato);
            }
        }
    }

    public function importxlxs()
    {
        $inputAñoI = $this->input->post('inputAñoI');
        $inputAñoF = $this->input->post('inputAñoF');

        $count = $this->ConModel->get_info_data_base()->CANT;

        if($count > 0){

            $delete = $this->ConModel->set_delete_fasecolda();

            if(!$delete){
                $error = array('error' => true, 'Mens' => 'El archivo seleccionado no es el correcto o simplemente no ha seleccionado un archivo');
                echo json_encode($error);
                return;
            }
        }

        if(isset($_FILES["file"]["name"])){

            $data = array();

            $datVal = array();

            $path = $_FILES["file"]["tmp_name"];

            $object = PHPExcel_IOFactory::load($path);

            $date_init = 0;
            $date_end = 0;
            $ini = [20];

            foreach ($object->getWorksheetIterator() as $v) {
                $highestRow = $v->getHighestRow();
                $highestColumn = $v->getHighestColumn();

                for($row = 2; $row < $highestRow; $row++){

                    
                    if($date_init == 0 && $v->getCellByColumnAndRow($row,1)->getValue() == $inputAñoI){
                        $ini[0] = $row;
                    }

                    if($date_end == 0 && $v->getCellByColumnAndRow($row,1)->getValue() == $inputAñoF) {
                        $ini[1] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'Potencia'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'TipoCaja'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'Cilindraje'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'Nacionalidad'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'AireAcondicionado'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'Combustible'){
                        $ini[] = $row;
                    }

                    if($v->getCellByColumnAndRow($row,1)->getValue() == 'Transmision'){
                        $ini[] = $row;
                        break;
                    }

                    
                }
            }

            $date_init = intval($ini[0]);
            $date_end = intval($ini[1]);
            //$date_end = $date_end + 1;
            $NumeroPotencia = intval($ini[2]);
            $NumeroTipoCaja = intval($ini[3]);
            $NumeroCilindraje = intval($ini[4]);
            $NumeroNacionalidad = intval($ini[5]);
            $NumeroCombustible = intval($ini[7]);
            $NumeroTransmision = intval($ini[8]);

            
            foreach ($object->getWorksheetIterator() as $v) {
                $highestRow = $v->getHighestRow();
                $highestColumn = $v->getHighestColumn();

                for($row = 2; $row < $highestRow; $row++){

                    for ($i=$date_init; $i < $date_end; $i++) {

                        $datVal[$i]['AÑO'] = $v->getCellByColumnAndRow($i,1)->getValue();
                        $datVal[$i]['VALOR'] = $v->getCellByColumnAndRow($i,$row)->getValue();
                    }

                    $data[$row-1]['MARCA'] = $v->getCellByColumnAndRow(0,$row)->getValue();
                    $data[$row-1]['modelo'] = $v->getCellByColumnAndRow(2,$row)->getValue();
                    $data[$row-1]['version'] = $v->getCellByColumnAndRow(3,$row)->getValue();
                    
                    $data[$row-1]['NumeroPotencia'] = $v->getCellByColumnAndRow($NumeroPotencia,$row)->getValue();
                    $data[$row-1]['NumeroTipoCaja'] = $v->getCellByColumnAndRow($NumeroTipoCaja,$row)->getValue();
                    $data[$row-1]['NumeroCilindraje'] = $v->getCellByColumnAndRow($NumeroCilindraje,$row)->getValue();
                    $data[$row-1]['NumeroNacionalidad'] = $v->getCellByColumnAndRow($NumeroNacionalidad,$row)->getValue();
                    $data[$row-1]['NumeroCombustible'] = $v->getCellByColumnAndRow($NumeroCombustible,$row)->getValue();
                    $data[$row-1]['NumeroTransmision'] = $v->getCellByColumnAndRow($NumeroTransmision,$row)->getValue();

                    $data[$row-1]['año'] = $datVal;
                }
            }

            $countData = count($data);

            $resultado = 1;
            for ($i=1; $i < $countData; $i++) { 
                
                $dato = array(
                    'marca' => $data[$i]['MARCA'],
                    'modelo' => $data[$i]['modelo'],
                    'version' => $data[$i]['version'],
                    'combustible' => $data[$i]['NumeroCombustible'],
                    'tipo_caja' => $data[$i]['NumeroTipoCaja'],
                    'cilindraje' => $data[$i]['NumeroCilindraje'],
                    'nacionalidad' => $data[$i]['NumeroNacionalidad'],
                    'transmision' => $data[$i]['NumeroTransmision']
                );
                
                $año = $data[$i]['año'];

                $vehiculo = $this->ConModel->set_register_vehiculo($dato);

                if($vehiculo){
                    for ($ii=$date_init; $ii < $date_end; $ii++) {                         
                        $valor = $año[$ii]['VALOR'];
                        if($valor != 0){
                            $añoValor = array(
                                'fecha' => $año[$ii]['AÑO'],
                                'valor' => $valor,
                                'id_vh' => $vehiculo
                            );

                            $valAño = $this->ConModel->set_register_fechavalor($añoValor);

                            if($valAño){

                            }else{
                                $error = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de cargar los datos del excel 1');
                                echo json_encode($error);
                                break;
                            }
                        }
                    }
                }else{
                    $error = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de cargar los datos del excel 2');
                    echo json_encode($error);
                    break;
                }
                $resultado += 1;
            }

            $depuracion = $this->ConModel->set_delete_fasecolda_depu();

            if($resultado != $countData || $depuracion <= 0){
                $error = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de cargar los datos del excel 3 => lista {'.$resultado.'} count =>{'.$countData.'}');
                echo json_encode($error);
            }else{
                $error = array('error' => false, 'Mens' => 'Datos cargados de forma correcta en la base de datos => '.$depuracion);
                echo json_encode($error);
            }
        }else{
            $error = array('error' => true, 'Mens' => 'El archivo seleccionado no es el correcto o simplemente no ha seleccionado un archivo');
            echo json_encode($error);
        }
    }

    public function act_mens()
    {
        $id = $this->input->post('id');
        $mens = $this->input->post('mens');

        if(!$mens){
            $data = array('error' => true, 'Mens' => 'Tiene que escribir algun texto!!!!');
            echo json_encode($data);
            return;
        }

        $data = array('mensaje' => $mens );

        $result = $this->ConModel->set_register_mens($id,$data);

        if($result){
            $data = array('error' => false, 'Mens' => 'Datos guardados de forma correcta');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de guardar los cambios, por favor intente mas tarde');
            echo json_encode($data);
        }
    }

    public function registro()
    {
        $id = $this->input->post('id');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta = $this->input->post('inputValorVenta');
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');
        $inputFile1 = $this->input->post('inputFile1');

        $op_valor = 0;

        if($inputValorVenta != 0 || $inputValorVenta != null){
            $op_valor = 1;
        }else{
            $inputValorVenta = $inputValorpromediodeventa;
        }

        $vh = array(
            'marca' => $inputMarca,
            'modelo' => $inputModelo,
            'version' => $inputVersion,
            'year' => $inputAño,
            'kilometraje' => $inputKilometraje,
            'placa' => $inputPlaca,
            'valor_venta' => $inputValorVenta,
            'op_valor' => $op_valor,
            'documento' => $id,
            'activo' => 0,
            'pintura' => $radio1,
            'interior' => $radio2,
            'mecanico' => $radio3,
            'accidente' => $radio6,
            'choque' => $radio4,
            'combustible' => $radio5,
            'descripcion' => $inputDesc,
            'user' => 1
        );
        
        $carro = $this->ConModel->set_register_car($vh);

        $count_file = count($_FILES['inputFile1']['name']);

        if($carro){
            if($count_file > 1){
                $ruta = "./assets/public/img/";

                $this->load->library('upload');
                
                $uploadData = array();
                $config = array();
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']     = FALSE;

                $count_file = count($_FILES['inputFile1']['name']);

                for ($i=0; $i < $count_file; $i++) { 
                    
                    $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile'))
                    {

                    }else{
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['path'] = $fileData['file_path'];
                        $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                        $uploadData[$i]['id_vh'] = $carro;
                    }
                }        

                if(empty($uploadData)){
                    //Insert file information into the database
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                if($count_file != count($uploadData)){
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                $cont = 0;

                $img = array();

                for ($i=0; $i < count($uploadData); $i++) { 
                    $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                    $insert_img = $this->ConModel->set_register_img($img);
                    $cont = $cont + 1;
                }
                
                if($cont == count($uploadData)){

                    $user = $this->UserModel->get_user_by_document($id);

                    $bodyEmail = array('nombre' => $user->NOM.' '.$user->APE);

                    $body = $this->load->view('emails/email_registro_vehiculo',$bodyEmail, true);

                    $sendMAil = $this->send_email($user->EMA,$body,'Registro de vehiculo');

                    if($sendMAil){
                        $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                        echo json_encode($dato);
                    }else{
                        $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                        echo json_encode($dato);
                    }
                }else{
                    $dato = array('error' => false, 'Mens' => 'El vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                    echo json_encode($dato);
                }
            }else{
                $imagen = array(
                    'name' => 'default.png',
                    'img' => './assets/public/logos/icono.svg',
                    'id_vh' => $carro
                );

                $insert_img = $this->ConModel->set_register_img($imagen);

                if($insert_img){
                    $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => false, 'Mens' => 'El vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                    echo json_encode($dato);
                }
                
            }
        }else{
            $dato = array('error' => true, 'Mens' => 'El vehículo fue registrado de forma exitosa, pero las imagenes NO, por favor proceda a eliminar el nuevo vehículo y trate de registrar nuevamente o trate completar el registro del vehículo en la ventana de actualización.');
            echo json_encode($dato);
        }
    }

    public function get_list_ofert()
    {
        $id = $this->input->post('id');

        $list_ofert = $this->ConModel->get_list_ofert_view($id);

        if($list_ofert){
            $data = array('error' => false, 'Mens' => $list_ofert);
            echo json_encode($data);
        }else{
            $data = array('error' => true);
            echo json_encode($data);
        }
    }

    public function set_ofert()
    {
        $id_vh = $this->input->post('vh');
        $id_of = $this->input->post('id');
        $token = $this->get_token();    
        $codigo_venta = $this->get_order_id();

        $data_interes = array(
            'ofert_gana' =>1,
            'nofit' => 1,
            'token' => $token,
            'codigo_venta' => $codigo_venta
        );

        $data_vh = array('activo' => 2);

        $data_cancel = array('ofert_gana' => 2 );

        $reg_cancel = $this->ConModel->set_regis_cancel_all_ofert($id_vh,$data_cancel);

        if($reg_cancel){

            $reg_act = $this->ConModel->set_update_car($id_vh,$data_vh);

            $reg_inte = $this->ConModel->set_update_ofert($id_of,$data_interes);

            if($reg_inte && $reg_act){

                $vendedor = $this->UserModel->get_user_by_vh($id_vh);

                $comprador = $this->UserModel->get_user_by_id_vh($id_of, $id_vh);

                $data_venta = array('id_vh_venta' => $id_vh,'token' => $token, 'codigo_venta' => $codigo_venta, 'vendedor' => $vendedor->DOC, 'concesionario' => $comprador->DOC, 'venta_ap' => 1);

                $reg_venta = $this->ConModel->set_register_venta($data_venta);

                if($reg_venta){
                    $data = array('error' => false, 'Mens' => '¡¡¡Felicidades!!! La venta ha sido registrada de forma correcta.');
                    echo json_encode($data);
                }else{
                    $data = array('error' => true, 'Mens' => 'Ups, disculpe, se presento un error al registrar la venta, por favor intente nuevamente desde el perfil del vehículo.');
                    echo json_encode($data);
                }                
            }else{
                $data = array('error' => true, 'Mens' => 'Ups, disculpe, se presento un error al tratar de registrar la oferta por el vehículo, trate de hacerlo más tarde desde el perfil del vehículo.');
                echo json_encode($data);
            }
        }
    }

    public function set_register_sale_user_anonimus()
    {
        $inputDocumento = $this->input->post('inputDocumento');
        $inputNombre = $this->input->post('inputNombre');
        $inputApellido = $this->input->post('inputApellido');

        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');

        $token = $this->get_token();    
        $codigo_venta = $this->get_order_id();
        $id_vh = $this->input->post('vh');
        $valor = $this->input->post('valor');
        
        $vendedor = $this->UserModel->get_user_by_vh($id_vh);

        $user = $this->UserModel->get_user_by_document($inputDocumento);

        if($vendedor->DOC == $user->DOC){
            $data = array('error' => true, 'Mens' => 'El numero de identificación es el mismo del vendedor, por favor verifique…');
            echo json_encode($data);
            return;
        }

        $data_us = array(
            'vt_usuario.documento' => $inputDocumento, 
            'vt_usuario.nombre' => $inputNombre,
            'vt_usuario.apellido' => $inputApellido,
            'vt_usuario.telefono' => $inputCelular,
            'vt_usuario.email' => $inputEmail,
            'vt_usuario.direccion' => $inputDireccion,
            'vt_usuario.ciudad' => $inputCiudad,
            'vt_usuario.tipo_us' => 4,
            'vt_usuario.estado' => 1
        );

        $data_interes = array(
            'concesionario' => $inputDocumento, 
            'vh' => $id_vh,
            'cant_oferta' => $valor,
            'ofert_activa' => 1,
            'ofert_gana' =>1,
            'nofit' => 1,
            'token' => $token,
            'codigo_venta' => $codigo_venta
        );

        $data_vh = array('activo' => 2);

        $data_cancel = array('ofert_gana' => 2 );


        if(!$user){
            $reg_us = $this->UserModel->set_registro($data_us);
            $user = $reg_us;
        }

        if($user){

            $reg_cancel = $this->ConModel->set_regis_cancel_all_ofert($id_vh,$data_cancel);

            if($reg_cancel){

                $reg_act = $this->ConModel->set_update_car($id_vh,$data_vh);

                $reg_inte = $this->ConModel->set_register_oferta($data_interes);

                if($reg_inte && $reg_act){

                    $data_venta = array('id_vh_venta' => $id_vh,'token' => $token, 'codigo_venta' => $codigo_venta, 'vendedor' => $vendedor->DOC, 'concesionario' => $inputDocumento, 'venta_ap' => 1);

                    $reg_venta = $this->ConModel->set_register_venta($data_venta);

                    if($reg_venta){
                        $url = base_url().'invoice?id='.$codigo_venta;

                        $bodyEmail = array('nombre' => $inputNombre.' '.$inputApellido, 'url' => $url );

                        $body = $this->load->view('emails/email_send_venta',$bodyEmail, true);

                        $sendMAil = $this->send_email($inputEmail,$body,'Factura de venta');

                        if($sendMAil){
                            $data = array('error' => false, 'Mens' => '¡¡¡Felicidades!!! La venta ha sido registrada de forma correcta.');
                            echo json_encode($data);
                        }else{
                            $data = array('error' => false, 'Mens' => '¡¡¡Felicidades!!! La venta ha sido registrada de forma correcta.');
                            echo json_encode($data);
                        }
                    }else{
                        $data = array('error' => true, 'Mens' => 'Ups, disculpe, se presento un error al registrar la venta, por favor intente nuevamente desde el perfil del vehículo.');
                        echo json_encode($data);
                    }                
                }else{
                    $data = array('error' => true, 'Mens' => 'Ups, disculpe, se presento un error al tratar de registrar la oferta por el vehículo, trate de hacerlo más tarde desde el perfil del vehículo.');
                    echo json_encode($data);
                }
            }
        }else{
            $data = array('error' => true, 'Mens' => 'Ups, disculpe, se presento un error al tratar de registrar el usuario, por favor trate nuevamente...');
            echo json_encode($data);
        }

    }

    //------------------------------------------------
    public function set_register_pur()
    {
        $marca = $this->input->post('txtMarca');

        $data = array('marca' => $marca);

        $result = $this->ConModel->set_register_pur($data);

        if($result){
            $data = array('error' => false, 'Mens' => 'Marca registrada para la purga!!');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo registrar la marca, por favor trate más tarde');
            echo json_encode($data);
        }
    }

    public function set_delete_marca_purg()
    {
        $marca = $this->input->post('id');

        $delete = $this->ConModel->set_delete_marca_purg($marca);

        if($delete){
            $data = array('error' => false, 'Mens' => 'Marca eliminada para la purga');
            echo json_encode($data);
        }else{
            $data = array('error' => false, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar la marca, por favor trate más luego.');
            echo json_encode($data);
        }
    }

    public function set_delete_marca_db()
    {
        $marca = $this->input->post('marca');

        $result = $this->ConModel->set_delete_marca_facecolda($marca);

        if($result){
            $data = array('error' => false, 'Mens' => 'Datos eliminados de forma exitosa');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar los registros de esta marca, por favor trate más luego.');
            echo json_encode($data);
        }
    }

    // --------------------------------------------------------
    public function set_cancel_venta()
    {
        $order_id = $this->input->post('cod_vt');
        $motivo = $this->input->post('inputMotivo');

        $invoice = $this->ConModel->get_invoce_by_cod($order_id);

        if(!$invoice){
            $data = array('error' => true, 'Mens' => 'Disculpe, pero no se pudo encontrar los datos asociados a esta factura, por favor intente más tarde.');
            echo json_encode($data);
            return;
        }

        $bodyEmail = array('nombre' => $invoice->COMP_NOM.' '.$invoice->COMP_APE);

        $body = $this->load->view('emails/email_send_cancel_venta',$bodyEmail, true);

        $sendMAil = $this->send_email($invoice->COMP_EMA,$body,'Venta cancelda');

        $delete_ofert = $this->ConModel->set_delete_vt_interes_by_orderId($order_id);

        $venta = array('venta_ap' => 2, 'motivo' => $motivo, 'fech_cancel' => date('Y-m-d'));

        if($delete_ofert){
            $result = $this->ConModel->set_update_venta($order_id,$venta);
            if($result){
                if($sendMAil){
                    $data = array('error' => false, 'Mens' => 'Venta cancelada, recuerde esta operación no puede ser desecha.');
                    echo json_encode($data);
                }else{
                    $data = array('error' => false, 'Mens' => 'Venta cancelada, recuerde esta operación no puede ser desecha.');
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de cancelar la venta, por favor intente más tarde.');
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => 'Esta operación no pudo ser iniciada, por favor, recargue la pagina e intente nuevamente.');
            echo json_encode($data);
        }

    }

    // --------------------------------------------------------
    public function get_data_invoice()
    {
        $cod = $this->input->post('cod');

        $invoice = $this->ConModel->get_invoce_by_cod($cod);

        if($invoice){
            $data = array('error' => false, 'Mens' => $invoice);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo obtener los datos del usuario para enviar el email, trate mas tarde');
            echo json_encode($data);
        }
    }

    public function send_invoice()
    {
        $cod = $this->input->post('cod');
        $email = $this->input->post('inputEmial');
        $invoice = $this->ConModel->get_invoce_by_cod($cod);

        $url = base_url().'invoice?id='.$cod;

        $bodyEmail = array('nombre' => $invoice->COMP_NOM.' '.$invoice->COMP_APE, 'url' => $url );

        $body = $this->load->view('emails/email_send_venta',$bodyEmail, true);

        $sendMAil = $this->send_email($email,$body,'Factura de venta');

        if($sendMAil){
            $data = array('error' => false, 'Mens' => 'Factura enviada al correo => '.$email);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe, se presento un error al enviar el correo, por favor intente más tarde');
            echo json_encode($data);
        }
    }

    public function get_info_cancel()
    {
        $cod = $this->input->post('cod');

        $info = $this->ConModel->get_info_cancel($cod);

        if($info){
            $data = array('error' => false, 'Mens' => $info);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo encontrar los datos asociados a este codigo');
            echo json_encode($data);
        }
    }

    /**
     * Genera una nueva contraseña
     */
    private function claveAleatoria()
    {
        $longitud = 15;
        $opcLetra = true;
        $opcNumero = true;
        $opcMayus = true;
        $opcEspecial = false;

        $letras = "abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales = "|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == true) {
            $listado .= $letras;
        }

        if ($opcNumero == true) {
            $listado .= $numeros;
        }

        if ($opcMayus == true) {
            $listado .= $letrasMayus;
        }

        if ($opcEspecial == true) {
            $listado .= $especiales;
        }

        for ($i = 1; $i <= $longitud; $i++) {
            $caracter = $listado[rand(0, strlen($listado) - 1)];
            $password .= $caracter;
            $listado = str_shuffle($listado);
        }
        return $password;
    }

    /**
     * Genera un nuevo token por usuario
     */
    public function get_token()
    {
        $numero = rand(40,80);
        return  bin2hex(random_bytes(($numero - (50 % 2)) / 2));
    }

    public function prueba()
    {
        echo $this->get_order_id();
    }

    /**
     * Genera un oirden id
     */
    public function get_order_id()
    {
        $numero = rand(5,10);
        return  bin2hex(random_bytes(($numero - (50 % 2)) / 2));
    }

    /**
     * Función que permite realizar el envios de correos electrónicos
     * @param String $email => Direccion email del usuario
     * @param String $body => Cuerpo del correo en html and php
     * @param String $asunto => Mensaje del correo
     * @return Int
     */
    private function send_email($email,$body,$asunto)
    {
        $config['protocol']     = $this->config->item('email_protocol');
        $config['smtp_host']    = $this->config->item('email_smtp_host');
        $config['smtp_port']    = $this->config->item('email_smtp_port');
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $this->config->item('email_smtp_user');
        $config['smtp_pass']    = $this->config->item('email_smtp_pass');
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; // o html
        $config['validation']   = TRUE; // para validar el correo electronico 

        $this->email->initialize($config);

        $this->email->from('notificaciones@ventcar.com', 'Notificaciones Ventcar');
        $this->email->to($email);
        $this->email->subject($asunto);
        $this->email->message($body);

        return $this->email->send();
    }
}
