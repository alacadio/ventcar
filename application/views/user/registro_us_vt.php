<!-- Content Header (Page header) -->
      <div class="content-header">
        <input type="hidden" name="val">
        <div class="container">
          <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark2">Registro de venta del vehiculo</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Registro</a></li>
                <li class="breadcrumb-item active">Venta</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-info"></i> Información a tener en cuenta</h5>
            <h5>Estimado cliente, le queremos informar por medio del presente mensaje</h5>
            <ul>
              <li>Vetncar.com se abstiene de realizar negociaciones con vehículos en los cuales se han realizados modificaciones en su motor o en su estructura física</li>
            </ul>
          </div>
        </div><!-- /.container-fluid -->
        </div>
      </div>
      <!-- /.content-header -->

      <div class="content">
        <div class="container">
          <div class="container-fluid">
          
          <form action="<?php echo base_url();?>usuario/registro" method="post" id="regs_user" enctype="multipart/form-data">
            <p>Campos obligatorios: *</p>
            <input type="hidden" name="estado" value="0">
                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Cuéntanos sobre ti</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputDocumento">Documento*</label>
                          <input type="text" class="form-control" name="inputDocumento" id="inputDocumento" placeholder="Documento" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputNombre">Nombre*</label>
                          <input type="text" class="form-control" name="inputNombre" id="inputNombre" placeholder="Nombre" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputApellido">Apellido</label>
                          <input type="text" class="form-control" name="inputApellido" id="inputApellido" placeholder="Apellido">
                        </div>
                      </div>
                    </div>
                    <div class="row">                      
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputCelular">Celular*</label>
                          <input type="text" class="form-control" name="inputCelular" id="inputCelular" placeholder="Celular" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputCiudad">Ciudad*</label>                          
                          <select id="inputCiudad" name="inputCiudad" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputEmail">Email*</label>
                          <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-outline card-primary collapsed-card">
                  <div class="card-header">
                    <h3 class="card-title">Descríbenos tu vehículo</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                    <!-- /.card-tools -->                    
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAño" id="inputAño" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputMarca" name="inputMarca" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputModelo" name="inputModelo" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="inputVersion" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="text" class="form-control" name="inputKilometraje" id="inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlaca" id="inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5" value="1" checked>
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5" value="0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMenorValorVenta">Menor valor venta</label>
                          <input type="text" class="form-control" name="inputMenorValorVenta" id="inputMenorValorVenta" placeholder="Menor valor venta" disabled value="">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="hidden" name="inputValorpromediodeventa" id="pro">
                          <label for="inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="text" class="form-control" name="inputValorp" id="inputValorpromediodeventa" placeholder="Valor promedio de venta" disabled value="">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="text" class="form-control" name="inputMejorvalordeventa" id="inputMejorvalordeventa" placeholder="Mejor valor de venta" disabled value="">
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVenta" id="inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">                      
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" checked value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" checked>
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-outline card-primary collapsed-card">
                  <div class="card-header">
                    <h3 class="card-title">¿Tienes imágenes del vehículo? <small>(opcional)</small> </h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Imagen</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-body">
                    <button type="submit" class="btn btn-block btn-primary">Registrar</button>
                  </div>
                </div>
                          
          </form>
          <!-- /.card -->
          <!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
      </div>
      <!-- /.content -->