<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
     * Usuario sistema
     * 
     * @var object
     */
    protected $user;
    /**
     * Listado de vehiculos
     * 
     * @var Array 
     * 
    */
    protected $list_car;

	// --------------------------------------------------------------------
	public function __construct()
	{
		parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ConModel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('pagination');
        
        
        /*
        if ($this->session->userdata('login')) {
            if($this->session->userdata('car') == 3){
                redirect(base_url().'ingreso');
            }
        } else {
            redirect(base_url().'ingreso');
        }*/

        if($this->session->userdata('login')){
            $token = $this->session->userdata('doc');
            $this->user = $this->UserModel->get_user_by_document($token);
        }
    }
    
	public function index()
    {
        $config = array();
        $user = 0;

        if($this->session->userdata('login')){
            if($this->user->TIP == 2){
                $user = $this->user->TIP;
                $this->list_car = $this->ConModel->get_list_vh();
            }else if($this->user->TIP == 3){
                $user = $this->user->TIP;
                $this->list_car = $this->ConModel->get_list_vh_cli();
            }else{
                $this->list_car = $this->ConModel->get_list_vh_cli();
            }
        }else{
            $this->list_car = $this->ConModel->get_list_vh_cli();
        }
        
        $cont = count($this->list_car);

        $config['base_url'] = base_url().'welcome';
		$config['total_rows'] = $cont;
        $config['per_page'] = 9;
        $config["uri_segment"] = 2;
        

        $config['num_links'] = 2;
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;

        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		
		$config['first_tag_open'] = '<li class="page-item page-link">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		
		$config['prev_tag_open'] = '<li class="prev page-link"> ';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		
		$config['next_tag_open'] = '<li class="page-item page-link">';
		$config['next_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li class="page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item page-link">';
		$config['num_tag_close'] = '</li>';


        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $post = $this->ConModel->get_num_pagina($config['per_page'],$page, $user);

        $data = array(
            'est' => 0,
            'doc' => $this->session->userdata('token'),
            'count' => $cont,
            'js' => 1,
            'script' => 'fil1.js',
            'lt' => $post,
            't' => $this->session->userdata('token'),
            'link' => $this->pagination->create_links()
        );
		$this->load->view("layout/venta_init/header",$data);
        $this->load->view("venta/index");
        $this->load->view("layout/venta_init/footer");
    }

	public function detalles_vehiculo()
	{
        $my_car = 0;

        $id = $this->input->get('id');

        if($this->session->userdata('login')){
            $token = $this->session->userdata('doc');
            $my_car = $this->ConModel->get_car_by_user($token,$id);
        }

        if($my_car){
            $my_car = 1;
        }

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        $num = $this->UserModel->get_num_adm()->TEL;

        $list_alt = $this->ConModel->get_list_rela($id);

        $dat = array(
            'est' => $id,
            'doc' => $this->session->userdata('token'),
            'car' => $car,
            'img' => $img,
            'num' => $num,
            'js' => 1,
            'script' => 'fil1.js',
            'list' => $list_alt,
            'my_car' => $my_car
        );

		$this->load->view("layout/venta_init/header",$dat);
        $this->load->view("venta/detalles");
        $this->load->view("layout/venta_init/footer");
    }
    
    public function calculador()
    {
        $this->load->view("layout/calculador/header");
        $this->load->view("venta/calculador");
        $this->load->view("layout/calculador/footer");
    }

    public function get_invoice()
    {
        $id = $this->input->get('id');
        $admin = $this->UserModel->get_admin_by_invoice();
        $invoice = $this->ConModel->get_invoce_by_cod($id);
        $iva = $invoice->VAL * 0.19;

        $data = array(            
            'date_invo' => $admin,
            'invoice' => $invoice,
            'iva' => $iva
        );
        
        $this->load->view("venta/invoice",$data);        
    }

    public function public_list()
    {
        $post = $this->ConModel->get_list_landing();

        $data = array(
            'lt' => $post
        );
        $this->load->view("layout/list/header", $data);
        $this->load->view("user/public_list");
        $this->load->view("layout/list/footer");
    }

    //Funciones
    public function get_ofer_con()
    {
        $id = $this->input->post('id');

        $doc = $this->session->userdata('doc');

        $consulta = $this->ConModel->get_ofer_ID_doc($id,$doc);

        if($consulta){
            $data = array('error' => true, 'Mens' => $consulta, 'est' => 'Ya ha realizado una oferta por este vehículo, la cual el valor es de: ');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => $consulta, 'EST' => 'Valor a ofertar');
            echo json_encode($data);
        }
    }

    public function set_ofertar()
    {
        $id = $this->input->post('id');
        $valor = $this->input->post('val');
        $doc = $this->user->DOC;

        $data = array(
            'concesionario' => $doc,
            'vh' => $id,
            'cant_oferta' => $valor            
        );

        $result = $this->ConModel->set_register_oferta($data);

        if($result){
            $dato = array('error' => false, 'Mens' => '¡¡¡La oferta ha sido registrada!!!');
            echo json_encode($dato);
        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, pero no se ha podido registrar la oferta, por favor trate más tarde');
            echo json_encode($dato);
        }
    }

    public function get_list_year()
    {
        $list = $this->ConModel->get_list_año_registrados();
        $marca = $this->ConModel->get_list_marca_registrado();
        $model = $this->ConModel->get_list_modelo_registrado();
        $version = $this->ConModel->get_list_version_registrada();
        if($list){
            $data = array('error' => false, 'Mens' => $list, 'mar' => $marca, 'mod' => $model, 'ver' => $version);
            echo json_encode($data);
        }else{            
            $data = array('error' => true, 'Mens' => 0);
            echo json_encode($data);
        }
    }

    public function get_list_marc()
    {

        $fecha = $this->input->get('fecha');

        $list = $this->ConModel->get_list_marca_Registrada($fecha);

        if($list){
            $data = array('error' => false, 'Mens' => $list);
            echo json_encode($data);
        }else{            
            $data = array('error' => true, 'Mens' => 0);
            echo json_encode($data);
        }
    }

    public function get_list_model()
    {
        $marca = $this->input->get('marca');

        $fecha = $this->input->get('fecha');

        $modelo = $this->ConModel->get_list_model_by_date_marc($fecha,$marca);

        if($modelo){
            $data = array('error' => false, 'Mens' => $modelo);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos =>'.$fecha.' MArca => '.$marca);
            echo json_encode($data);
        }
    }
}
