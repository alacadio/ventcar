<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ConModel');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') == 1){
                redirect(base_url().'admin/');
            }
            if($this->session->userdata('car') == 2){
                redirect(base_url().'concesionario/');
            }
            if($this->session->userdata('car') == 3){
                redirect(base_url().'usuario/perfil');
            }
        } else {
            $this->load->view("auth/login");
        }
        
    }

    public function register()
    {
        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') == 1){
                redirect(base_url().'admin/');
            }
            if($this->session->userdata('car') == 2){
                redirect(base_url().'concesionario/');
            }
            if($this->session->userdata('car') == 3){
                redirect(base_url().'usuario/');
            }
        } else {
            $this->load->view("auth/registro");
        }
    }

    public function cambio_contrasena()
    {
        $this->load->view("auth/cambio");
    }

    //----------------------------------------------
    public function obtener_por_id()
    {
        $id = $this->input->post('id');

        $cons = $this->UserModel->get_user_by_document($id);

        if($cons){
            $data = array('error' => false, 'Mens' => $cons);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe pero se presento un error al tratar de cargar la información, por favor trate más tarde');
            echo json_encode($data);
        }
    }


    public function login()
    {
        $user = $this->input->post("user");
        $pass = $this->input->post("pass");

        $pass = md5($pass);

        $result = $this->UserModel->get_login($user,$pass);

        if($result){
            $dato = array('doc' => $result->DOC, 'est' => $result->EST, 'car' => $result->TIP, 'login' => true, 'token' => $result->TOKEN);
            $this->session->set_userdata($dato);
            $data = array('error' => false, 'Mens' => 'Bienvenido a ventcar');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Las credenciales de acceso son incorrectas, por favor verifique el usuario y/o contraseña!'.$pass);
            echo json_encode($data);
        }


    }

    public function set_register()
    {
        $nit    = $this->input->post('inputNit');
        $razon  = $this->input->post('inputRaz');
        $nombre = $this->input->post('inputNom');
        $asesor = $this->input->post('inputAsc');
        $dir    = $this->input->post('inputDir');
        $city   = $this->input->post('inputCid');
        $cell   = $this->input->post('inputCell');
        $email  = $this->input->post('inputEmail');
        $pass   = $this->input->post('inputPass');        
        $rep    = $this->input->post('inputRep');
        $token  = $this->get_token();

        $result = $this->UserModel->get_user_by_email($email);

        $documento = $this->UserModel->get_user_by_document($nit);

        if($documento){
            $data = array('error' => true, 'Mens' => 'El NIT ya existe, por favor verifique el numero ingresado');
            echo json_encode($data);
            return;
        }

        if($result){
            $data = array('error' => true, 'Mens' => 'La dirección de email ya se encuentra registrada, pro favor pruebe con otra');
            echo json_encode($data);
            return;
        }

        if($rep != $pass){
            $data = array('error' => true, 'Mens' => '¡¡¡Las contraseñas no coinciden!!!');
            echo json_encode($data);
            return;
        }

        $data = array(
            'documento'     => $nit, 
            'razon'         => $razon,
            'nombre'        => $nombre,
            'responsable'   => $asesor,
            'telefono'      => $cell,  
            'email'         => $email,
            'direccion'     => $dir,
            'ciudad'        => $city,
            'token'         => $token,
            'tipo_us'       => 2,
            'estado'        => 1
        );

        $pass = md5($pass);

        $login = array(
            'id_login'  => $nit, 
            'usuario'   => $nit,
            'pass'      => $pass
        );


        $emailData = array('nombre' => $nombre );

        $body = $this->load->view('emails/email_register_cons',$emailData,true);


        $result_set = $this->UserModel->set_registro($data);

        if($result_set){

            $result_set_login = $this->UserModel->set_insert_login($login);

            if($result_set_login){
                //if($this->send_email($email,))
                $dato = array('doc' => $nit, 'est' => 1, 'car' => 2, 'login' => true, 'token' => $token);
                $this->session->set_userdata($dato);
                $sendEmail = $this->send_email($email,$body,'Registro exitoso');
                if($sendEmail){
                    $data = array('error' => false, 'Mens' => 'El usuario ha sido registrado de forma exitosa, en breve recibirá un correo electrónico');
                    echo json_encode($data);
                }else{
                    $data = array('error' => false, 'Mens' => 'El usuario ha sido registrado de forma exitosa!!!');
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => 'El usuario ha sido registrado de forma exitosa, pero los datos de ingreso no, por favor comuníquese con webmaster@ventcar.com');
                echo json_encode($data);
            }

        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo registrar el usuario en estos momentos, por favor vuelva a intentar más tarde');
            echo json_encode($data);
        }
        
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function new_pass()
    {
        $email = $this->input->post('email');
        $doc = $this->input->post('doc');

        $user = $this->UserModel->get_user_by_emadoc($doc,$email);

        if($user){

            $new_pass = $this->claveAleatoria();

            $pass = md5($new_pass);

            $newLogin = array('pass' => $pass);

            $cambio = $this->UserModel->set_update_login($doc, $newLogin);

            if($cambio){

                $data = array('documento' => $doc );

                $reg = $this->UserModel->set_insert_cambio($data);

                $body_email = array('nombre' => $user->NOM, 'apellido' => $user->APE, 'contraseña' => $new_pass);

                $body = $this->load->view('emails/email_cambio_pass',$body_email,true);

                if($reg){
                    if($this->send_email($email,$body,"Cambio de contraseña")){
                        $data = array('error' => false, 'Mens' => '¡La contraseña ha sido cambiada de forma exitosa!!! En breve recibirá un email');
                        echo json_encode($data);
                    }else{
                        $data = array('error' => true, 'Mens' => '¡La contraseña ha sido cambiada de forma exitosa!!! El email de confirmación no ha podido ser enviado, esta es su nueva contraseña ==> '.$new_pass);
                        echo json_encode($data);
                    }
                }else{
                    $data = array('error' => true, 'Mens' => '¡La contraseña ha sido cambiada de forma exitosa!!! El email de confirmación no ha podido ser enviado, esta es su nueva contraseña ==> '.$new_pass);
                    echo json_encode($data);
                }
            }else{
                $data = array('error' => true, 'Mens' => 'Lo sentimos, pero en estos momentos no podemos realizar el cambio de contraseña, por favor póngase en contacto con el administrador del sitio para el cambio de la contraseña');
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => 'Los datos suministrados no son correctos o el usuario no se encuentra registrado, por favor verifíquelos');
            echo json_encode($data);
        }
    }

    public function prueba()
    {
        $body_email = array('nombre' => "Pepito", 'apellido' => "Plus", 'usuario' => 'hsdhu8h8077834h80');

        $this->load->view('emails/email_cambio_pass',$body_email);
    }    

    public function generar_clave()
    {
        $clave = $this->claveAleatoria();
        $data = array('error' => false, 'Mens' => $clave);
        echo json_encode($data);
    }

    public function get_list_marca()
    {
        $fecha = $this->ConModel->get_list_vehiculo_fecha();

        if($fecha){
            $data = array('error' => false, 'Fecha' => $fecha);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos');
            echo json_encode($data);
        }
    }

    /**
     * Busca las listas en la base de datos
     */
    public function get_marca_by_fecha()
    {
        $fecha = $this->input->get('fecha');

        $marca = $this->ConModel->get_list_marca_by_fecha($fecha);

        if($marca){
            $data = array('error' => false, 'Marca' => $marca);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos');
            echo json_encode($data);
        }
    }

    public function get_modelo_by_fecha_marca()
    {
        $marca = $this->input->get('marca');

        $fecha = $this->input->get('fecha');

        $modelo = $this->ConModel->get_list_modelo_by_fecha_marca($fecha,$marca);

        if($modelo){
            $data = array('error' => false, 'Modelo' => $modelo);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos =>'.$fecha.' MArca => '.$marca);
            echo json_encode($data);
        }
    }

    public function get_version_by_fecha_marca_modelo()
    {
        $marca = $this->input->get('marca');

        $fecha = $this->input->get('fecha');

        $modelo = $this->input->get('modelo');

        $version = $this->ConModel->get_list_version_by_fecha_marca_modelo($fecha,$marca,$modelo);

        if($version){
            $data = array('error' => false, 'Version' => $version);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos');
            echo json_encode($data);
        }
    }

    public function get_valor_by_all()
    {
        $marca = $this->input->get('marca');

        $fecha = $this->input->get('fecha');

        $modelo = $this->input->get('modelo');

        $version = $this->input->get('version');

        $valor = $this->ConModel->get_list_valor_by_fe_ma_mo_ve($fecha,$marca,$modelo,$version)->F * 1000;

        $por = $valor * 0.20;

        $menos = $valor - $por;

        $mas = $valor + $por;

        if($valor){
            $data = array(
                'error' => false, 
                'Valor' => $valor,
                'menos' => $menos,
                'mas' => $mas
            );
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'No se pudo cargar los datos');
            echo json_encode($data);
        }
    }

    public function reg_noti()
    {
        $id = $this->input->get("id");

        $data = array('id_vh' => $id );

        $result = $this->ConModel->set_register_notificacion($data);

        if($result){
            $data = array('error' => false, 'Mens' => '¡¡¡Éxito!!! Ventcar ha sido notificados de tu interés por el vehículo, en minutos nos estaremos comunicando por medio de whatsapp');
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => '¡¡Upppss!! Se presento un error inesperado, pero tranquilo, si el whatsapp se ejecuto y envió el mensaje, fuimos notificado de tu interés por el vehículo, en minutos nos comunicaremos por vía whatsapp');
            echo json_encode($data);
        }
    }    
    
    /**
     * Genera una nueva contraseña
     */
    private function claveAleatoria()
    {
        $longitud = 15;
        $opcLetra = true;
        $opcNumero = true;
        $opcMayus = true;
        $opcEspecial = false;

        $letras = "abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales = "|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == true) {
            $listado .= $letras;
        }

        if ($opcNumero == true) {
            $listado .= $numeros;
        }

        if ($opcMayus == true) {
            $listado .= $letrasMayus;
        }

        if ($opcEspecial == true) {
            $listado .= $especiales;
        }

        for ($i = 1; $i <= $longitud; $i++) {
            $caracter = $listado[rand(0, strlen($listado) - 1)];
            $password .= $caracter;
            $listado = str_shuffle($listado);
        }
        return $password;
    }

    /**
     * Genera un nuevo token por usuario
     */
    public function get_token()
    {
        $numero = rand(40,80);
        return  bin2hex(random_bytes(($numero - (50 % 2)) / 2));
    }

    /**
     * Función que permite realizar el envios de correos electrónicos
     * @param String $email => Direccion email del usuario
     * @param String $body => Cuerpo del correo en html and php
     * @param String $asunto => Mensaje del correo
     * @return Int
     */
    private function send_email($email,$body,$asunto)
    {
        $config['protocol']     = $this->config->item('email_protocol');
        $config['smtp_host']    = $this->config->item('email_smtp_host');
        $config['smtp_port']    = $this->config->item('email_smtp_port');
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $this->config->item('email_smtp_user');
        $config['smtp_pass']    = $this->config->item('email_smtp_pass');
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; // o html
        $config['validation']   = TRUE; // para validar el correo electronico 

        $this->email->initialize($config);

        $this->email->from('notificaciones@ventcar.com', 'Notificaciones Ventcar');
        $this->email->to($email);
        $this->email->subject($asunto);
        $this->email->message($body);

        return $this->email->send();
    }
}
