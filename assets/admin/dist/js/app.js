var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $('[data-toggle="tooltip"]').tooltip();

  $(".select2").select2();

  $(".select2bs4").select2({
    theme: "bootstrap4",
  });

  $("#example1").DataTable({
    responsive: true,
  });

  $("#example2").DataTable({
    responsive: true,
  });

  $("#example3").DataTable({
    responsive: true,
  });

  $("#example4").DataTable({
    responsive: true,
  });

  $(".with-caption").magnificPopup({
    type: "image",
    closeBtnInside: false,
    mainClass: "mfp-with-zoom mfp-img-mobile",

    image: {
      verticalFit: true,
      titleSrc: function (item) {
        var caption = "Ventcar";

        return (
          caption +
          ' <img src="' +
          base_url +
          'assets/public/logos/originalhorizontalblanco.svg" width="50" height="28" />'
        );
      },
    },

    gallery: {
      enabled: true,
    },

    callbacks: {
      open: function () {
        this.wrap.on("click.pinhandler", ".pin-it", function (e) {
          // This part of code doesn't work on CodePen, as it blocks window.open
          // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
          /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);

                    
                    return false;*/
        });
      },
      beforeClose: function () {
        //this.wrap.off('click.pinhandler');
      },
    },
  });

  
  
  
  
  $("#act_log_amd").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text: "Se está actualizando la información, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

 
  
  $("#regs_user").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  
  $("#reg_car_cons").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#cgr_xlxs").submit(function (ev) {
    Swal.fire({
      icon: "warning",
      title: "Registro en proceso",
      text:
        "Se están cargando los datos a la base de datos, esto puede tardar un par de minutos, POR FAVOR NO CIERRE ESTE MENSAJE NI LA VENTANA HASTA QUE SE TERMINE LA OPERACIÓN",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  fun_get_info_db();

  $("#inputAño").change(function () {
    const año = $("#inputAño").val();
    if (año == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/marca",
      data: { fecha: año },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputMarca").empty();
          $("#inputMarca").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Marca, function (i, item) {
            $("#inputMarca").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputMarca").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputMarca").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/modelo",
      data: { fecha: año, marca: marca },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Modelo, function (i, item) {
            $("#inputModelo").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputModelo").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputModelo").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    const modelo = $("#inputModelo").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/version",
      data: { fecha: año, marca: marca, modelo: modelo },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Version, function (i, item) {
            $("#inputVersion").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputVersion").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputVersion").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    const modelo = $("#inputModelo").val();
    const version = $("#inputVersion").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    if (version == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/valor",
      data: { fecha: año, marca: marca, modelo: modelo, version: version },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputValorpromediodeventa").val(
            new Intl.NumberFormat("es-MX").format(data.Valor)
          );
          $("#inputMenorValorVenta").val(
            new Intl.NumberFormat("es-MX").format(data.menos)
          );
          $("#inputMejorvalordeventa").val(
            new Intl.NumberFormat("es-MX").format(data.mas)
          );
          $("#pro").val(data.Valor);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Año").change(function () {
    const año = $("#Año").val();
    if (año == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/marca",
      data: { fecha: año },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Marca").empty();
          $("#Marca").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Modelo").empty();
          $("#Modelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Marca, function (i, item) {
            var marca = item.F;
            $("#Marca").append(
              '<option value="' + marca + '">' + marca + "</option>"
            );
          });
          $("#Marca").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Marca").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/modelo",
      data: { fecha: año, marca: marca },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Modelo").empty();
          $("#Modelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Modelo, function (i, item) {
            $("#Modelo").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#Modelo").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Modelo").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    const modelo = $("#Modelo").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/version",
      data: { fecha: año, marca: marca, modelo: modelo },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#Version").empty();
          $("#Version").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Version, function (i, item) {
            $("#Version").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#Version").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#Version").change(function () {
    const año = $("#Año").val();
    const marca = $("#Marca").val();
    const modelo = $("#Modelo").val();
    const version = $("#Version").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    if (version == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/valor",
      data: { fecha: año, marca: marca, modelo: modelo, version: version },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputValorpromediodeventa").val(
            new Intl.NumberFormat("es-MX").format(data.Valor)
          );
          $("#inputMenorValorVenta").val(
            new Intl.NumberFormat("es-MX").format(data.menos)
          );
          $("#inputMejorvalordeventa").val(
            new Intl.NumberFormat("es-MX").format(data.mas)
          );
          $("#pro").val(data.Valor);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  
  $("#form_reg_pur").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando la marca, por favor espere un momento.",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  
  fun_mostrar_not();

  $("#inputDocumentoW").blur(function () {
    var a = $("#inputDocumentoW").val();
    if (a.length > 0) {
      $.ajax({
        url: base_url + "admin/info/client",
        type: "post",
        dataType: "json",
        data: { doc: a },
        success: function (data) {
          if (!data.error) {
            $("#inputNombreW").val(data.Mens.NOM);
            if (data.Mens.APE) $("#inputApellidoW").val(data.Mens.APE);
            $("#inputCelularW").val(data.Mens.TEL);
            $("#inputCiudadW").val(data.Mens.CIUD);
            $("#inputEmailW").val(data.Mens.EMA);
          } else {
            $("#inputNombreW").val("");
            $("#inputApellidoW").val("");
            $("#inputCelularW").val("");
            $("#inputCiudadW").val("");
            $("#inputEmailW").val("");
            Swal.fire({
              icon: "info",
              title: "Usuario no registrado",
              text:
                "El comprador para este número de identificación no esta registrado, al generar la venta será registrado",
            });
          }
        },
        error: function () {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text:
              "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
          });
        },
      });
    }
  });

  bsCustomFileInput.init();

  loadJSON(function (response) {
    // Parse JSON string into object
    var JSONFinal = JSON.parse(response);
    $("#inputCiudad").append(
      '<option value="0">- Seleccione una opcion -</option>'
    );
    console.log("LL");
    $.each(JSONFinal, function (data, obj) {
      $.each(obj.ciudades, function (data, cid) {
        $("#inputCiudad").append(
          '<option value="' + cid + '">' + cid + "</option>"
        );
      });
    });
  });
});

function habilitar_us(id) {
  Swal.fire({
    icon: "info",
    title: "Habilitando",
    text:
      "Se esta realizando el proceso de habilitar el usuario, por favor espere",
  });
  $.ajax({
    url: base_url + "admin/cons/hab",
    type: "post",
    dataType: "json",
    data: { doc: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Habilitando",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}





function modal_del_mar_pug(id) {
  $("#id_m").val(id);
  $("#modal-danger-marca").modal();
}

function modal_del_mar_db(id) {
  $("#id_M").val(id);
  $("#modal-lg").modal();
}

function modal_del_car(id) {
  $("#id_con").val(id);
  $("#modal_delete_car").modal();
}





function modal_delete_oferta(id) {
  $("#id_con").val(id);
  $("#modal-danger").modal();
}

function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", base_url + "assets/public/js/colombia.json", true); // Reemplaza colombia-json.json con el nombre que le hayas puesto
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function fun_des_p() {
  Swal.fire({
    icon: "warning",
    title: "Desactivar",
    text:
      "Se está procesando la acción de desactivar la publicación, por favor espere un momento",
  });
  const id = $("#id_v").val();
  $.ajax({
    url: base_url + "admin/post_des_act",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Desactivar",
          text: data.Mens,
        });

        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}



function fun_del_car() {
  const id = $("#id_con").val();
  Swal.fire({
    icon: "info",
    title: "Eliminar",
    text:
      "Se está procesando la eliminación de esta publicación, por favor espere un momento",
  });
  $.ajax({
    url: base_url + "concesionario/delete/car",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Eliminar",
          text: data.Mens,
        });

        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_clave() {
  $.ajax({
    url: base_url + "clave",
    type: "post",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputPass").val(data.Mens);
        Swal.fire({
          icon: "info",
          title: "Contraseña generada",
          text:
            "Se ha generado una nueva contraseña, esta es su nueva contraseña por favor cópiela y guárdela en un lugar seguro: " +
            data.Mens,
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_del_ofer() {
  const id = $("#id_con").val();
  Swal.fire({
    icon: "info",
    title: "Eliminar",
    text:
      "Se está procesando la eliminación de esta oferta, por favor espere un momento",
  });
  $.ajax({
    url: base_url + "concesionario/delete/of",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Eliminar Oferta",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_get_info_db() {
  $.ajax({
    type: "GET",
    url: base_url + "admin/import/list",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputVersion")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputMarca")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputModelo")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputAño").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Fecha, function (i, item) {
          $("#inputAño").append(
            "<option value=" + item.F + ">" + item.F + "</option>"
          );
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_datos_año(param) {
  $.ajax({
    type: "GET",
    url: base_url + "admin/import/list",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#Año").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Fecha, function (i, item) {
          if (item.F == param) {
            $("#Año").append(
              "<option value=" + item.F + " selected>" + item.F + "</option>"
            );
          } else {
            $("#Año").append(
              "<option value=" + item.F + ">" + item.F + "</option>"
            );
          }
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_acep_ofert(id, vh) {
  $.ajax({
    type: "post",
    url: base_url + "admin/oferta/acet",
    data: { id: id, vh: vh },
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Oferta registrada",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_set_datos_select(input, dato, url, ii) {
  $.ajax({
    type: "GET",
    url: base_url + url,
    data: { fecha: dato[0], marca: dato[1], modelo: dato[2] },
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        console.log(dato[ii]);
        var d = "#" + input;
        $(d).append('<option value="0">- Seleccione una opción -</option>');
        if (ii == 0) {
          $.each(data.Marca, function (i, item) {
            fun_pintar(item.F, dato[1], d);
          });
        } else if (ii == 1) {
          $.each(data.Modelo, function (i, item) {
            fun_pintar(item.F, dato[2], d);
          });
        } else if (ii == 2) {
          $.each(data.Version, function (i, item) {
            fun_pintar(item.F, dato[3], d);
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_pintar(paramA, paramB, input) {
  if (paramA == paramB) {
    $(input).append(
      '<option value="' + paramA + '" selected>' + paramA + "</option>"
    );
  } else {
    $(input).append('<option value="' + paramA + '">' + paramA + "</option>");
  }
}

function fun_oferta_G(nit, id, tel, url, e, nom) {
  var mens;
  url += "admin/con/car/perfil?id=" + id;
  if (e == 1) {
    mens =
      "Hola ventcar, he decidido aceptar la oferta ofrecida por el concesionario " +
      nom +
      " al vehículo " +
      url +
      ", por favor comuníquese con mi persona por este medio para ultimar detalles";
  } else if (e == 2) {
    mens =
      "¡¡¡Felicidades!!!, su oferta realizada al vehículo " +
      url +
      ", fue aceptada por el vendedor del vehículo, por favor comuníquese con nosotros por este medio para ultimar detalles";
  } else if (e == 3) {
    mens =
      "Hola ventcar, este vehículo me interesa " +
      url +
      ", ¡¡Agendemos una cita para checar el vehículo!!";
  } else if (e == 4) {
    mens =
      "Hola, para completar el registro, agendemos una cita para chequear el vehículo";
  }
  var whatsapp =
    "https://api.whatsapp.com/send?phone=" + 57 + tel + "&text=" + mens;
  window.open(whatsapp, "_blank");
  return false;
}

function fun_mostrar_not() {
  const not = $("#count_not").val();

  if (not > 0) {
    Swal.fire({
      icon: "success",
      title: "Notificaciones",
      text: "En hora buena tus vehículos han recibido varios interesados",
    });
  }
}

function fun_reg_mensj(id) {
  var mens = $("#text_" + id).val();
  if (mens == null) {
    Swal.fire({
      icon: "warning",
      title: "Advertencia",
      text:
        "No puede dejar los campos en blanco, por favor agregue un nuevo mensaje ",
    });
    return;
  }
  Swal.fire({
    icon: "info",
    title: "Guardando cambios",
    text: "Se están guardando los cambios, por favor espere un momento",
  });
  $.ajax({
    url: base_url + "admin/mensaje/rg",
    type: "post",
    dataType: "json",
    data: { id: id, mens: mens },
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Datos guardados",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_rech_oft(id) {
  alert(id);
}

function fun_del_mar_pur() {
  var id = $("#id_m").val();
  Swal.fire({
    icon: "info",
    title: "Eliminando la marca",
    text: "Se esta eliminando la marca, por favor espere un momento...",
  });
  $.ajax({
    url: base_url + "admin/delpur/marca",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Marca eliminada",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_del_mar_db() {
  var id = $("#id_M").val();
  Swal.fire({
    icon: "info",
    title: "Eliminando registros",
    text:
      "Se están eliminando los registros en la base de datos, por favor espere un momento...",
  });
  $.ajax({
    url: base_url + "admin/deldb/marca",
    type: "post",
    dataType: "json",
    data: { marca: id },
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Datos eliminados",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}
