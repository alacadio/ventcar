    
 <div class="content">
      <div class="container">
      <p style="color: #fff;">.</p>
      <p style="color: #fff;">.</p>
        <div class="row">
            <?php 
            foreach ($lt as $v): ?>
                <div class="col-sm-3">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h6 class="card-title m-0"> <?php echo $v->M." ".$v->MO; ?></h6>
                        </div>
                        <div class="card-body">
                            <center>
                            <img src="<?php echo base_url().$v->IMG;?>" alt="" width="150px" height="80px">                                  
                            <p></p>
                            <p><span><?php echo $v->A; ?> | <?php echo $v->K; ?></span> Km</p>
                            <h6>$<?php echo number_format($v->VAL); ?></h6>
                            </center>
                        </div>
                        <div class="card-footer">
                        <center>
                            <a href="<?php echo base_url();?>vehiculo/detalles?id=<?php echo $v->ID; ?>" data-toggle="tooltip" data-placement="top" title="Detalles" class="btn btn-primary">Ver detalles vehículos</a>
                        </center>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>