<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConModel extends CI_Model
{

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos las vehiculos registrada desde facecolda
     * @return Array
     */
    public function get_list_vehiculo()
    {
        $this->db->select("vehiculo_data.marca AS M, vehiculo_data.modelo AS MO, vehiculo_data.version AS V");
        $this->db->from("vehiculo_data");
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las marcas registrada desde facecolda
     * @return Array
     */
    public function get_list_vehiculo_marca()
    {
        $query = "SELECT
                    vehiculo_data.marca AS M
                    FROM
                    vehiculo_data
                    GROUP BY vehiculo_data.marca";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las modelos registrada desde facecolda
     * @return Array
     */
    public function get_list_vehiculo_modelo()
    {
        $query = "SELECT
                    vehiculo_data.modelo AS M
                    FROM
                    vehiculo_data
                    GROUP BY vehiculo_data.modelo";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las version registrada desde facecolda
     * @return Array
     */
    public function get_list_vehiculo_version()
    {
        $query = "SELECT
                    vehiculo_data.version AS V
                    FROM
                    vehiculo_data
                    GROUP BY vehiculo_data.version";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las fechas registrada desde facecolda
     * @return Array
     */
    public function get_list_vehiculo_fecha()
    {
        $query = "SELECT
                    fecha_valor.fecha AS F
                    FROM
                    fecha_valor
                    GROUP BY fecha_valor.fecha";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar las marcas por fecha
     * @param String $fecha => Año del vehiculo
     * @return Array
     */
    public function get_list_marca_by_fecha($fecha)
    {
        $query = "SELECT
                    vehiculo_data.marca AS F
                    FROM
                    vehiculo_data
                    INNER JOIN fecha_valor ON fecha_valor.id_vh = vehiculo_data.id_vh
                    WHERE fecha_valor.fecha = '$fecha'
                    GROUP BY vehiculo_data.marca";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar los modelos por fecha y marca
     * @param String $fecha => Año del vehiculo
     * @param String $marca => Marca del vehiculo
     * @return Array
     */
    public function get_list_modelo_by_fecha_marca($fecha,$marca)
    {
        $query = "SELECT
                    vehiculo_data.modelo AS F
                    FROM
                    vehiculo_data
                    INNER JOIN fecha_valor ON fecha_valor.id_vh = vehiculo_data.id_vh
                    WHERE fecha_valor.fecha = '$fecha' AND vehiculo_data.marca = '$marca'
                    GROUP BY vehiculo_data.modelo";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las versiones por la fecha marca y modelo
     * @param String $fecha => Año del vehiculo
     * @param String $marca => Marca del vehiculo
     * @param String $modelo => Modelo del vehiculo
     * @return Array
     */
    public function get_list_version_by_fecha_marca_modelo($fecha,$marca,$modelo)
    {
        $query = "SELECT
                    vehiculo_data.version AS F
                    FROM
                    vehiculo_data
                    INNER JOIN fecha_valor ON fecha_valor.id_vh = vehiculo_data.id_vh
                    WHERE fecha_valor.fecha = '$fecha' AND vehiculo_data.marca = '$marca' AND vehiculo_data.modelo = '$modelo'
                    GROUP BY vehiculo_data.version";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener el valor de un vehiculo por su fecha, marca,
     * modelo y version
     * @param String $fecha => Año del vehiculo
     * @param String $marca => Marca del vehiculo
     * @param String $modelo => Modelo del vehiculo
     * @param String $version => Version del vehiculo
     * @return Array
     */
    public function get_list_valor_by_fe_ma_mo_ve($fecha,$marca,$modelo,$version)
    {
        $this->db->select("fecha_valor.valor AS F");
        $this->db->from("vehiculo_data");        
        $this->db->join("fecha_valor","fecha_valor.id_vh = vehiculo_data.id_vh");
        $this->db->where("fecha_valor.fecha",$fecha);
        $this->db->where("vehiculo_data.marca",$marca);
        $this->db->where("vehiculo_data.modelo",$modelo);
        $this->db->where("vehiculo_data.version",$version);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los años registrados en la pagina
     */
    public function get_list_año_registrados()
    {
        $query = "SELECT
                    vt_venta_vh.`year` AS A
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.activo = 1
                    GROUP BY vt_venta_vh.`year`";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las marcas registradas
     */
    public function get_list_marca_registrado()
    {
        $query = "SELECT
                    vt_venta_vh.marca AS MA
                    FROM
                                        vt_venta_vh
                    WHERE vt_venta_vh.activo = 1
                    GROUP BY vt_venta_vh.marca";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas los modelos registrados
     */
    public function get_list_modelo_registrado()
    {
        $query = "SELECT
                    vt_venta_vh.modelo AS MO
                    FROM
                                        vt_venta_vh
                    WHERE vt_venta_vh.activo = 1
                    GROUP BY vt_venta_vh.modelo";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las versiones registradas
     */
    public function get_list_version_registrada()
    {
        $query = "SELECT
                    vt_venta_vh.version AS VER
                    FROM
                                        vt_venta_vh
                    WHERE vt_venta_vh.activo = 1
                    GROUP BY vt_venta_vh.version";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar las marcas registradas dentro del sistema
     * @return Array
     */
    public function get_list_marca_Registrada($fecha)
    {
        $query = "SELECT
                    vt_venta_vh.marca AS M
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.activo = 1 and vt_venta_vh.`year` = $fecha
                    GROUP BY vt_venta_vh.marca";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listra todos los modelos por fecha y marca
     */
    public function get_list_model_by_date_marc($fecha,$marca)
    {
        $query = "SELECT
                    vt_venta_vh.modelo AS M
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.activo = 1 and vt_venta_vh.`year` = '$fecha' AND vt_venta_vh.marca = '$marca'
                    GROUP BY vt_venta_vh.modelo";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listra todos las versiones por fecha, marca y modelo
     */
    public function get_list_version_by_date_marc_model($fecha,$marca,$model)
    {
        $query = "SELECT
                    vt_venta_vh.version AS M
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.activo = 1 and vt_venta_vh.`year` = '$fecha' AND vt_venta_vh.marca = '$marca' AND vt_venta_vh.modelo = '$model'
                    GROUP BY vt_venta_vh.version";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los vehiculos del sistema registrados para los concesionarios
     * @return Array
     */
    public function get_list_vh()
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID,
                    vt_venta_vh.marca AS MAR,
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.version AS V,
                    vt_venta_vh.`year` AS A,
                    vt_venta_vh.kilometraje AS K,
                    vt_venta_vh.placa AS P,
                    vt_venta_vh.valor_venta AS VAL,
                    vt_venta_vh.op_valor AS OP,
                    vt_venta_vh.activo AS EST,
                    vt_venta_vh.pintura AS PI,
                    vt_venta_vh.interior AS IN_T,
                    vt_venta_vh.mecanico AS ME,
                    vt_venta_vh.accidente AS ACC,
                    vt_venta_vh.choque AS CHO,
                    vt_venta_vh.combustible AS COM,
                    vt_venta_vh.descripcion AS DES,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,
                    vt_usuario.nombre AS NOM,
                    vt_usuario.apellido AS APE,
                    vt_usuario.telefono AS TEL,
                    vt_venta_vh.`user` AS US
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_usuario ON vt_venta_vh.documento = vt_usuario.documento
                    WHERE vt_venta_vh.`user` = 2 AND vt_venta_vh.activo = 1";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los vehiculos del sistema registrados para los clientes
     * @return Array
     */
    public function get_list_vh_cli()
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID,
                    vt_venta_vh.marca AS MAR,
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.version AS V,
                    vt_venta_vh.`year` AS A,
                    vt_venta_vh.kilometraje AS K,
                    vt_venta_vh.placa AS P,
                    vt_venta_vh.valor_venta AS VAL,
                    vt_venta_vh.op_valor AS OP,
                    vt_venta_vh.activo AS EST,
                    vt_venta_vh.pintura AS PI,
                    vt_venta_vh.interior AS IN_T,
                    vt_venta_vh.mecanico AS ME,
                    vt_venta_vh.accidente AS ACC,
                    vt_venta_vh.choque AS CHO,
                    vt_venta_vh.combustible AS COM,
                    vt_venta_vh.descripcion AS DES,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,
                    vt_usuario.nombre AS NOM,
                    vt_usuario.apellido AS APE,
                    vt_usuario.telefono AS TEL,
                    vt_venta_vh.`user` AS US
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_usuario ON vt_venta_vh.documento = vt_usuario.documento
                    WHERE vt_venta_vh.`user` = 1 AND vt_venta_vh.activo = 1";
        $dd = $this->db->query($query);
        return $dd->result();
    }
    
    //---------------------------------------------------------------------------
    public function get_num_pagina($limit,$offset, $user)
    {
        $this->db->select('vt_venta_vh.id_vh_venta AS ID,vt_venta_vh.marca AS MAR,vt_venta_vh.modelo AS MO,vt_venta_vh.version AS V,vt_venta_vh.`year` AS A,vt_venta_vh.kilometraje AS K,vt_venta_vh.placa AS P,vt_venta_vh.valor_venta AS VAL,vt_venta_vh.op_valor AS OP,vt_venta_vh.activo AS EST,vt_venta_vh.pintura AS PI,vt_venta_vh.interior AS IN_T,vt_venta_vh.mecanico AS ME,vt_venta_vh.accidente AS ACC,vt_venta_vh.choque AS CHO,vt_venta_vh.combustible AS COM,vt_venta_vh.descripcion AS DES,(SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,vt_usuario.nombre AS NOM,vt_usuario.apellido AS APE,vt_usuario.telefono AS TEL,vt_venta_vh.`user` AS US');
        $this->db->join('vt_usuario','vt_venta_vh.documento = vt_usuario.documento');
        if($user != 0){
            $this->db->where('vt_venta_vh.`user`',$user);
        }
        $this->db->where('vt_venta_vh.activo',1);
        $this->db->limit($limit, $offset);
        return $this->db->get('vt_venta_vh')->result();
    }

    public function get_num_pagina_bus($año, $marca, $modelo,$version,$comb,$k, $user , $check_val_10,$check_val_mx_4,$check_val_m_4)
    {
        $this->db->select('vt_venta_vh.id_vh_venta AS ID,vt_venta_vh.marca AS MAR,vt_venta_vh.modelo AS MO,vt_venta_vh.version AS V,vt_venta_vh.`year` AS A,vt_venta_vh.kilometraje AS K,vt_venta_vh.placa AS P,vt_venta_vh.valor_venta AS VAL,vt_venta_vh.op_valor AS OP,vt_venta_vh.activo AS EST,vt_venta_vh.pintura AS PI,vt_venta_vh.interior AS IN_T,vt_venta_vh.mecanico AS ME,vt_venta_vh.accidente AS ACC,vt_venta_vh.choque AS CHO,vt_venta_vh.combustible AS COM,vt_venta_vh.descripcion AS DES,(SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,vt_usuario.nombre AS NOM,vt_usuario.apellido AS APE,vt_usuario.telefono AS TEL,vt_venta_vh.`user` AS US');
        $this->db->from('vt_venta_vh');
        $this->db->join('vt_usuario','vt_venta_vh.documento = vt_usuario.documento');        
        $this->db->where('vt_venta_vh.activo', 1);
        if($user != 0){
            $this->db->where('vt_venta_vh.`user`',$user);
        }
        if($año != '0'){
            $this->db->where('vt_venta_vh.year',$año);
        }
        if($marca != '0'){
            $this->db->where('vt_venta_vh.marca',$marca);
        }
        
        if($modelo != '0'){
            $this->db->where('vt_venta_vh.modelo',$modelo);
        }
        if($version != '0'){
            $this->db->where('vt_venta_vh.version',$version);
        }
        if($comb != '-1'){
            $this->db->where('vt_venta_vh.combustible',$comb);
        }

        if($k != '0'){
            if($k == 1){
                $this->db->where('vt_venta_vh.kilometraje >=','0');
                $this->db->where('vt_venta_vh.kilometraje <=','10000');
            }
            elseif($k == 2){
                $this->db->where('vt_venta_vh.kilometraje >=','10000');
                $this->db->where('vt_venta_vh.kilometraje <=','20000');
            }elseif($k == 3){
                $this->db->where('vt_venta_vh.kilometraje >=','21000');
                $this->db->where('vt_venta_vh.kilometraje <=','30000');
            }
            elseif($k == 4){
                $this->db->where('vt_venta_vh.kilometraje >=','31000');
                $this->db->where('vt_venta_vh.kilometraje <=','40000');
            }elseif($k == 5){
                $this->db->where('vt_venta_vh.kilometraje >=','41000');
                $this->db->where('vt_venta_vh.kilometraje <=','50000');
            }elseif($k == 6){
                $this->db->where('vt_venta_vh.kilometraje >=','51000');
                $this->db->where('vt_venta_vh.kilometraje <=','60000');
            }elseif($k == 7){
                $this->db->where('vt_venta_vh.kilometraje >=','61000');
            }

        }
        
        if($check_val_10 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','0');
            $this->db->where('vt_venta_vh.valor_venta <=','10000000');
        }
        if($check_val_mx_4 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','11000000');
            $this->db->where('vt_venta_vh.valor_venta <=','40000000');
        }
        if($check_val_m_4 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','41000000');
        }
        return $this->db->get()->result();
    }

    public function get_busqueda_rapida($limit,$offset, $año, $marca, $modelo,$version,$comb,$k, $user, $check_val_10,$check_val_mx_4,$check_val_m_4)
    {
        $this->db->select('vt_venta_vh.id_vh_venta AS ID,vt_venta_vh.marca AS MAR,vt_venta_vh.modelo AS MO,vt_venta_vh.version AS V,vt_venta_vh.`year` AS A,vt_venta_vh.kilometraje AS K,vt_venta_vh.placa AS P,vt_venta_vh.valor_venta AS VAL,vt_venta_vh.op_valor AS OP,vt_venta_vh.activo AS EST,vt_venta_vh.pintura AS PI,vt_venta_vh.interior AS IN_T,vt_venta_vh.mecanico AS ME,vt_venta_vh.accidente AS ACC,vt_venta_vh.choque AS CHO,vt_venta_vh.combustible AS COM,vt_venta_vh.descripcion AS DES,(SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,vt_usuario.nombre AS NOM,vt_usuario.apellido AS APE,vt_usuario.telefono AS TEL,vt_venta_vh.`user` AS US');
        $this->db->join('vt_usuario','vt_venta_vh.documento = vt_usuario.documento');
        $this->db->where('vt_venta_vh.activo',1);
        if($user != 0){
            $this->db->where('vt_venta_vh.`user`',$user);
        }
        if($año != '0'){
            $this->db->where('vt_venta_vh.year',$año);
        }
        if($marca != '0'){
            $this->db->where('vt_venta_vh.marca',$marca);
        }
        if($modelo != '0'){
            $this->db->where('vt_venta_vh.modelo',$modelo);
        }
        if($version != '0'){
            $this->db->where('vt_venta_vh.version',$version);
        }
        if($comb != '-1'){
            $this->db->where('vt_venta_vh.combustible',$comb);
        }

        if($k != '0'){
            if($k == 1){
                $this->db->where('vt_venta_vh.kilometraje >=','0');
                $this->db->where('vt_venta_vh.kilometraje <=','10000');
            }
            elseif($k == 2){
                $this->db->where('vt_venta_vh.kilometraje >=','10000');
                $this->db->where('vt_venta_vh.kilometraje <=','20000');
            }elseif($k == 3){
                $this->db->where('vt_venta_vh.kilometraje >=','21000');
                $this->db->where('vt_venta_vh.kilometraje <=','30000');
            }
            elseif($k == 4){
                $this->db->where('vt_venta_vh.kilometraje >=','31000');
                $this->db->where('vt_venta_vh.kilometraje <=','40000');
            }elseif($k == 5){
                $this->db->where('vt_venta_vh.kilometraje >=','41000');
                $this->db->where('vt_venta_vh.kilometraje <=','50000');
            }elseif($k == 6){
                $this->db->where('vt_venta_vh.kilometraje >=','51000');
                $this->db->where('vt_venta_vh.kilometraje <=','60000');
            }elseif($k == 7){
                $this->db->where('vt_venta_vh.kilometraje >=','61000');
            }

        }

        if($check_val_10 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','0');
            $this->db->where('vt_venta_vh.valor_venta <=','10000000');
        }
        if($check_val_mx_4 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','11000000');
            $this->db->where('vt_venta_vh.valor_venta <=','40000000');
        }
        if($check_val_m_4 != 0){
            $this->db->where('vt_venta_vh.valor_venta >=','41000000');
        }
        
        $this->db->limit($limit, $offset);
        return $this->db->get('vt_venta_vh')->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los vehiculos del sistema
     * @return Array
     */
    public function get_list_vehiculos()
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID,
                    vt_venta_vh.marca AS MAR,
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.version AS V,
                    vt_venta_vh.`year` AS A,
                    vt_venta_vh.kilometraje AS K,
                    vt_venta_vh.placa AS P,
                    vt_venta_vh.valor_venta AS VAL,
                    vt_venta_vh.op_valor AS OP,
                    vt_venta_vh.activo AS EST,
                    vt_venta_vh.pintura AS PI,
                    vt_venta_vh.interior AS IN_T,
                    vt_venta_vh.mecanico AS ME,
                    vt_venta_vh.accidente AS ACC,
                    vt_venta_vh.choque AS CHO,
                    vt_venta_vh.combustible AS COM,
                    vt_venta_vh.descripcion AS DES,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,
                    vt_usuario.nombre AS NOM,
                    vt_usuario.apellido AS APE,
                    vt_usuario.telefono AS TEL,
                    vt_venta_vh.`user` AS US
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_usuario ON vt_venta_vh.documento = vt_usuario.documento";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los vehiculos listo para la venta
     * @return Array
     */
    public function get_list_to_sell()
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID,
                    vt_venta_vh.marca AS MAR,
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.version AS V,
                    vt_venta_vh.`year` AS A,
                    vt_venta_vh.kilometraje AS K,
                    vt_venta_vh.placa AS P,
                    vt_venta_vh.valor_venta AS VAL,
                    vt_venta_vh.op_valor AS OP,
                    vt_venta_vh.activo AS EST,
                    vt_venta_vh.pintura AS PI,
                    vt_venta_vh.interior AS IN_T,
                    vt_venta_vh.mecanico AS ME,
                    vt_venta_vh.accidente AS ACC,
                    vt_venta_vh.choque AS CHO,
                    vt_venta_vh.combustible AS COM,
                    vt_venta_vh.descripcion AS DES,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG,
                    (SELECT COUNT(vt_vehiculo_interes.vh) FROM vt_vehiculo_interes WHERE vt_vehiculo_interes.vh = vt_venta_vh.id_vh_venta ) AS COU,
                    (SELECT COUNT(vt_notificacion.id_vh) FROM vt_notificacion WHERE vt_notificacion.id_vh = vt_venta_vh.id_vh_venta ) AS NOTIF,
                    vt_usuario.nombre AS NOM,
                    vt_usuario.apellido AS APE,
                    vt_usuario.telefono AS TEL,
                    vt_venta_vh.`user` AS US
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_usuario ON vt_venta_vh.documento = vt_usuario.documento
                    WHERE vt_venta_vh.activo != 2";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las ventas aprovadas
     * @return Array
     */
    public function get_list_vt_ap()
    {
        $this->db->select("vt_venta.id_venta AS ID, vt_venta.id_vh_venta AS ID_VH, vt_venta.vendedor AS ID_V, vt_venta.concesionario AS ID_CON, vt_venta.fecha_venta AS FEC, vt_venta.venta_ap AS VT");
        $this->db->from("vt_venta");        
        $this->db->where("vt_venta.venta_ap",1);
        return $this->db->get()->result();
    }

    /**
     * Funcion que permite listar de forma aleatoria los vehiculos diferentes al id actual
     */
    public function get_list_rela($id)
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID, vt_venta_vh.marca AS M, vt_venta_vh.modelo AS MO, (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG, vt_venta_vh.`year` AS A, vt_venta_vh.kilometraje AS K, vt_venta_vh.valor_venta AS VAL
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.id_vh_venta != $id AND vt_venta_vh.activo = 1
                    ORDER BY RAND()
                    LIMIT 0,3";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    /**
     * Funcion que permite listar de forma aleatoria los vehiculos diferentes al id actual para el landing
     */
    public function get_list_landing()
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID, vt_venta_vh.marca AS M, vt_venta_vh.modelo AS MO, (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG, vt_venta_vh.`year` AS A, vt_venta_vh.kilometraje AS K, vt_venta_vh.valor_venta AS VAL
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.activo = 1
                    ORDER BY RAND()
                    LIMIT 0,4";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar las ofertas que tiene un vehiculo por id
     * @param Int $id => Identificacion del vehiculo
     * @return Array
     */
    public function get_list_ofert_view($id)
    {
        $this->db->select("vt_vehiculo_interes.id_vh_interes AS ID, vt_vehiculo_interes.ofert_activa AS ACT,vt_vehiculo_interes.cant_oferta AS CANT,vt_usuario.nombre AS NOM, vt_usuario.telefono AS TEL,vt_usuario.email AS EMA, vt_vehiculo_interes.vh AS ID_VH, vt_vehiculo_interes.ofert_gana AS OF_G");
        $this->db->from("vt_vehiculo_interes");
        $this->db->join("vt_usuario","vt_vehiculo_interes.concesionario = vt_usuario.documento");
        $this->db->where("vt_vehiculo_interes.vh ",$id);
        return $this->db->get()->result();
    }    

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite mostrar todas las notificaciones que tiene por el dia o no vista por un carro
     * @param $id => Identificacion del vehiculo
     * @return Array
     */
    public function get_notificacion_by_id_vh($id)
    {
        $this->db->select("vt_notificacion.notificacion AS NOTI, vt_notificacion.fecha AS FEC");
        $this->db->from("vt_notificacion");
        $this->db->where("vt_notificacion.notificacion_vista",0);
        $this->db->where("vt_notificacion.id_vh",$id);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todas las no ventas aprovadas
     * @return Array
     */
    public function get_list_vt_no_ap()
    {
        $this->db->select("vt_venta.id_venta AS ID, vt_venta.id_vh_venta AS ID_VH, vt_venta.vendedor AS ID_V, vt_venta.concesionario AS ID_CON, vt_venta.fecha_venta AS FEC, vt_venta.venta_ap AS VT");
        $this->db->from("vt_venta");        
        $this->db->where("vt_venta.venta_ap",0);
        $this->db->or_where("vt_venta.venta_ap",2);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los vehiculos interesados por el concesionario
     * @param Int $document => Nit del concesionario
     * @return Array
     */
    public function get_list_car_int($document)
    {
       $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID, 
                    vt_venta_vh.marca AS MAR, 
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.`year` AS A, 
                    vt_vehiculo_interes.fecha_oferta AS FECH, 
                    vt_vehiculo_interes.cant_oferta AS OFE,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_vehiculo_interes ON vt_vehiculo_interes.vh = vt_venta_vh.id_vh_venta
                    WHERE vt_vehiculo_interes.concesionario = $document";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar el total de vehiculos comprados por un concesionario
     * @param Int $document => Nit del concesionario
     * @return Array
     */
    public function get_list_car_com($document)
    {
        $query = "SELECT
                    vt_venta.id_venta AS ID, 
                    vt_venta_vh.id_vh_venta AS ID_C, 
                    vt_venta.fecha_venta AS FECH, 
                    vt_venta.venta_ap AS AP, 
                    vt_venta_vh.marca AS M, 
                    vt_venta_vh.modelo AS MO, 
                    vt_venta_vh.version AS VER, 
                    vt_venta_vh.`year` AS A, 
                    vt_venta_vh.kilometraje AS K, 
                    vt_venta_vh.valor_venta AS VAL, 
                    vt_usuario.documento AS DOC, 
                    vt_usuario.nombre AS NOM, 
                    vt_usuario.apellido AS APE, 
                    vt_usuario.telefono AS TEL, 
                    vt_usuario.email AS EMA,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID_C LIMIT 1) AS IMG
                    FROM
                    vt_venta_vh
                    INNER JOIN vt_venta ON vt_venta.id_vh_venta = vt_venta_vh.id_vh_venta
                    INNER JOIN vt_usuario ON vt_venta.concesionario = vt_usuario.documento
                    WHERE vt_venta.concesionario = $document";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar las imagenes de un vehiculo
     * @param Int $id => Identificacion del vehiculo
     * @return Array
     */
    public function get_list_img_by_car($id)
    {
        $this->db->select("vt_img.id_img AS ID, vt_img.id_vh AS ID_C, vt_img.img AS IMG, vt_img.`name` AS NOM");
        $this->db->from("vt_img");
        $this->db->where("vt_img.id_vh ",$id);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar los vehiculos publicados por el usuario
     * @param Int $document => Documento del usuario
     * @return Array
     */
    public function get_list_car_us($document)
    {
        $query = "SELECT
                    vt_venta_vh.id_vh_venta AS ID,
                    vt_venta_vh.marca AS M,
                    vt_venta_vh.modelo AS MO,
                    vt_venta_vh.version AS V,
                    vt_venta_vh.`year` AS A,
                    vt_venta_vh.kilometraje AS K,
                    vt_venta_vh.placa AS P,
                    vt_venta_vh.valor_venta AS VAL,
                    vt_venta_vh.op_valor AS OP,
                    vt_venta_vh.activo AS EST,
                    vt_venta_vh.pintura AS PI,
                    vt_venta_vh.interior AS IN_T,
                    vt_venta_vh.mecanico AS ME,
                    vt_venta_vh.accidente AS ACC,
                    vt_venta_vh.choque AS CHO,
                    vt_venta_vh.combustible AS COM,
                    vt_venta_vh.descripcion AS DES,
                    (SELECT vt_img.img FROM vt_img WHERE vt_img.id_vh = ID LIMIT 1) AS IMG
                    FROM
                    vt_venta_vh
                    WHERE vt_venta_vh.documento = $document";
        $dd = $this->db->query($query);
        return $dd->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener un vehiculo en particular
     */
    public function get_car_by_user($document, $id)
    {
        $this->db->select("vt_venta_vh.id_vh_venta AS ID");
        $this->db->from("vt_venta_vh");
        $this->db->where("vt_venta_vh.documento",$document);
        $this->db->where("vt_venta_vh.id_vh_venta",$id);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite ob
     */
    public function get_data_by_order_id($order)
    {
        
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite mostrar la informacion de un vehiculo por id
     * @param Int $id => Identificacion del vehiculo
     * @return Object
     */
    public function get_info_car_by_car($id)
    {
        $this->db->select("vt_venta_vh.id_vh_venta AS ID, vt_venta_vh.marca AS MA, vt_venta_vh.modelo AS MO, vt_venta_vh.version AS V, vt_venta_vh.`year` AS A, vt_venta_vh.kilometraje AS K, vt_venta_vh.placa AS P, vt_venta_vh.valor_venta AS VA, vt_venta_vh.op_valor AS OP, vt_venta_vh.activo AS EST, vt_venta_vh.pintura AS PI, vt_venta_vh.interior AS IN_T, vt_venta_vh.mecanico AS ME, vt_venta_vh.accidente AS ACC, vt_venta_vh.choque AS CHO, vt_venta_vh.combustible AS COM, vt_venta_vh.descripcion AS DES, vt_usuario.documento AS DOC, vt_usuario.nombre AS NOM, vt_usuario.apellido AS APE, vt_usuario.telefono AS TEL");
        $this->db->from("vt_venta_vh");
        $this->db->join("vt_usuario","vt_venta_vh.documento = vt_usuario.documento");
        $this->db->where("vt_venta_vh.id_vh_venta",$id);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite saber si existen datos en la base de datos de facecolda
     * @return Int
     */
    public function get_info_data_base()
    {
        $this->db->select("COUNT(vehiculo_data.id_vh) AS CANT");
        $this->db->from("vehiculo_data");
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar los concesionarios interesados por vehiculo
     * @param Int $id => Identificacion del vehiculo
     * @return Array
     */
    public function get_list_con_inst($id)
    {
        $this->db->select("vt_usuario.documento AS NIT, vt_usuario.nombre AS NOM, vt_usuario.razon AS RA, vt_usuario.fech_registro AS FECH, vt_usuario.telefono AS TEL, vt_usuario.responsable AS RES, vt_vehiculo_interes.ofert_activa AS OP_A, vt_vehiculo_interes.ofert_gana AS OP_G, vt_usuario.email AS EMA, vt_vehiculo_interes.cant_oferta AS OF");
        $this->db->from("vt_usuario");
        $this->db->join("vt_vehiculo_interes","vt_vehiculo_interes.concesionario = vt_usuario.documento");
        $this->db->where("vt_vehiculo_interes.vh",$id);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite mostrar la oferta de un vehiculo por un concesionario
     * @param Int $id => Identificacion del vehiculo
     * @param Int $doc => Identificacion del concesionario
     * @return Object
     */
    public function get_ofer_ID_doc($id,$doc)
    {
        $this->db->select("vt_vehiculo_interes.cant_oferta AS OF, vt_vehiculo_interes.fecha_oferta AS FECH, vt_vehiculo_interes.ofert_activa AS EST");
        $this->db->from("vt_vehiculo_interes");
        $this->db->where("vt_vehiculo_interes.vh",$id);
        $this->db->where("vt_vehiculo_interes.concesionario",$doc);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los mensajes automaticos de whatsapp
     * @return Array
     */
    public function get_list_mens()
    {
        $this->db->select("vt_mensaje.id_mensaje_what AS ID, vt_mensaje.mensaje AS MENS");
        $this->db->from("vt_mensaje");        
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite traer todas las ventas aprovadas
     * 
     */
    public function get_list_invoce_complet()
    {
        $this->db->select("vt_venta.id_venta AS ID, vt_venta.fecha_venta AS FECH, vt_venta.codigo_venta AS COD, C.nombre AS CL_NOM, C.apellido AS CL_APE, C.telefono AS CL_TEL, C.email AS CL_EMA, COMP.nombre AS COMP_NOM, COMP.telefono AS COMP_TEL, COMP.email AS COMP_EMA, vt_venta.venta_ap AS AP_VT");
        $this->db->from("vt_venta");
        $this->db->join("vt_usuario AS C","vt_venta.vendedor = C.documento");
        $this->db->join("vt_usuario AS COMP","vt_venta.concesionario = COMP.documento");
        $this->db->where("vt_venta.venta_ap",1);    
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite traer todas las ventas canceladas
     * 
     */
    public function get_list_invoce_cancel()
    {
        $this->db->select("vt_venta.id_venta AS ID, vt_venta.token AS COD, vt_venta.codigo_venta AS COD_V, C.nombre AS CL_NOM, C.apellido AS CL_APE, C.telefono AS CL_TEL, C.email AS CL_EMA, COMP.nombre AS COMP_NOM, COMP.telefono AS COMP_TEL, COMP.email AS COMP_EMA, vt_venta.venta_ap AS AP_VT, vt_venta.fech_cancel AS FECH, vt_venta.motivo AS MV");
        $this->db->from("vt_venta");
        $this->db->join("vt_usuario AS C","vt_venta.vendedor = C.documento");
        $this->db->join("vt_usuario AS COMP","vt_venta.concesionario = COMP.documento");
        $this->db->where("vt_venta.venta_ap",2);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite mostrar la informacion de una venta cancelada
     */
    public function get_info_cancel($cod)
    {
        $this->db->select("vt_venta.id_venta AS ID, vt_venta.token AS COD, vt_venta.codigo_venta AS COD_V, C.nombre AS CL_NOM, C.apellido AS CL_APE, C.telefono AS CL_TEL, C.email AS CL_EMA, COMP.nombre AS COMP_NOM, COMP.telefono AS COMP_TEL, COMP.email AS COMP_EMA, vt_venta.venta_ap AS AP_VT, vt_venta.fech_cancel AS FECH, vt_venta.motivo AS MV");
        $this->db->from("vt_venta");
        $this->db->join("vt_usuario AS C","vt_venta.vendedor = C.documento");
        $this->db->join("vt_usuario AS COMP","vt_venta.concesionario = COMP.documento");
        $this->db->where("vt_venta.venta_ap",2);
        $this->db->where("vt_venta.codigo_venta",$cod);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite traer la informacion de la factura
     */
    public function get_invoce_by_cod($cod)
    {
        $this->db->select("vt_venta_vh.id_vh_venta AS ID_V, vt_venta.id_venta AS ID, vt_venta.fecha_venta AS FECH, vt_venta.token AS COD, vt_venta.codigo_venta AS COD_V, COMP.nombre AS COMP_NOM, COMP.apellido AS COMP_APE, COMP.telefono AS COMP_TEL, COMP.email AS COMP_EMA, COMP.direccion AS DIR, COMP.ciudad AS CID, vt_venta.venta_ap AS AP_VT, vt_venta_vh.marca AS MA, vt_venta_vh.modelo AS MO, vt_venta_vh.version AS V, vt_vehiculo_interes.cant_oferta AS VAL, vt_venta_vh.placa AS PA, COMP.documento AS ID_COMP");
        $this->db->from("vt_venta");        
        $this->db->join("vt_usuario AS COMP","vt_venta.concesionario = COMP.documento");
        $this->db->join("vt_venta_vh","vt_venta.id_vh_venta = vt_venta_vh.id_vh_venta");
        $this->db->join("vt_vehiculo_interes","vt_vehiculo_interes.concesionario = COMP.documento");
        $this->db->where("vt_venta.venta_ap",1);
        $this->db->where("vt_venta.codigo_venta",$cod);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Permite traer la el listado de marcas a purgar
     */
    public function get_list_purg()
    {
        $this->db->select("vt_purga.id_purgar AS ID, vt_purga.marca AS M, (SELECT COUNT(vehiculo_data.marca) FROM vehiculo_data WHERE vehiculo_data.marca = vt_purga.marca) AS CONT");
        $this->db->from("vt_purga");
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar una marca para su purga
     */
    public function set_register_pur($data = array())
    {
        return $this->db->insert('vt_purga',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Función que permite registrar un nuevo vehículo al sistema
     * @param Array $data => Datos del vehiculo
     * @return Int
     */
    public function set_register_car($data = array())
    {
        $this->db->insert('vt_venta_vh',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar una nueva oferta
     * @param Array $data => Informacion de la oferta a registrar
     * @return Int
     */    
    public function set_register_oferta($data = array())
    {
        return $this->db->insert('vt_vehiculo_interes',$data);
    }    

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar una nueva venta
     */
    public function set_register_venta($data = array())
    {
        return $this->db->insert('vt_venta',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar imagenes por vehiculo
     * @param Array $data => Listado de las imagenes a insertar
     * @return Int
     */
    public function set_register_img($data = array())
    {
        return $this->db->insert('vt_img',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar vehiculos en la base de datos
     * @param Array $data => Array con la informacion de los vehiculos
     * @return Int
     */
    public function set_register_vehiculo($data = array())
    {
        $this->db->insert('vehiculo_data',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar la fecha y valores en la base de datos
     * @param Array $data => Array con la informacion de los vehiculos
     * @return Int
     */
    public function set_register_fechavalor($data = array())
    {
        return $this->db->insert('fecha_valor',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar una oferta ganadora
     */
    public function set_regis_ofert_g($id, $data = array())
    {
        $this->db->where("vt_vehiculo_interes.id_vh_interes", $id);
        return $this->db->update('vt_vehiculo_interes',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que cancela las otras ofertas al seleccionar una ganadora
     */
    public function set_regis_cancel_all_ofert($id, $data = array())
    {
        $this->db->where("vt_vehiculo_interes.vh", $id);
        return $this->db->update('vt_vehiculo_interes',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite guardar la configuracion de los mensajes automaticos de whatsapp
     * @param Int $id => Identificacion del mensaje
     * @param Array $data => Datos del mensaje
     * @return Int
     */
    public function set_register_mens($id,$data=array())
    {
        $this->db->where("vt_mensaje.id_mensaje_what", $id);
        return $this->db->update('vt_mensaje',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar una notificacion por vehiculo
     * @param Array $data => Informacion con las notificacion
     * @return Int
     */
    public function set_register_notificacion($data = array())
    {
        return $this->db->insert('vt_notificacion',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Permite marcar una notificacion como vista o no
     */
    public function set_update_notificacion($id, $data = array())
    {
        $this->db->where("vt_notificacion.id_vh", $id);
        return $this->db->update('vt_notificacion',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar la informacion de una oferta
     */
    public function set_update_ofert($id, $data = array())
    {
        $this->db->where('vt_vehiculo_interes.id_vh_interes', $id);
        return $this->db->update('vt_vehiculo_interes',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar los datos de un vehiculo
     * @param Int $id => Identificacion del vehiculo
     * @param Array $data => Informacion a actualizar
     * @return Int
     */
    public function set_update_car($id,$data = array())
    {
        $this->db->where("vt_venta_vh.id_vh_venta", $id);
        return $this->db->update('vt_venta_vh',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar los estado de una venta
     */
    public function set_update_venta($orderId, $data = array())
    {
        $this->db->where('vt_venta.codigo_venta', $orderId);
        return $this->db->update('vt_venta',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar un vehiculo de forma completa de la base de datos
     * @param Int $id => Identificacion del vehiculo
     * @return Int
     */
    public function set_delete_car($id)
    {
        $this->db->where("vt_venta_vh.id_vh_venta", $id);
        return $this->db->delete('vt_venta_vh');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar una oferta de un concecionario
     * @param Int $id => Identificacion del vehiculo
     * @param Int $doc => Identificacion del concesionario
     * @return Int
     */
    public function set_delete_oferta($id,$doc)
    {
        $this->db->where("vt_vehiculo_interes.vh",$id);
        $this->db->where("vt_vehiculo_interes.concesionario",$doc);
        return $this->db->delete('vt_vehiculo_interes');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar todas las imagenes con referente a un vehiculo
     * @param Int $id => Identificacion del vehiculo
     * @return Int
     */
    public function set_detele_img($id)
    {
        $this->db->where('vt_img.id_vh',$id);
        return $this->db->delete('vt_img');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar toda la base de datos de facecolda
     * @return Int
     */
    public function set_delete_fasecolda()
    {
        return $this->db->empty_table('vehiculo_data');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite depurar la base de datos
     */
    public function set_delete_fasecolda_depu()
    {
        $this->db->like('vehiculo_data.modelo','MOTOCICLETA');
        $this->db->or_like('vehiculo_data.modelo','MOTOCARRO');
        $this->db->or_like('vehiculo_data.modelo','REMOLQUE');
        return $this->db->delete('vehiculo_data');
    }


    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar una marca de la tabla de purga
     */
    public function set_delete_marca_purg($id)
    {
        $this->db->where('vt_purga.id_purgar',$id);
        return $this->db->delete('vt_purga');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar una marca con su registro de facecolda
     */
    public function set_delete_marca_facecolda($marca)
    {
        $this->db->where('vehiculo_data.marca',$marca);
        return $this->db->delete('vehiculo_data');
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar una oferta por orderId
     */
    public function set_delete_vt_interes_by_orderId($orderId)
    {
        $this->db->where('vt_vehiculo_interes.codigo_venta',$orderId);
        return $this->db->delete('vt_vehiculo_interes');
    }
    
}
