var base_url;

$(document).ready(function () {

    $('#val').hide();

    base_url = $("#base_url").val();

    $('.select2').select2()

    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    fun_get_info_db();


    $("#Año").change(function () {
        const año = $("#Año").val();
        if (año == 0) {
            return;
        }
        $.ajax({
            type: "GET",
            url: base_url + 'list/marca',
            data: { fecha: año },
            dataType: "json",
            success: function (data) {
                if (!data.error) {
                    $("#Marca").empty();
                    $("#Marca").append('<option value="0">- Seleccione una opción -</option>');
                    $("#Modelo").empty();
                    $("#Modelo").append('<option value="0">- Seleccione una opción -</option>');
                    $("#Version").empty();
                    $("#Version").append('<option value="0">- Seleccione una opción -</option>');
                    $.each(data.Marca, function (i, item) {
                        var marca = item.F;
                        $("#Marca").append('<option value="' + marca + '">' + marca + '</option>');
                    });
                    $("#Marca").prop('disabled', false);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.Mens
                    })
                }
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.'
                })
            }
        });
    });

    $("#Marca").change(function () {
        const año = $("#Año").val();
        const marca = $("#Marca").val();
        if (año == 0) {
            return;
        }
        if (marca == 0) {
            return;
        }
        $.ajax({
            type: "GET",
            url: base_url + 'list/modelo',
            data: { fecha: año, marca: marca },
            dataType: "json",
            success: function (data) {
                if (!data.error) {
                    $("#Modelo").empty();
                    $("#Modelo").append('<option value="0">- Seleccione una opción -</option>');
                    $("#Version").empty();
                    $("#Version").append('<option value="0">- Seleccione una opción -</option>');
                    $.each(data.Modelo, function (i, item) {
                        $("#Modelo").append('<option value="' + item.F + '">' + item.F + '</option>');
                    });
                    $("#Modelo").prop('disabled', false);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.Mens
                    })
                }
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.'
                })
            }
        });
    });

    $("#Modelo").change(function () {
        const año = $("#Año").val();
        const marca = $("#Marca").val();
        const modelo = $("#Modelo").val();
        if (año == 0) {
            return;
        }
        if (marca == 0) {
            return;
        }
        if (modelo == 0) {
            return;
        }
        $.ajax({
            type: "GET",
            url: base_url + 'list/version',
            data: { fecha: año, marca: marca, modelo: modelo },
            dataType: "json",
            success: function (data) {
                if (!data.error) {
                    $("#Version").empty();
                    $("#Version").append('<option value="0">- Seleccione una opción -</option>');
                    $.each(data.Version, function (i, item) {
                        $("#Version").append('<option value="' + item.F + '">' + item.F + '</option>');
                    });
                    $("#Version").prop('disabled', false);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.Mens
                    })
                }
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.'
                })
            }
        });
    });

    $("#Version").change(function () {
        const año = $("#Año").val();
        const marca = $("#Marca").val();
        const modelo = $("#Modelo").val();
        const version = $("#Version").val();
        if (año == 0) {
            return;
        }
        if (marca == 0) {
            return;
        }
        if (modelo == 0) {
            return;
        }
        if (version == 0) {
            return;
        }
        $.ajax({
            type: "GET",
            url: base_url + 'list/valor',
            data: { fecha: año, marca: marca, modelo: modelo, version: version },
            dataType: "json",
            success: function (data) {
                if (!data.error) {
                    $('#va').html("<h4>El valor aproximado de venta es de: " + new Intl.NumberFormat('es-MX').format(data.Valor) + "</h4>");
                    $('#val').show(800);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.Mens
                    })
                }
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.'
                })
            }
        });
    });
});


function fun_get_info_db() {
    $.ajax({
        type: "GET",
        url: base_url + 'admin/import/list',
        dataType: "json",
        success: function (data) {
            if (!data.error) {
                $("#Version").append('<option value="0">- Seleccione una opción -</option>').prop('disabled', true);
                $("#Marca").append('<option value="0">- Seleccione una opción -</option>').prop('disabled', true);
                $("#Modelo").append('<option value="0">- Seleccione una opción -</option>').prop('disabled', true);
                $("#Año").append('<option value="0">- Seleccione una opción -</option>');
                $.each(data.Fecha, function (i, item) {
                    $("#Año").append('<option value=' + item.F + '>' + item.F + '</option>');
                });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: data.Mens
                })
            }
        },
        error: function () {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.'
            })
        }
    });
}