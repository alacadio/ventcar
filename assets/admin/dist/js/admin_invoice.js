var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $("#cancel_vnt").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Cancelando la venta",
      text: "Se está realizando la cancelación de esta venta, por favor espere un momento.",
      showConfirmButton: false,    
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Cancelación de la venta",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#example1").DataTable({
    responsive: true,
  });

  $("#example2").DataTable({
    responsive: true,
  });
});


function modal_cancel_vent(id) {  
  $("#btn_init_c").show();
  $("#cod_vt").val(id);
  $("#mot").hide();
  $("#btn_cancel").hide();
  $("#modal-danger").modal();
}

function modal_info_cancel(id) { 
    $.ajax({
      url: base_url + 'admin/invoice/cancel/info',
      type: "post",
      dataType: "json",
      data: {cod: id},
      success: function (data) {
        console.log(data);
        $('#fech').text(data.Mens.FECH);
        $('#inputVendedor').val(data.Mens.CL_NOM + ' ' + data.Mens.CL_APE);
        $('#inputComprador').val(data.Mens.COMP_NOM);
        $('#mov_cancel').text(data.Mens.MV);
        $('#inputVendedorC').val(data.Mens.CL_TEL);
        $('#inputCompradorC').val(data.Mens.COMP_TEL);
        $('#cancel_info').modal();
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
}

function modal_view_mot() {
  $("#btn_init_c").hide(300);
  $("#mot").show(500);
  $("#btn_cancel").show(500);
}


function fun_contact(id) {
    var num;
    if(id === 1){
        alert(1)
        num = $('#inputVendedorC').val();
    }else{
        alert(2)
        num = $('#inputCompradorC').val();
    }
  var whatsapp =
    "https://api.whatsapp.com/send?phone=" + 57 + num + "&text=Hola, de parte del equipo de ventcar queremos hablar sobre los motivos de la cancelación la venta generada";
  window.open(whatsapp, "_blank");
  return false;
}