<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Usuario concesionario
     * 
     * @var object
     */
    protected $user;

    // --------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ConModel');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        if ($this->session->userdata('login')) {            
            redirect(base_url().'ingreso');
        }
        $js = array('js' => 2 );
        $this->load->view("layout/venta_init/header",$js);
        $this->load->view("user/registro_us_vt");
        $this->load->view("layout/venta_init/footer");
    }

    public function perfil()
    {
        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') != 3){
                redirect(base_url().'ingreso');
            }
        } else {
            redirect(base_url().'ingreso');
        }

        $user = $this->UserModel->get_user_by_document($this->session->userdata('doc'));

        $car = $this->ConModel->get_list_car_us($this->session->userdata('doc'));

        $count_ven = 0;

        $count_p = 0;

        foreach ($car as $v) {
            
            if($v->EST == 2){
                $count_ven = $count_ven + 1;
            }

            if($v->EST == 1){
                $count_p = $count_p + 1;
            }

        }

        $data = array(
            'id' => $user->DOC,
            'nombre' => $user->NOM,
            'apellido' => $user->APE,
            'ema' => $user->EMA,
            'cell' => $user->TEL,
            'car' => $car,
            'publicado' => $count_p,
            'vendido' => $count_ven
        );

        $this->load->view("layout/user/header",$data);
        $this->load->view("layout/user/sidebar");
        $this->load->view("user/perfil_us");
        $this->load->view("layout/user/footer");

    }

    public function perfil_carro()
    {
        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') != 3){
                redirect(base_url().'ingreso');
            }
        } else {
            redirect(base_url().'ingreso');
        }

        $user = $this->UserModel->get_user_by_document($this->session->userdata('doc'));

        $id = $this->input->get("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        $interes = $this->ConModel->get_list_con_inst($id);

        $data = array(
            'id' => $user->DOC,
            'nombre' => $user->NOM,
            'apellido' => $user->APE,
            'ema' => $user->EMA,
            'cell' => $user->TEL,
            'car' => $car,
            'img' => $img,
            'inte' => $interes,
            'num' => $this->UserModel->get_num_adm()->TEL
        );

        $this->load->view("layout/user/header", $data);
        $this->load->view("layout/user/sidebar");
        $this->load->view("user/perfil_car_us");
        $this->load->view("layout/user/footer");
    }

    // Funciones
    public function get_info_car()
    {
        $id = $this->input->post("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        if($car){
            $data = array('error' => false, 'Mens' => $car, 'img' => $img);
            echo json_encode($data);
        }else{
            $data = array('error' => true, 'Mens' => 'Disculpe pero se presento un error al tratar de cargar la información, por favor trate más tarde');
            echo json_encode($data);
        }
    }

    public function us_act()
    {
        $inputDocumento = $this->input->post('inputDocumento');
        $inputNombre = $this->input->post('inputNombre');
        $inputApellido = $this->input->post('inputApellido');
        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');
        $inputUser = $this->input->post('inputUser');
        $inputPass = $this->input->post('inputPass');

        $data = array(
           'nombre' => $inputNombre,
           'apellido' => $inputApellido,
           'telefono' => $inputCelular,
           'direccion' => $inputDireccion,
           'ciudad' => $inputCiudad,
           'email' => $inputEmail
        );

        $login = array();

        $user = $this->UserModel->get_user_by_document($inputDocumento);

        $result = $this->UserModel->set_update_user($inputDocumento, $data);

        if($result){
            if(!empty($_POST['inputPass'])){
                $inputPass = md5($inputPass);
                $login = array(
                    'usuario' => $inputUser,
                    'pass' => $inputPass
                );
            }else{
                $login = array(
                    'usuario' => $inputUser
                );
            }
            $result_l = $this->UserModel->set_update_login($inputDocumento, $login);
            if($result_l){
                $data = array('error' => false, 'Mens' => "¡¡Datos del usuario actualizados de forma exitosa!!");
                echo json_encode($data);
            }else{
                $data = array('error' => true, 'Mens' => "Se presente un error al actualizar los datos de ingreso al sistema");
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => "Disculpe, se presento un error al tratar de actualizar los datos, por favor intente más tarde");
            echo json_encode($data);
        }
    }

    public function registro()
    {
        $inputDocumento = $this->input->post('inputDocumento');
        $inputNombre = $this->input->post('inputNombre');
        $inputApellido = $this->input->post('inputApellido');
        $inputCelular = $this->input->post('inputCelular');
        $inputDireccion = $this->input->post('inputDireccion');
        $inputCiudad = $this->input->post('inputCiudad');
        $inputEmail = $this->input->post('inputEmail');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta = $this->input->post('inputValorVenta');
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');
        $inputFile1 = $this->input->post('inputFile1');
        $inputEstado = $this->input->post('estado');

        $numero = rand(40,80);
        $token = bin2hex(random_bytes(($numero - (50 % 2)) / 2));

        $data = array(
            'documento' => $inputDocumento,
            'nombre' => $inputNombre,
            'apellido' => $inputApellido,
            'telefono' => $inputCelular,
            'direccion' => $inputDireccion,
            'ciudad' => $inputCiudad,
            'email' => $inputEmail,
            'tipo_us' => 3,
            'token' => $token,
            'estado' => 1
        );

        if($inputEstado != 1){
            $doc = $this->UserModel->get_user_by_emadoc($inputDocumento,$inputEmail);
            if($doc){
                if($doc->EMA == $inputEmail){
                    $dato = array('error' => true, 'Mens' => 'La dirección de email ya se encuentra registrada, por favor trate con otra');
                    echo json_encode($dato);
                    return;
                }
                if($doc->DOC == $inputDocumento){
                    $dato = array('error' => true, 'Mens' => 'El documento de usuario ya se encuentra registrado, por favor pruebe con otro');
                    echo json_encode($dato);
                    return;
                }
            }
        }else{
            $inputDocumento = $this->session->userdata('doc');
        }

        $result = 0;

        if($inputEstado != 1){
            $result = $this->UserModel->set_registro($data);
        }else{
            $result = 1;
        }

        if($result){

            $pass = $this->claveAleatoria();

            $clave = md5($pass);

            $emailData = array('nombre' => $inputNombre, 'apellido' => $inputApellido, 'contraseña' => $pass );

            $login = array(
                'id_login' => $inputDocumento, 
                'usuario' => $inputDocumento, 
                'pass' => $clave
            );

            $log = 0;

            if($inputEstado != 1){
                $log = $this->UserModel->set_insert_login($login);
            }else{
                $log = 1;
            }

            if($log){

                $op_valor = 0;

                if($inputValorVenta != 0 || $inputValorVenta != null){
                    $op_valor = 1;
                }else{
                    $inputValorVenta = $inputValorpromediodeventa;
                }

                $vh = array(
                    'marca' => $inputMarca,
                    'modelo' => $inputModelo,
                    'version' => $inputVersion,
                    'year' => $inputAño,
                    'kilometraje' => $inputKilometraje,
                    'placa' => $inputPlaca,
                    'valor_venta' => $inputValorVenta,
                    'op_valor' => $op_valor,
                    'documento' => $inputDocumento,
                    'activo' => 0,
                    'pintura' => $radio1,
                    'interior' => $radio2,
                    'mecanico' => $radio3,
                    'accidente' => $radio6,
                    'choque' => $radio4,
                    'combustible' => $radio5,
                    'descripcion' => $inputDesc,
                    'user' => 2
                );
                
                $carro = $this->ConModel->set_register_car($vh);

                $count_file = count($_FILES['inputFile1']['name']);

                if($carro){
                    if($count_file > 1){
                        $ruta = "./assets/public/img/";

                        $this->load->library('upload');
                        
                        $uploadData = array();
                        $config = array();
                        $config['upload_path']          = $ruta;
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['overwrite']     = FALSE;

                        $count_file = count($_FILES['inputFile1']['name']);

                        for ($i=0; $i < $count_file; $i++) { 
                            
                            $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                            $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                            $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                            $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                            $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('userFile'))
                            {

                            }else{
                                $fileData = $this->upload->data();
                                $uploadData[$i]['name'] = $fileData['file_name'];
                                $uploadData[$i]['path'] = $fileData['file_path'];
                                $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                                $uploadData[$i]['id_vh'] = $carro;
                            }
                        }        

                        if(empty($uploadData)){
                            //Insert file information into the database
                            $dato = array('error' => false, 'Mens' => $uploadData);
                            echo json_encode($dato);
                            return;
                        }

                        if($count_file != count($uploadData)){
                            $dato = array('error' => false, 'Mens' => $uploadData);
                            echo json_encode($dato);
                            return;
                        }

                        $cont = 0;

                        $img = array();

                        for ($i=0; $i < count($uploadData); $i++) { 
                            $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                            $insert_img = $this->ConModel->set_register_img($img);
                            $cont = $cont + 1;
                        }

                        $dato = array('doc' => $inputDocumento, 'est' => 1, 'car' => 3, 'login' => true, 'token' => $token);
                        $this->session->set_userdata($dato);

                        if($cont == count($uploadData)){
                            
                            $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!! En breve recibirá un email.');
                            echo json_encode($dato);
                        }else{
                            $dato = array('error' => false, 'Mens' => 'El usuario y el vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                            echo json_encode($dato);
                        }
                    }else{
                        $imagen = array(
                            'name' => 'default.png',
                            'img' => './assets/public/logos/originalhorizontal.svg',
                            'id_vh' => $carro
                        );

                        $insert_img = $this->ConModel->set_register_img($imagen);

                        $dato = array('doc' => $inputDocumento, 'est' => 1, 'car' => 3, 'login' => true, 'token' => $token);
                        $this->session->set_userdata($dato);

                        $body = $this->load->view('emails/email_registro',$emailData,true);

                        $email = $this->send_email($inputEmail,$body,'Registro exitoso');



                        if($insert_img){
                            if($email){
                                $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!! En breve recibirá un email.');
                                echo json_encode($dato);
                            }else{
                                $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                                echo json_encode($dato);
                            }
                        }else{
                            $dato = array('error' => false, 'Mens' => 'El usuario y el vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                            echo json_encode($dato);
                        }
                        
                    }
                }else{
                    $dato = array('error' => true, 'Mens' => 'El usuario fue registrado de forma exitosa, pero el vehículo NO, por favor proceda a eliminar el nuevo usuario y trate de registrar nuevamente o trate completar el registro del vehículo en la ventana de actualización información de la publicación.');
                    echo json_encode($dato);
                }

            }else{
                $dato = array('error' => true, 'Mens' => 'El usuario fue registrado de forma exitosa, pero no los datos de login ni del vehículo, por favor trate de eliminar el usuario y proceda a realizar el registro nuevamente, tenga en cuenta la información a registrar para no volver a presentar este error.');
                echo json_encode($dato);
            }

        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de registrar el usuario, por favor verifique la información suministrada y/o la conexión a internet.');
            echo json_encode($dato);
        } 
        
    }

    public function act_post()
    {
        $id = $this->input->post('id_v');
        $inputMarca = $this->input->post('inputMarcaU');
        $inputModelo = $this->input->post('inputModeloU');
        $inputVersion = $this->input->post('inputVersionU');
        $inputAño = $this->input->post('inputAñoU');
        $inputKilometraje = $this->input->post('inputKilometrajeU');
        $inputPlaca = $this->input->post('inputPlacaU');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVentaU');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventaU');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventaU');
        $inputValorVenta = $this->input->post('inputValorVentaU');
        $radio1 = $this->input->post('radio1U');
        $radio2 = $this->input->post('radio2U');
        $radio3 = $this->input->post('radio3U');
        $radio6 = $this->input->post('radio6U');
        $radio4 = $this->input->post('radio4U');
        $radio5 = $this->input->post('radio5U');
        $inputDesc = $this->input->post('inputDesc');

        $data = array(
            'marca' => $inputMarca,
            'modelo' => $inputModelo,
            'version' => $inputVersion,
            'year' => $inputAño,
            'kilometraje' => $inputKilometraje,
            'placa' => $inputPlaca,
            'valor_venta' => $inputValorVenta,
            'pintura' => $radio1,
            'interior' => $radio2,
            'mecanico' => $radio3,
            'accidente' => $radio6,
            'choque' => $radio4,
            'combustible' => $radio5,
            'descripcion' => $inputDesc
        );

        $img = $this->ConModel->get_list_img_by_car($id);

        $ext = $_FILES['inputFile1']['name'][0];

        if($ext != null){
            $count_img = count($img);

            $cont = 0;

            foreach ($img as $v) {
                if(!unlink($v->IMG)){                
                    $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                    echo json_encode($dato);
                    return;
                    break;
                }
                $cont = $cont + 1;
            }

            $delete_img = $this->ConModel->set_detele_img($id);

            if($delete_img){
                $ruta = "./assets/public/img/";

                $this->load->library('upload');
                
                $uploadData = array();
                $config = array();
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']     = FALSE;

                $count_file = count($_FILES['inputFile1']['name']);

                for ($i=0; $i < $count_file; $i++) { 
                    
                    $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile'))
                    {

                    }else{
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['path'] = $fileData['file_path'];
                        $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                        $uploadData[$i]['id_vh'] = $id;
                    }
                }        

                if(empty($uploadData)){
                    //Insert file information into the database
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                if($count_file != count($uploadData)){
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                $cont = 0;

                $img = array();

                for ($i=0; $i < count($uploadData); $i++) { 
                    $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                    $insert_img = $this->ConModel->set_register_img($img);
                    $cont = $cont + 1;
                }

                $result = $this->ConModel->set_update_car($id,$data);

                if($result){
                    $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                    echo json_encode($dato);
                }
            }
        }else{
            $result = $this->ConModel->set_update_car($id,$data);

            if($result){
                $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                echo json_encode($dato);
            }else{
                $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                echo json_encode($dato);
            }
        }
    }

    /**
     * Genera una nueva contraseña
     */
    private function claveAleatoria()
    {
        $longitud = 15;
        $opcLetra = true;
        $opcNumero = true;
        $opcMayus = true;
        $opcEspecial = false;

        $letras = "abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales = "|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == true) {
            $listado .= $letras;
        }

        if ($opcNumero == true) {
            $listado .= $numeros;
        }

        if ($opcMayus == true) {
            $listado .= $letrasMayus;
        }

        if ($opcEspecial == true) {
            $listado .= $especiales;
        }

        for ($i = 1; $i <= $longitud; $i++) {
            $caracter = $listado[rand(0, strlen($listado) - 1)];
            $password .= $caracter;
            $listado = str_shuffle($listado);
        }
        return $password;
    }

    /**
     * Función que permite realizar el envios de correos electrónicos
     * @param String $email => Direccion email del usuario
     * @param String $body => Cuerpo del correo en html and php
     * @param String $asunto => Mensaje del correo
     * @return Int
     */
    private function send_email($email,$body,$asunto)
    {
        $config['protocol']     = $this->config->item('email_protocol');
        $config['smtp_host']    = $this->config->item('email_smtp_host');
        $config['smtp_port']    = $this->config->item('email_smtp_port');
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $this->config->item('email_smtp_user');
        $config['smtp_pass']    = $this->config->item('email_smtp_pass');
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; // o html
        $config['validation']   = TRUE; // para validar el correo electronico 

        $this->email->initialize($config);

        $this->email->from('notificaciones_revista@revistajaveriana.org.co', 'Revista Javeriana');
        $this->email->to($email);
        $this->email->subject($asunto);
        $this->email->message($body);

        return $this->email->send();
    }
    
}
