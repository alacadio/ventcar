var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $("#new_cons_amd").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando el concesionario, por favor espere...",
      showConfirmButton: false,
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
            showConfirmButton: false,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#edt_cons_amd").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text:
        "Se está actualizando la información del concesionario, por favor espere...",
        showConfirmButton: false,
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
            showConfirmButton: false,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#example1").DataTable({
    responsive: true,
  });

  $("#example2").DataTable({
    responsive: true,
  });
});

function modal_act_cons(id) {
  $.ajax({
    url: base_url + "admin/cons/id",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      if (!data.error) {
        $("#inputNIT2").val(data.Mens.DOC);
        $("#inputNombreConsecionaria2").val(data.Mens.NOM);
        $("#inputRazon2").val(data.Mens.RAZON);
        $("#inputAsesor2").val(data.Mens.RES);
        $("#inputDireccion2").val(data.Mens.DIR);
        $("#inputCiudad2").val(data.Mens.CIUD);
        $("#inputTelefono2").val(data.Mens.TEL);
        $("#inputEmail2").val(data.Mens.EMA);
        $("#inputUser").val(data.Mens.US);

        $("#modal_act").modal();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function modal_del_cons(id) {
  $.ajax({
    url: base_url + "admin/cons/id",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        $("#id_con").val(data.Mens.DOC);
        $("#modal-danger").modal();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}


function fun_del_cons(pa) {
  var id = $("#id_con").val();
  $.ajax({
    url: base_url + "admin/cons/del",
    type: "post",
    dataType: "json",
    data: { id: id, pa: pa },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        var men = "";
        if (pa == 1) {
          men = "Eliminación";
        } else {
          men = "Des habilitación";
        }
        Swal.fire({
          icon: "success",
          title: men,
          text: data.Mens,
        });

        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}