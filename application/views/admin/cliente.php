      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Clientes</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">clientes</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de los clientes</h3>
              <div class="card-tools">
                <a href="#" onclick="new_client()" class="btn btn-primary">Registrar nuevo cliente</a>
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>Documento</th>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Fecha Registro</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($list_cliente as $v): ?>
                      <tr>
                        <td>
                          <?php echo $v->DOC;?>
                        </td>
                        <td><?php echo $v->NOM; ?></td>
                        <td><?php echo $v->APE; ?></td>
                        <td><?php echo $v->TEL; ?></td>
                        <td><?php echo $v->EMA; ?></td>
                        <td>
                          <?php echo $v->FECH; ?>
                        </td>
                        <td>
                        <a target="framename" href="<?php echo base_url();?>admin/cliente/perfil?id=<?php echo $v->DOC; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                        -
                        <?php if($v->EST != 1): ?>
                          <a href="#" class="btn btn-success" onclick="habilitar_us('<?php echo $v->DOC;?>')" data-toggle="tooltip" data-placement="top" title="Habilitar" ><i class="fas fa-check-square"></i></a>
                        <?php else: ?>
                          <a href="#" class="btn btn-danger" onclick="modal_del_cons('<?php echo $v->DOC;?>')" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fas fa-trash-alt"></i></a>
                        <?php endif; ?>
                        </td>
                      </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>

        </div><!-- /.container-fluid -->
        
      </div>      
      <!-- /.content -->


      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Registro de clientes</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>admin/cliente/registro" method="post" id="regs_us_admin" enctype="multipart/form-data">

                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Datos de contacto</h3>
                    <div class="card-tools">
                      <p>Campos obligatorios: *</p>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputDocumento">Documento*</label>
                          <input type="number" class="form-control" name="inputDocumento" id="inputDocumento" placeholder="Documento" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputNombre">Nombre*</label>
                          <input type="text" class="form-control" name="inputNombre" id="inputNombre" placeholder="Nombre" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputApellido">Apellido</label>
                          <input type="text" class="form-control" name="inputApellido" id="inputApellido" placeholder="Apellido">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputCelular">Celular*</label>
                          <input type="number" class="form-control" name="inputCelular" id="inputCelular" placeholder="Celular" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputDireccion">Dirección*</label>
                          <input type="text" class="form-control" name="inputDireccion" id="inputDireccion" placeholder="Direccion" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputCiudad">Ciudad*</label>
                          <input type="text" class="form-control" name="inputCiudad" id="inputCiudad" placeholder="Ciudad" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputEmail">Email*</label>
                          <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Descripción del vehículo</h3>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAño" id="inputAño" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputMarca" name="inputMarca" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputModelo" name="inputModelo" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="inputVersion" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="number" class="form-control" name="inputKilometraje" id="inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlaca" id="inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5" value="1" checked>
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5" value="0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMenorValorVenta">Menor valor venta</label>
                          <input type="number" class="form-control" name="inputMenorValorVenta" id="inputMenorValorVenta" placeholder="Menor valor venta" disabled value="0">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="hidden" name="inputValorpromediodeventa" id="pro">
                          <label for="inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="number" class="form-control" name="inputValorp" id="inputValorpromediodeventa" placeholder="Valor promedio de venta" disabled value="0">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="number" class="form-control" name="inputMejorvalordeventa" id="inputMejorvalordeventa" placeholder="Mejor valor de venta" disabled value="0">
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVenta" id="inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" checked value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" checked>
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>                    
                    <hr>
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Imagen #1*</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text" id="">Upload</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Registrar</button>
                  </div>
                </div>
                          
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal-danger">
        <div class="modal-dialog modal-xl">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar o desactivar usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
              <h4>Podrá eliminar o desactivar un vendedor en específico, solo tenga en cuenta que:</h4>
              <ul>
                <li>
                    <p>Si da click en eliminar, los datos del vendedor, así como sus registros quedaran permanentemente eliminados y esta acción no podrá deshacerse</p>
                </li>
                <li>
                    <p>Si da click en deshabilitar, los datos se mantendrán solo el vendedor no podrá ingresar nuevamente hasta ser habilitado nuevamente</p>
                </li>
              </ul>
              <a href="#" onclick="fun_del_cons(1)" class="btn btn-danger">Eliminar</a>
              <a href="#" onclick="fun_del_cons(2)" class="btn btn-danger">Deshabilitar</a>
            </div>
            <div class="modal-footer justify-content-between">            
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->