/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : ventcar_db_sell

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-03 16:35:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fecha_valor
-- ----------------------------
DROP TABLE IF EXISTS `fecha_valor`;
CREATE TABLE `fecha_valor` (
  `id_date` int(11) NOT NULL AUTO_INCREMENT,
  `id_vh` int(11) NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `valor` int(11) NOT NULL,
  PRIMARY KEY (`id_date`),
  KEY `fk_fecha_vehiculo` (`id_vh`),
  CONSTRAINT `fk_fecha_vehiculo` FOREIGN KEY (`id_vh`) REFERENCES `vehiculo_data` (`id_vh`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=337186 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vehiculo_data
-- ----------------------------
DROP TABLE IF EXISTS `vehiculo_data`;
CREATE TABLE `vehiculo_data` (
  `id_vh` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(50) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `version` varchar(100) NOT NULL,
  `combustible` varchar(50) DEFAULT NULL,
  `tipo_caja` varchar(50) DEFAULT NULL,
  `cilindraje` varchar(50) DEFAULT NULL,
  `nacionalidad` varchar(50) DEFAULT NULL,
  `transmision` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_vh`)
) ENGINE=InnoDB AUTO_INCREMENT=113590 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_cambio
-- ----------------------------
DROP TABLE IF EXISTS `vt_cambio`;
CREATE TABLE `vt_cambio` (
  `id_cambio` int(11) NOT NULL AUTO_INCREMENT,
  `documento` int(20) NOT NULL,
  `fech_cam` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_cambio`),
  KEY `fk_cambio_usuario` (`documento`),
  CONSTRAINT `fk_cambio_usuario` FOREIGN KEY (`documento`) REFERENCES `vt_usuario` (`documento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_img
-- ----------------------------
DROP TABLE IF EXISTS `vt_img`;
CREATE TABLE `vt_img` (
  `id_img` int(11) NOT NULL AUTO_INCREMENT,
  `id_vh` int(11) NOT NULL,
  `img` text NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id_img`),
  KEY `fk_imagen_vehiculo` (`id_vh`),
  CONSTRAINT `fk_imagen_vehiculo` FOREIGN KEY (`id_vh`) REFERENCES `vt_venta_vh` (`id_vh_venta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_login
-- ----------------------------
DROP TABLE IF EXISTS `vt_login`;
CREATE TABLE `vt_login` (
  `id_login` int(20) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `pass` text NOT NULL,
  PRIMARY KEY (`id_login`),
  CONSTRAINT `fk_login_usuario` FOREIGN KEY (`id_login`) REFERENCES `vt_usuario` (`documento`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_mensaje
-- ----------------------------
DROP TABLE IF EXISTS `vt_mensaje`;
CREATE TABLE `vt_mensaje` (
  `id_mensaje_what` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext DEFAULT NULL,
  PRIMARY KEY (`id_mensaje_what`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_notificacion
-- ----------------------------
DROP TABLE IF EXISTS `vt_notificacion`;
CREATE TABLE `vt_notificacion` (
  `id_interes` int(11) NOT NULL AUTO_INCREMENT,
  `notificacion` tinyint(4) DEFAULT 1,
  `notificacion_vista` tinyint(4) DEFAULT 0,
  `fecha` date DEFAULT curdate(),
  `id_vh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_interes`),
  KEY `fk_notificacion_vehiculo` (`id_vh`),
  CONSTRAINT `fk_notificacion_vehiculo` FOREIGN KEY (`id_vh`) REFERENCES `vt_venta_vh` (`id_vh_venta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_purga
-- ----------------------------
DROP TABLE IF EXISTS `vt_purga`;
CREATE TABLE `vt_purga` (
  `id_purgar` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(100) NOT NULL,
  PRIMARY KEY (`id_purgar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_usuario
-- ----------------------------
DROP TABLE IF EXISTS `vt_usuario`;
CREATE TABLE `vt_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `documento` int(20) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `direccion` text DEFAULT NULL,
  `ciudad` varchar(50) NOT NULL,
  `fech_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `razon` text DEFAULT NULL,
  `token` text NOT NULL,
  `tipo_us` tinyint(4) NOT NULL,
  `estado` tinyint(2) NOT NULL,
  `responsable` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`,`documento`),
  KEY `documento` (`documento`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_vehiculo_interes
-- ----------------------------
DROP TABLE IF EXISTS `vt_vehiculo_interes`;
CREATE TABLE `vt_vehiculo_interes` (
  `id_vh_interes` int(11) NOT NULL AUTO_INCREMENT,
  `concesionario` int(20) NOT NULL,
  `vh` int(11) NOT NULL,
  `cant_oferta` int(20) NOT NULL,
  `fecha_oferta` datetime DEFAULT current_timestamp(),
  `ofert_activa` tinyint(4) NOT NULL DEFAULT 1,
  `ofert_gana` tinyint(4) NOT NULL DEFAULT 0,
  `nofit` tinyint(4) DEFAULT 0,
  `token` longtext DEFAULT NULL,
  `codigo_venta` longtext DEFAULT NULL,
  PRIMARY KEY (`id_vh_interes`,`concesionario`),
  KEY `fk_interes_consecionario` (`concesionario`),
  KEY `fk_interes_vehiculo` (`vh`),
  CONSTRAINT `fk_interes_consecionario` FOREIGN KEY (`concesionario`) REFERENCES `vt_usuario` (`documento`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interes_vehiculo` FOREIGN KEY (`vh`) REFERENCES `vt_venta_vh` (`id_vh_venta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_venta
-- ----------------------------
DROP TABLE IF EXISTS `vt_venta`;
CREATE TABLE `vt_venta` (
  `id_venta` int(11) NOT NULL AUTO_INCREMENT,
  `id_vh_venta` int(11) NOT NULL,
  `vendedor` int(20) NOT NULL,
  `concesionario` int(20) NOT NULL,
  `fecha_venta` datetime NOT NULL DEFAULT current_timestamp(),
  `venta_ap` tinyint(2) NOT NULL DEFAULT 1,
  `token` longtext DEFAULT NULL,
  `codigo_venta` longtext DEFAULT NULL,
  `motivo` longtext DEFAULT NULL,
  `fech_cancel` date DEFAULT NULL,
  PRIMARY KEY (`id_venta`,`id_vh_venta`),
  KEY `fk_venta_usuario` (`vendedor`),
  KEY `fk_venta_consecionario` (`concesionario`),
  KEY `fk_venta_vehiculo` (`id_vh_venta`),
  CONSTRAINT `fk_venta_consecionario` FOREIGN KEY (`concesionario`) REFERENCES `vt_usuario` (`documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_usuario` FOREIGN KEY (`vendedor`) REFERENCES `vt_usuario` (`documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_vehiculo` FOREIGN KEY (`id_vh_venta`) REFERENCES `vt_venta_vh` (`id_vh_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for vt_venta_vh
-- ----------------------------
DROP TABLE IF EXISTS `vt_venta_vh`;
CREATE TABLE `vt_venta_vh` (
  `id_vh_venta` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `version` varchar(100) NOT NULL,
  `year` int(11) NOT NULL,
  `kilometraje` int(20) NOT NULL,
  `placa` varchar(10) NOT NULL,
  `valor_venta` int(20) NOT NULL,
  `op_valor` tinyint(2) NOT NULL,
  `documento` int(20) NOT NULL,
  `activo` tinyint(2) NOT NULL DEFAULT 0,
  `pintura` int(11) DEFAULT NULL,
  `interior` int(11) DEFAULT NULL,
  `mecanico` int(11) DEFAULT NULL,
  `accidente` tinyint(4) DEFAULT NULL,
  `choque` tinyint(4) DEFAULT NULL,
  `combustible` tinyint(4) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `user` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_vh_venta`),
  KEY `fk_vehiculo_usuario` (`documento`),
  CONSTRAINT `fk_vehiculo_usuario` FOREIGN KEY (`documento`) REFERENCES `vt_usuario` (`documento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
