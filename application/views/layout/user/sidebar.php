<!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url();?>assets/public/logos/iconoblanco.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">Ventcar</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?php echo base_url();?>assets/admin/dist/img/perfil-del-usuario.png" class="img-circle elevation-2">
          </div>
          <div class="info">
            <?php if($this->session->userdata('login')): ?>
              <a href="#" class="d-block"><?php echo $nombre." - ".$apellido;?></a>
            <?php else: ?>
              <a href="#" class="d-block">Registro y venta</a>
            <?php endif; ?>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <?php if($this->session->userdata('login')): ?>
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Registro Venta
                  </p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url();?>" class="nav-link">
                <i class="nav-icon fas fa-car-alt"></i>
                <p>
                  Vehículos en venta
                </p>
              </a>
            </li>
            <?php else: ?>
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Venta Vehículo
                  </p>
                </a>
              </li>
            <?php endif; ?>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">