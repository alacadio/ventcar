
    <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Factura de venta</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Starter Page</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <img src="<?php echo base_url();?>assets/public/logos/icono.svg" width="50px" height="50px" alt=""> <?php echo $date_invo->NOM; ?>
                    <small class="float-right">Date: <?php echo date("Y-m-d"); ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong><?php echo $date_invo->NOM; ?></strong><br>
                    Nit: <?php echo $date_invo->NIT; ?><br>
                    <?php echo $date_invo->DIR; ?><br>
                    <?php echo $date_invo->CID; ?><br>
                    Phone: <?php echo $date_invo->TEL; ?><br>
                    Email: <?php echo $date_invo->EMA; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <?php if($invoice->COMP_APE): ?>
                        <strong><?php echo $invoice->COMP_NOM.' - '.$invoice->COMP_APE; ?></strong><br>
                    <?php else: ?>
                        <strong><?php echo $invoice->COMP_NOM; ?></strong><br>
                    <?php endif; ?>
                    <?php echo $invoice->DIR; ?><br>
                    <?php echo $invoice->CID; ?><br>
                    Phone: <?php echo $invoice->COMP_TEL; ?><br>
                    Email: <?php echo $invoice->COMP_EMA; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice #<?php echo $invoice->ID; ?></b><br>
                  <br>
                  <b>Order ID:</b> <?php echo $invoice->COD_V; ?><br>
                  <b>Fecha facturación:</b> <?php 
                        $newDate = date("d/m/Y", strtotime($invoice->FECH));
                        echo $newDate; 
                  ?><br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>                      
                      <th>Marca</th>
                      <th>Modelo</th>
                      <th>Version</th>
                      <th>Placa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td><?php echo $invoice->MA; ?></td>
                      <td><?php echo $invoice->MO; ?></td>
                      <td><?php echo $invoice->V; ?></td>
                      <td><?php echo $invoice->PA; ?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead">Payment Methods:</p>
                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Monto a pagar <?php echo $newDate; ?></p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>$<?php echo $invoice->VAL; ?></td>
                      </tr>
                      <tr>
                        <th>IVA (19%)</th>
                        <td>$<?php echo $iva/100; ?></td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>$<?php echo $iva; ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="<?php echo base_url();?>invoice?id=<?php echo $invoice->COD_V; ?>" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                  <a href="#" onclick="modal_email('<?php echo $invoice->COD_V; ?>')" class="btn btn-primary float-right"><i class="fas fa-paper-plane"></i> Enviar Factura</a>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->    
    </div>      
    <!-- /.content -->


    <div class="modal fade" id="send_email">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Enviar factura</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="card">
                  <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">Datos del comprador a enviar</h3>
                    <div class="card-tools">
                    </div>
                  </div>
                  <div class="card-body">
                      <form action="<?php echo base_url()?>admin/invoice/send" method="post" id="frm_send_email">
                          <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="inputCod">Codigo Factura</label>
                                <input type="text" class="form-control" name="inputCod" id="inputCod" placeholder="Codigo Factura" required>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="inputCliente">Cliente</label>
                                <input type="text" class="form-control" name="inputCliente" id="inputCliente" placeholder="Cliente" required>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="inputEmial">Email</label>
                                <input type="text" class="form-control" name="inputEmial" id="inputEmial" placeholder="Email">
                              </div>
                            </div>
                          </div>
                          <input type="hidden" name="cod" id="cod">
                          <br>
                          <button type="submit" class="btn btn-success">Enviar factura</button>
                        </form>
                  </div>
                </div>                  
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal --> 

    