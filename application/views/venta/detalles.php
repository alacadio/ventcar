
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Descripción del vehículo</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <input type="hidden" name="est" id="est" value="<?php echo $est; ?>">
      <input type="hidden" name="oferta" id="oferta" value="0">
      <div class="container">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Perfil del carro en venta</h3>
              <div class="card-tools">
                <?php if($this->session->userdata('login')): ?>
                  <?php if($my_car == 1): ?>
                    <h3>Mi vehículo</h3>                    
                  <?php else: ?>
                    <a href="#" class="btn btn-primary" id="id_oferta" onclick="modal_oferta()" data-toggle="tooltip" data-placement="top" title="Ofertar por este vehículo">Ofertar</a>                    
                  <?php endif; ?>
                <?php else: ?>
                  <a href="#" class="btn btn-primary" id="id_oferta" onclick="fun_oferta_G('<?php echo $est;?>','<?php echo $num;?>','<?php echo base_url();?>','3','N/A')" data-toggle="tooltip" data-placement="top" title="Ofertar por este vehículo">Contactar</a>
                <?php endif; ?>
              </div>
            </div>
            <div class="card-body">
              <h4>Detalles del vehículo</h4>
              <hr>
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <label for="inputAño">Año</label>
                    <input type="number" class="form-control" id="inputAño" placeholder="Año" value="<?php echo $car->A;?>">
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                      <label for="inputMarc">Marca</label>
                      <input type="text" class="form-control" id="inputMarc" placeholder="Marca" value="<?php echo $car->MA;?>">
                  </div>
                </div>
                <div class="col-4">
                  <div class="form-group">
                      <label for="inputModelo">Modelo</label>
                      <input type="text" class="form-control" id="inputModel" placeholder="Modelo" value="<?php echo $car->MO;?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-3">
                  <div class="form-group">
                      <label for="inputVersion">Versión</label>
                      <input type="text" class="form-control" id="inputModel" placeholder="Modelo" value="<?php echo $car->V;?>">
                    </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="inputKilometraje">Kilometraje</label>
                    <input type="number" class="form-control" id="inputKilometraje" placeholder="Kilometraje" value="<?php echo $car->K;?>">
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                    <label for="inputPlaca">Placa</label>
                    <input type="text" class="form-control" id="inputPlaca" placeholder="Placa" value="<?php echo $car->P;?>">
                  </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Tipo combustible </label>
                        <div class="form-check">
                          <?php 
                              $var1 = ''; $var2 = '';
                              if($car->COM == 1){
                                $var1 = 'checked';
                              }else{
                                $var2 = 'checked';
                              }
                          ?>
                          <input type="radio" name="radio5" value="1" <?php echo $var1; ?>>
                          <label class="form-check-label">Gasolina</label>
                          <input type="radio" name="radio5" value="0" <?php echo $var2; ?>>
                          <label class="form-check-label">Diesel</label>                          
                          
                          
                        </div>
                    </div>
                </div>
              </div>
               <hr>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                      <center>
                        <input type="hidden" id="_val" value="<?php echo $car->VA;?>">
                        <h3 for="inputValor">Valor descrito por el vendedor</h3>
                        <h3 id="cop" ></h3>
                      </center>
                  </div>
                </div>                
              </div>

              <hr>
              <h4>Descripción del vehículo</h4>
              <hr>
              <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="">Califique el estado interior de su vehículo:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->IN_T){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio2" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="">Califique el estado fisico general:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->ME){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio3" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
              </div>
              <hr>
              <center>
                <h4>Imágenes del vehículo</h4>
              </center>
              <hr>
              <center>
                <div class="row">
                      <?php
                        foreach ($img as $v):
                      ?>
                        <div class="">
                          <div class="post-thumbnail product-image-thumb">
                              <a href="<?php echo base_url().$v->IMG;?>" class="with-caption image-link">
                                  <img src="<?php echo base_url().$v->IMG;?>" alt="" width="250px">
                              </a>
                          </div>
                        </div>
                      <?php endforeach; ?>
                  </div>
                </div>
              </center>
          </div>
        <!-- /.row -->
        <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Vehículos relacionados</h3>
              <div class="card-tools">
              </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <?php if($list): ?>
                      <?php foreach ($list as $v):?>
                        <div class="col-4">
                          <div class="card card-primary card-outline">
                              <div class="card-header">
                                  <h5 class="card-title m-0"> <?php echo $v->M.' '.$v->MO; ?></h5>
                              </div>
                              <div class="card-body">
                                  <center>
                                    <img src="<?php echo base_url().$v->IMG;?>" alt="" width="200px" height="120px">
                                    <p></p>
                                    <p><span><?php echo $v->A; ?> | <?php echo $v->K; ?></span> Km</p>
                                    <h3>$<?php echo number_format($v->VAL); ?></h3>
                                  </center>
                              </div>
                              <div class="card-footer">
                                <center>  
                                  <a href="<?php echo base_url();?>vehiculo/detalles?id=<?php echo $v->ID; ?>" data-toggle="tooltip" data-placement="top" title="Detalles" class="btn btn-primary">Ver detalles vehículos</a>
                                </center>
                              </div>
                          </div>
                      </div>
                      <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
          </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ofertar por este vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                      <label for="inpuoValor" id="text_oferta">Valor a ofertar</label>
                      <input type="number" class="form-control" id="inputValor" name="inputValor" placeholder="Valor a ofertar" >
                  </div>
                </div>
              </div>
            <div class="modal-footer justify-content-between">
                <a href="#" onclick="fun_ofertar()" id="btn_oferta" class="btn btn-info">Ofertar</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 