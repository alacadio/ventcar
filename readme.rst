###################
Vencart project
###################

Este es el proyecto de ventcar, el cual constituye de un sistema administrable para el administrador del sitio y 
para los concesionarios inscrito a nuestro sistema, además, contara con una vista de registro de vehículos para 
los clientes que estén dispuesto a vender su vehículo en nuestro sistema.

*******************
Información de lanzamiento
*******************
Este repositorio encontrara todo lo relacionado con la parte administrativa del sitio web, 
para más información poner en contacto con `Masifica <https://masifica.co/>`_.


**************************
Información de instalación
**************************
En la carpeta sql se encuentra el archivo sql, en el mysql crear una base de datos llama ventcar
La configuración de la base de datos, se encuentra en application/config/database.php


**************************
Registro de cambios y nuevas características
**************************

Puede encontrar una lista de todos los cambios para cada versión en el registro de cambios

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.


************
Installation
************

Please see the `installation section <https://codeigniter.com/user_guide/installation/index.html>`_
of the CodeIgniter User Guide.

*******
License
*******

Este software es de uso exclusivo de `Masifica <https://masifica.co/>`_ y de `Ventcar <https://ventcar.com>`_, 
quienes son los unicos propietarios exclusivos del uso, manipulación, modificación y distribución de este software, 
queda totalmente PROHIBIDA su comercialización, distribución, manipulación, modificación, redistribución, 
por partes de terceros, para más información `Masifica <https://masifica.co/>`_, `Ventcar <https://ventcar.com>`_.

************
Recursos
************

Para trabajar en este aplicativo, se trabajará bajo la siguiente estructura:

-	Todos los archivos de vistas se encuentran dentro de la siguiente ruta ‘application/views/carpeta alusiva al controlador’
-	La carpeta layout cuentan con la estructura principal del html que son: header, sidebar, footer, para cada controlador existe una carpeta alusiva a su nombre que cuenta con estos tres archivos mencionados anterior mente.
-	Los archivos de layout no ha de ser modificados amenos que se tenga que hacer alguna de las siguientes modificaciones: agregar estilo css, agregar un archivo js, modificación del footer, modificación del sidebar, modificación del header, todas las modificaciones que se realicen tienen que ser agregadas en el archivo changelog.rst ubicado en ‘raiz_sistema/log/changelog.rst’
-	Todas las modificaciones tienen que ser agregada en un orden especifico, este orden varía en función a la operación que se esta modificando y la versión del sistema actual

El changelog.rst es un archivo crucial para el correcto seguimiento de errores y actualizaciones a futuro, por eso, es de vital importancia el correcto cumplimiento de esta estructura, para evitar fallos y dolores de cabezas al momento de agregar una nueva funcionalidad o corregir fallos.

La estructura a manejar es la siguiente:

::

    ##########
    Change Log
    ##########

    Version X.Y.Z
    =============

    Fecha de lanzamiento: no publicado

    -  General Changes

        - En este apartado se especifican los cambios generales del sistemas, de una forma más desglosada y general

    -  Libraries

        - Todas las librerías que, se utilizan, se eliminaron, se actualizaron, se crearon, se modificaron

    -  Helpers
        - Los helpers son todas las funcionalidades que permiten ayudar en la construcción del sitio, como la carga de documentos, el redireccionamiento, etc. Se tiene que especificar que fue modificado, agregado, eliminado, descartado, desactualizado, etc.
    
    - Views 
        - Se tiene que especificar todas las modificaciones echas para las vistas del sistema, como la eliminación, actualización, agregar nuevo elemento, desuso de un elemento.

Para establecer cuales han sido los cambios principales, se ha establecido el estándar de log change names, los cuales nos permiten especificar que funcionalidad ha sido agregada, cual ha sido eliminada, cual estará en desuso, cual será obsoleta, la corrección de errores, y los casos de vulnerabilidad, ES DE IMPORTANCIA EL TOTAL CUMPLIMIENTO DE ESTO.

Para conocer más y leer para entender esto, entra en `ChangeLog <https://keepachangelog.com/es-ES/1.0.0/>`_.





***************
Reconocimiento
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.
