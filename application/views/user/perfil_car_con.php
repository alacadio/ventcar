      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Perfil del vehículo en venta</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Perfil</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <div class="content">
        <div class="container">
          <div class="content-fluid">

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Perfil del carro en venta</h3>
              <div class="card-tools"></div>
            </div>
            <div class="card-body">
              <h4>Propietario actual</h4>
              <hr>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label for="inputNombre">Nombre</label>
                      <input type="text" class="form-control" id="inputNombre" placeholder="Nombre" value="<?php echo $car->NOM;?>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label for="inputApellido">Apellido</label>
                      <input type="text" class="form-control" id="inputApellido" placeholder="Apellido" value="<?php echo $car->APE;?>">
                  </div>
                </div>
              </div>
              <hr>
              <h4>Detalles del vehículo</h4>              
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="inputAño">Año</label>
                    <input type="number" class="form-control" id="inputAño" placeholder="Año" value="<?php echo $car->A;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Marca</label>
                      <input type="text" class="form-control" id="inputMarc" placeholder="Marca" value="<?php echo $car->MA;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputModelo">Modelo</label>
                      <input type="text" class="form-control" id="inputModel" placeholder="Modelo" value="<?php echo $car->MO;?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                      <label for="inputVersion">Versión</label>
                      <input type="text" class="form-control" id="inputModel" placeholder="Modelo" value="<?php echo $car->V;?>">
                    </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for="inputKilometraje">Kilometraje</label>
                    <input type="number" class="form-control" id="inputKilometraje" placeholder="Kilometraje" value="<?php echo $car->K;?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for="inputPlaca">Placa</label>
                    <input type="text" class="form-control" id="inputPlaca" placeholder="Placa" value="<?php echo $car->P;?>">
                  </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="">Tipo combustible </label>
                        <div class="form-check">
                          <?php 
                              $var1 = ''; $var2 = '';
                              if($car->COM == 1){
                                $var1 = 'checked';
                              }else{
                                $var2 = 'checked';
                              }
                          ?>
                          <input type="radio" name="radio5" value="1" <?php echo $var1; ?>>
                          <label class="form-check-label">Gasolina</label>
                          <input type="radio" name="radio5" value="0" <?php echo $var2; ?>>
                          <label class="form-check-label">Diesel</label>                          
                          
                          
                        </div>
                    </div>
                </div>
              </div>
               <hr>
              <h4>Valor venta</h4>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Valor descrito por el vendedor</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="<?php echo $car->VA;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Menor valor de venta (-20% Aprox)</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="8000000">
                  </div>
                </div>
                <div class="col-sm-4">
                <div class="form-group">
                      <label for="inputMarc">Mejor valor de venta (+10% Aprox)</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="15000000">
                  </div>
                </div>
              </div>

              <hr>
              <h4>Descripción del vehículo</h4>
              <hr>
              <div class="row">                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Califique el estado interior de su vehículo:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->IN_T){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio2" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Califique el estado fisico general:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->ME){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio3" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
              </div>
              
              <hr>
              <center>
                <h4>Imágenes del vehículo</h4>
              </center>
              <hr>
                <div class="row">
                    <?php
                      foreach ($img as $v):
                    ?>
                      <div class="col-sm-2 product-image-thumbs">
                        <div class="post-thumbnail product-image-thumb">
                            <a href="<?php echo base_url().$v->IMG;?>" class="with-caption image-link">
                                <img src="<?php echo base_url().$v->IMG;?>" alt="" width="250px">
                            </a>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
            </div>
          </div>         
        </div>
        </div>
      </div>



      <div class="modal fade" id="modal_act">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información del concesionario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url(); ?>concesionario/update" method="post" id="edt_cons_us">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Datos de contacto</h3>
                            <div class="card-tools">
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputNIT2">NIT*</label>
                                        <input type="number" class="form-control" id="inputNIT2" name="inputNIT2" placeholder="NIT" required value="" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputNombreConsecionaria2">Nombre Consecionaria*</label>
                                        <input type="text" class="form-control" id="inputNombreConsecionaria2" name="inputNombreConsecionaria2" placeholder="Nombre Consecionaria" required value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputRazon2">Razón Social*</label>
                                        <input type="text" class="form-control" id="inputRazon2" name="inputRazon2" placeholder="Razón Social" required value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputAsesor2">Asesor/Gerente*</label>
                                        <input type="text" class="form-control" id="inputAsesor2" name="inputAsesor2" placeholder="Asesor/Gerente" required value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputDireccion2">Direccion</label>
                                        <input type="text" class="form-control" id="inputDireccion2" name="inputDireccion2" placeholder="Direccion" required value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputCiudad2">Ciudad</label>
                                        <input type="text" class="form-control" id="inputCiudad2" name="inputCiudad2" placeholder="Ciudad" required value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputTelefono2">Celular</label>
                                        <input type="number" class="form-control" id="inputTelefono2" name="inputTelefono2" placeholder="Celular" required value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="inputEmail2">Email</label>
                                        <input type="email" class="form-control" id="inputEmail2" name="inputEmail2" placeholder="Email" required value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Datos de ingreso</h3>
                            <div class="card-tools">
                                <a href="#" class="btn btn-primary" onclick="fun_clave()" id="inputbtn">Restablecer contraseña</a>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="inputUser">Nombre de usuario</label>
                                <input type="text" class="form-control" name="inputUser" id="inputUser" placeholder="User" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="inputPass">Contraseña</label>
                                <input type="password" class="form-control" name="inputPass" id="inputPass" placeholder="Nueva contraseña">
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
                        </div>
                    </div>
                    <!-- /.Formulario -->
                </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->