<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener los valores para ingresar al sistema
     * @param String $us => Nombre de usuario
     * @param Text $pass => Contraseña del usuario
     * @return Object.
     */
    public function get_login($us,$pass)
    {
        $this->db->select('vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.tipo_us AS TIP');
        $this->db->from('vt_usuario');
        $this->db->join("vt_login", "vt_login.id_login = vt_usuario.documento");
        $this->db->where('vt_login.usuario',$us);
        $this->db->where('vt_login.pass',$pass);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que obtiene la cantidad total de cambios de contraseña realizado
     * por el usuario
     * @param Int $documento => Documento del usuario
     * @return Array
     */
    public function get_cant_cambio($documento)
    {
        $this->db->select('vt_cambio.id_cambio AS ID, vt_cambio.documento AS DOC, vt_cambio.fech_cam AS FECH');
        $this->db->from('vt_cambio');
        $this->db->where('vt_cambio.documento',$documento);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener el numero de contacto del admin
     * @return Object
     */
    public function get_num_adm()
    {
        $this->db->select("vt_usuario.telefono AS TEL");
        $this->db->from("vt_usuario");
        $this->db->where("vt_usuario.id_usuario",1);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener un usuario por su token
     * @param String $token => Token del usuario
     * @return Object
     */
    public function get_user_by_token($token)    
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.nombre AS NOM, vt_usuario.apellido AS APE, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.razon AS RAZON, vt_usuario.responsable AS RES, vt_usuario.id_usuario AS ID, vt_usuario.apellido AS APE, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CIUD, vt_usuario.fech_registro AS FECH, vt_usuario.tipo_us AS TIP, vt_login.usuario AS US");
        $this->db->from("vt_usuario");        
        $this->db->join("vt_login","vt_login.id_login = vt_usuario.documento");     
        $this->db->where("vt_usuario.token",$token);
        $this->db->where("vt_usuario.estado",1);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Función que permite obtener la información del usuario por documento
     * @param Int $documento => Docuemnto del usuario
     * @return Object
     */
    public function get_user_by_document($documento)
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.nombre AS NOM, vt_usuario.apellido AS APE, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.razon AS RAZON, vt_usuario.responsable AS RES, vt_usuario.id_usuario AS ID, vt_usuario.apellido AS APE, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CIUD, vt_usuario.fech_registro AS FECH, vt_usuario.tipo_us AS TIP");
        $this->db->from("vt_usuario");
        $this->db->where("vt_usuario.documento",$documento);
        $this->db->where("vt_usuario.estado",1);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }   

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener el usuario ofertante por su oferta
     */
    public function get_user_by_id_vh($id_oft, $id_vh)
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.nombre AS NOM, vt_usuario.apellido AS APE, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS C, vt_usuario.responsable AS R, vt_usuario.razon AS RA");
        $this->db->from("vt_usuario");
        $this->db->join("vt_vehiculo_interes","vt_vehiculo_interes.concesionario = vt_usuario.documento");
        $this->db->where("vt_vehiculo_interes.id_vh_interes",$id_oft);
        $this->db->where("vt_vehiculo_interes.vh",$id_vh);
        $this->db->where("vt_usuario.estado",1);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //--------------------------------------------------------------------------- 
    /**
     * Funcion que permite obtener los datos de la empresa
     */
    public function get_admin_by_invoice()
    {
        $this->db->select("vt_usuario.documento AS NIT, vt_usuario.nombre AS NOM, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CID, vt_usuario.razon AS RAZ, vt_usuario.responsable AS RES");
        $this->db->from("vt_usuario");
        $this->db->where("vt_usuario.id_usuario",1);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Función que permite obtener un usuario por su email y documento
     * @param Int $doc => Docuemento del usuario
     * @param String $email => Email del usuario
     * @return Object
     */
    public function get_user_by_emadoc($doc,$email)
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.nombre AS NOM, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.razon AS RAZON, vt_usuario.responsable AS RES, vt_usuario.id_usuario AS ID, vt_usuario.apellido AS APE, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CIUD, vt_usuario.fech_registro AS FECH, vt_usuario.tipo_us AS TIP");
        $this->db->from("vt_usuario");        
        $this->db->where("vt_usuario.documento",$doc);
        $this->db->or_where('vt_usuario.email',$email);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los concesionarios de la base de datos
     * @return Array
     */
    public function get_list_con()
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.nombre AS NOM, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.razon AS RAZON, vt_usuario.responsable AS RES, vt_usuario.id_usuario AS ID, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CIUD, vt_usuario.fech_registro AS FECH, vt_usuario.tipo_us AS TIP");
        $this->db->from("vt_usuario");
        $this->db->where("vt_usuario.tipo_us",2);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite listar todos los usuarios vendedores de la base de datos
     * @return Array
     */
    public function get_list_ven()
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.nombre AS NOM, vt_usuario.telefono AS TEL, vt_usuario.email AS EMA, vt_usuario.razon AS RAZON, vt_usuario.responsable AS RES, vt_usuario.id_usuario AS ID, vt_usuario.apellido AS APE, vt_usuario.direccion AS DIR, vt_usuario.ciudad AS CIUD, vt_usuario.fech_registro AS FECH, vt_usuario.tipo_us AS TIP");
        $this->db->from("vt_usuario");
        $this->db->where("vt_usuario.tipo_us",3);
        return $this->db->get()->result();
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener un usuario en base a la direccion de correo
     * @param String $email => Email del usuario
     * @return Object
     */
    public function get_user_by_email($email)
    {
        $this->db->select("vt_usuario.email AS EMA");
        $this->db->from('vt_usuario');
        $this->db->where('vt_usuario.email',$email);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite obtener la informacion del vendedor por el vehiculo
     */
    public function get_user_by_vh($id)
    {
        $this->db->select("vt_usuario.documento AS DOC, vt_usuario.nombre AS NOM, vt_usuario.apellido AS APE, vt_usuario.estado AS EST, vt_usuario.token AS TOKEN, vt_usuario.tipo_us AS TIP");
        $this->db->from('vt_usuario');
        $this->db->join("vt_venta_vh","vt_venta_vh.documento = vt_usuario.documento");
        $this->db->where('vt_venta_vh.id_vh_venta',$id);
        $res = $this->db->get();
        if($res->num_rows() > 0){
            return $res->row();
        }else{
            return false;
        }
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar un nuevo usuario al sistema
     * @param Array $data => Array de objeto con la informacion del usuario
     * @return Int
     */
    public function set_registro($data = array())
    {
        return $this->db->insert("vt_usuario",$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar los datos de inicio de sesion de un usuario
     * @param Array $data => Array de objeto con la informacion del usuario
     * @return Int
     */
    public function set_insert_login($data = array())
    {
       return $this->db->insert('vt_login', $data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite registrar un nuevo cambio de contraseña del usuario
     * @param Array $data => Array con los datos del usuario
     * @return Int
     */
    public function set_insert_cambio($data = array())
    {
        return $this->db->insert('vt_cambio',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar las credenciales del usuario
     * @param Array $data => Array con los datos del usuario
     * @return Int
     */
    public function set_update_login($doc,$data = array())
    {
        $this->db->where("id_login", $doc);
        return $this->db->update('vt_login',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar la informacion de un usuario
     * @param Int $doc => Documento o Nit del usuario
     * @param Array $data => Array de la informacion del usuario
     * @return Int
     */
    public function set_update_user($doc,$data = array())
    {
        $this->db->where("vt_usuario.documento", $doc);
        return $this->db->update('vt_usuario',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite actualizar la informacion del adminstrador
     * @param Int $doc => Documento o Nit del usuario
     * @param Array $data => Array de la informacion del usuario
     * @return Int
     */
    public function set_update_amd($data = array())
    {
        $this->db->where("vt_usuario.id_usuario", 1);
        return $this->db->update('vt_usuario',$data);
    }

    //---------------------------------------------------------------------------
    /**
     * Funcion que permite eliminar un usaurio de la base de datos
     * @param Int $doc => Documento O Nit del usuario
     * @return Int
     */
    public function set_delete_user($doc)
    {
        $this->db->where("vt_usuario.documento", $doc);
        return $this->db->delete('vt_usuario');
    }
}
