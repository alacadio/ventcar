
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <input type="hidden" name="count_not" id="count_not" value="<?php echo $notificacion;?>">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Página de inicio</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Página de inicio</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3><?php echo count($list_car); ?></h3>

                  <p>Vehículos publicados</p>
                </div>
                <div class="icon">
                  <i class="fas fa-car-alt"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->   
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3><?php echo count($interes); ?></h3>

                  <p>Ventas realizadas</p>
                </div>
                <div class="icon">
                  <i class="fas fa-hand-holding-usd"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3><?php echo $venta_pos; ?></h3>

                  <p>Vehículos adquiridos</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-cart"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->                     
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3><?php echo $neg_venta; ?></h3>

                  <p>Ventas canceladas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>

          <div class="row">
              <div class="col-sm-12">
                <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Listado de vehículos registrados</h3>
                            <div class="card-tools">
                                <a href="#" onclick="modal_new_car()" class="btn btn-success">Publicar vehículo</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Año</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($list_car as $v): ?>
                                      <tr>
                                        <td>
                                          <img src="<?php echo base_url().$v->IMG; ?>" alt="" width="100px">
                                        </td>
                                        <td><?php echo $v->M; ?></td>
                                        <td><?php echo $v->MO; ?></td>
                                        <td><?php echo $v->A; ?></td>
                                        <td>
                                            <?php if($v->EST == 1 ): ?>
                                              <a href="#" class="badge bg-info">En venta</a>
                                            <?php elseif($v->EST == 0 ): ?>
                                              <a href="#" class="badge bg-danger">Pausado</a>
                                            <?php else: ?>
                                              <a href="#" class="badge bg-success">Vendido</a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                          <a target="framename" href="<?php echo base_url();?>concesionario/micar/perfil?id=<?php echo $v->ID;?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                                          -
                                          <a href="#" class="btn btn-warning" onclick="modal_act_car_us('<?php echo $v->ID; ?>')"><i class="fas fa-edit"></i></a>
                                          -
                                          <a href="#" class="btn btn-danger" onclick="modal_del_car('<?php echo $v->ID; ?>')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                      </tr>
                                  <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
              </div>
          </div>

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de compras realizadas</h3>
              <div class="card-tools">
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-striped">
                <thead>
                <tr>
                  <th>Fecha Venta</th>
                  <th>Marca</th>
                  <th>Modelo</th>                  
                  <th>Año</th>
                  <th>Kilometraje</th>
                  <th>Vendedor</th>
                  <th>Celular</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($ventas as $v): ?>
                      <tr>
                        <td>
                            <?php echo $v->FECH; ?>
                        </td>
                        <td><?php echo $v->M; ?></td>
                        <td><?php echo $v->MO; ?></td>
                        <td><?php echo $v->A; ?></td>
                        <td><?php echo $v->K; ?></td>
                        <td>
                          <?php echo $v->NOM." - ".$v->APE; ?>
                        </td>
                        <td><?php echo $v->TEL; ?></td>
                        <td>
                          <a target="framename" href="<?php echo base_url();?>admin/con/car/perfil?id=<?php echo $v->ID; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de vehículos interesados</h3>
              <div class="card-tools">
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>Marca</th>
                  <th>Modelo</th>                  
                  <th>Año</th>
                  <th>Fecha Oferta</th>
                  <th>Oferta Realizada</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($interes as $v): ?>
                      <tr>
                        <td><?php echo $v->MAR; ?></td>
                        <td><?php echo $v->MO; ?></td>
                        <td><?php echo $v->A; ?></td>
                        <td>
                          <?php echo $v->FECH ?>
                        </td>
                        <td><?php echo $v->OFE; ?></td>
                        <td>
                            <a target="framename" href="<?php echo base_url();?>concesionario/car/perfil?id=<?php echo $v->ID; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                            -
                            <a href="#" class="btn btn-danger" onclick="modal_delete_oferta('<?php echo $v->ID; ?>')" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fas fa-trash-alt"></i></a>
                        </td>
                      </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              
            </div>
            <!-- /.card-body -->
          </div>

        </div><!-- /.container-fluid -->
        
      </div>
      
      <!-- /.content -->   


      <div class="modal fade" id="modal_act">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información del concesionario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url(); ?>concesionario/update" method="post" id="edt_cons_us">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Datos de contacto</h3>
                            <div class="card-tools">
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputNIT2">NIT*</label>
                                        <input type="hidden" name="inputNIT2" id="NIT2">
                                        <input type="number" class="form-control" id="inputNIT2" name="inputNIT2" placeholder="NIT" required value="" disabled>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputNombreConsecionaria2">Nombre Consecionaria*</label>
                                        <input type="text" class="form-control" id="inputNombreConsecionaria2" name="inputNombreConsecionaria2" placeholder="Nombre Consecionaria" required value="">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputRazon2">Razón Social*</label>
                                        <input type="text" class="form-control" id="inputRazon2" name="inputRazon2" placeholder="Razón Social" required value="">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputAsesor2">Asesor/Gerente*</label>
                                        <input type="text" class="form-control" id="inputAsesor2" name="inputAsesor2" placeholder="Asesor/Gerente" required value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputDireccion2">Direccion</label>
                                        <input type="text" class="form-control" id="inputDireccion2" name="inputDireccion2" placeholder="Direccion" required value="">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputCiudad2">Ciudad</label>
                                        <input type="text" class="form-control" id="inputCiudad2" name="inputCiudad2" placeholder="Ciudad" required value="">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputTelefono2">Celular</label>
                                        <input type="number" class="form-control" id="inputTelefono2" name="inputTelefono2" placeholder="Celular" required value="">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="inputEmail2">Email</label>
                                        <input type="email" class="form-control" id="inputEmail2" name="inputEmail2" placeholder="Email" required value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Datos de ingreso</h3>
                            <div class="card-tools">
                                <a href="#" class="btn btn-primary" onclick="fun_clave()" id="inputbtn">Restablecer contraseña</a>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="inputUser">Nombre de usuario</label>
                                <input type="text" class="form-control" name="inputUser" id="inputUser" placeholder="User" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="inputPass">Contraseña</label>
                                <input type="password" class="form-control" name="inputPass" id="inputPass" placeholder="Nueva contraseña">
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
                        </div>
                    </div>
                    <!-- /.Formulario -->
                </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal-danger">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar la oferta</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
              <h4>Podrá eliminar la oferta realizada sobre el anterior vehículo, recuerde que esta operación no puede ser desecha una vez confirmada</h4>
              <h4>¿Desea eliminar la oferta realizada? </h4>
              <a href="#" onclick="fun_del_ofer()" class="btn btn-danger">Eliminar</a>
            </div>
            <div class="modal-footer justify-content-between">            
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal_pos_car">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Publicar nuevo vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>concesionario/registro/car" method="post" id="reg_car_cons" enctype="multipart/form-data">
                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Descripción del vehículo</h3>
                    <input type="hidden" name="id" id="id">
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAño" id="inputAño" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputMarca" name="inputMarca" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputModelo" name="inputModelo" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="inputVersion" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="number" class="form-control" name="inputKilometraje" id="inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlaca" id="inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5" value="1" id="radio_5_1">
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5" value="0" id="radio_5_0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <input type="hidden" name="inputValorpromediodeventa" id="pro">
                        <div class="form-group">
                          <label for="inputMenorValorVenta">Menor valor venta</label>
                          <input type="text" class="form-control" name="inputMenorValorVenta" id="inputMenorValorVenta" placeholder="Menor valor venta" disabled value="">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="text" class="form-control" id="inputValorpromediodeventa" placeholder="Valor promedio de venta" disabled value="">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="text" class="form-control" name="inputMejorvalordeventa" id="inputMejorvalordeventa" placeholder="Mejor valor de venta" disabled value="">
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVenta" id="inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" id="radio_2_1" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" id="radio_2_2" value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" id="radio_2_3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2" id="radio_2_4"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" id="radio_2_5" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2" id="radio_2_6"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" id="radio_2_7" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2" id="radio_2_8"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" id="radio_2_9" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2" id="radio_2_10"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1" id="radio_3_1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" id="radio_3_2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3" id="radio_3_3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4" id="radio_3_4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5" id="radio_3_5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6" id="radio_3_6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7" id="radio_3_7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8" id="radio_3_8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9" id="radio_3_9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10" id="radio_3_10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                      <p>Nota: la primera imagen en seleccionar será la primera en mostrar</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Seleccione el listado de imágenes</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text" id="">Upload</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Registrar</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal_delete_car">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar el vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
              <h4>Esta apunto de eliminar el vehículo publicado, esta acción no podrá ser desecha una vez completada</h4>
              <h4>¿Esta seguro que desea continuar con esta operación?</h4>
              <a href="#" onclick="fun_del_car()" class="btn btn-danger">Eliminar</a>
            </div>
            <div class="modal-footer justify-content-between">            
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal_act_car">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información del vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>admin/cons/act/car" method="post" id="act_pos_cli" enctype="multipart/form-data">
                <input type="hidden" name="id_v" id="id_v">
                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Descripción del vehículo</h3>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAño" id="Año" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="Marca" name="inputMarca" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="Modelo" name="inputModelo" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="Version" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="number" class="form-control" name="inputKilometraje" id="a_inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlaca" id="a_inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5" value="1" checked id="a_radio_5_1">
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5" value="0" id="a_radio_5_0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="a_inputMenorValorVenta">Menor valor venta</label>
                          <input type="text" class="form-control" name="inputMenorValorVenta" id="a_inputMenorValorVenta" placeholder="Menor valor venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="hidden" name="inputValorpromediodeventa" id="prod">
                          <label for="a_inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="text" class="form-control" name="inputValorpromediodeventa" id="a_inputValorpromediodeventa" placeholder="Valor promedio de venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="a_inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="text" class="form-control" name="inputMejorvalordeventa" id="a_inputMejorvalordeventa" placeholder="Mejor valor de venta" disabled>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVenta" id="a_inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">                      
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" id="a_radio_2_1" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" id="a_radio_2_2" value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" id="a_radio_2_3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2" id="a_radio_2_4"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" id="a_radio_2_5" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2" id="a_radio_2_6"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" id="a_radio_2_7" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2" id="a_radio_2_8"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" id="a_radio_2_9" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2" id="a_radio_2_10"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1" id="a_radio_3_1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" id="a_radio_3_2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3" id="a_radio_3_3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4" id="a_radio_3_4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5" id="a_radio_3_5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6" id="a_radio_3_6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7" id="a_radio_3_7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8" id="a_radio_3_8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9" id="a_radio_3_9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10" id="a_radio_3_10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                      <p>Nota: la primera imagen en seleccionar será la primera en mostrar</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Seleccione el listado de imágenes</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text" id="">Upload</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <center>
                      <h3>Listado de imágenes actuales</h3>
                    </center>
                    <hr>
                    <div class="row" id="id_img">
                      
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Actualizar datos</button>
                  </div>
                </div>
                          
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->