<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'E404';
$route['translate_uri_dashes'] = FALSE;

/**
 * Controlador Auth
 * 
 * Vistas
 */
$route['ingreso'] = "auth/index";
$route['registro'] = "auth/register";
$route['cambio'] = "auth/cambio_contrasena";
/**
 * Controlador Auth
 * 
 * Funciones
 */
$route['reestablecer'] = "auth/new_pass";
$route['login'] = "auth/login";
$route['registrar'] = "auth/set_register";
$route['logout'] = "auth/logout";
$route['clave'] = "auth/generar_clave";

/**
 * Controlador User
 * 
 * Vistas
 */
$route['concesionario/'] = "Concesionario/index";
$route['concesionario/perfil'] = "Concesionario/perfil";
$route['concesionario/car/perfil'] = "Concesionario/perfil_car";
$route['usuario'] = "user/index";
$route['usuario/perfil'] = "user/perfil";
$route['usuario/car/perfil'] = "user/perfil_carro";
$route['concesionario/micar/perfil'] = "concesionario/perfil_carro";
$route['concesionario/update'] = "Concesionario/update_cons";

/**
 * Controlador User
 * 
 * Funciones
 */
$route['usuario/registro'] = "user/registro";
$route['usuario/car/info'] = "user/get_info_car";
$route['usuario/car/act'] = "user/act_post";
$route['usuario/user/act'] = "user/us_act";
$route['concesionario/delete/of'] = "Concesionario/delete_oferta";
$route['concesionario/registro/car'] = "concesionario/registro";
$route['concesionario/delete/car'] = "concesionario/delete_post";
$route['admin/cons/act/car'] = "concesionario/act_post";


/**
 * Controlador Admin
 * 
 * Vistas
 */
 $route['admin/con'] = "admin/concesionario";
 $route['admin/con/perfil'] = "admin/concesionario_perfil";
 $route['admin/con/car/perfil'] = "admin/concesionario_carr";
 $route['admin/cliente'] = "admin/cliente";
 $route['admin/cliente/perfil'] = "admin/cliente_perfil";
 $route['admin/perfil'] = "admin/perfil";
 $route['admin/venta'] = "admin/venta";
 $route['admin/invoice'] = "admin/invoice";
 $route['admin/invoice/info'] = "admin/get_data_invoice";
 $route['admin/invoice/cancel/info'] = "admin/get_info_cancel";
 $route['invoice'] = "welcome/get_invoice";

/**
 * Contorlador Admin
 * 
 * Funciones
 */
$route['admin/new_cons'] = "admin/reg_con";
$route['admin/cons/id'] = "auth/obtener_por_id";
$route['admin/cons/act'] = "admin/act_cons";
$route['admin/cons/del'] = "admin/del_const";
$route['admin/cons/hab'] = "admin/act_us";
$route['admin/cons/des'] = "admin/post_des_act";
$route['admin/cons/act_pos'] = "admin/post_act";
$route['admin/cons/del_pos'] = "admin/delete_post";
$route['admin/cons/info/car'] = "admin/get_info_car";
$route['admin/cons/act/post'] = "admin/act_post";
$route['admin/cliente/registro'] = "admin/reg_client";
$route['admin/cliente/act'] = "admin/us_act";
$route['admin/admin/act'] = "admin/amd_act";
$route['admin/import'] = "admin/importxlxs";
$route['admin/registro/car'] = "admin/registro";
$route['admin/registro/notificacion'] = "auth/reg_noti";
$route['admin/mensaje/rg'] = "admin/act_mens";
$route['admin/oferta/lts'] = "admin/get_list_ofert";
$route['admin/oferta/reg'] = "admin/set_register_sale_user_anonimus";
$route['admin/oferta/acet'] = "admin/set_ofert";
$route['admin/info/client'] = "admin/get_info_user";
$route['admin/rgpur/marca'] = "admin/set_register_pur";
$route['admin/delpur/marca'] = "admin/set_delete_marca_purg";
$route['admin/deldb/marca'] = "admin/set_delete_marca_db";
$route['admin/cancelar/venta'] = "admin/set_cancel_venta";
$route['admin/invoice/send'] = "admin/send_invoice";

/**
 * Funcione de listado
 */
$route['admin/import/list'] = "auth/get_list_marca";
$route['list/marca'] = "auth/get_marca_by_fecha";
$route['list/modelo'] = "auth/get_modelo_by_fecha_marca";
$route['list/version'] = "auth/get_version_by_fecha_marca_modelo";
$route['list/valor'] = "auth/get_valor_by_all";
$route['list/marca/index'] = "welcome/get_list_marc";
$route['list/year/index'] = "welcome/get_list_year";
$route['list/model/index'] = "welcome/get_list_model";

/**
 * Controlador Welcome
*
* Vistas
*/
//$route['vehiculo/(:any)'] = "welcome/index";
$route['welcome/(:num)'] = "welcome/index";
$route['vehiculo/detalles'] = "welcome/detalles_vehiculo";
$route['calculadora'] = "welcome/calculador";
$route['lista/preliminar'] = "welcome/public_list";
$route['resultado'] = "buscar/resultado";
$route['resultado/(:any)'] = "buscar/resultado";

/**
 * Controlador Welcome
 * 
 * Funciones
*/
$route['vehiculo/detalles/ofer'] = "welcome/get_ofer_con";
$route['vehiculo/ofertar'] = "welcome/set_ofertar";




