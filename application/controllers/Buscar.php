<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buscar extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ConModel');
        $this->load->model('UserModel');
        $this->load->helper(array('form', 'url'));

        if($this->session->userdata('login')){
            $token = $this->session->userdata('doc');
            $this->user = $this->UserModel->get_user_by_document($token);
        }
    }

    public function resultado()
    {
        $user = 0;
        if($this->session->userdata('login')){
            $user = $this->user->TIP;
        }
        
        $check_val_10 = empty($_GET['check_val_10']) ? 0 : 10000000;
        $check_val_mx_4 = empty($_GET['check_val_mx_4']) ? 0 : 40000000;
        $check_val_m_4 = empty($_GET['check_val_m_4']) ? 0 : 41000000;
        
        $marca = $this->input->get('inputMarca');
        $modelo = $this->input->get('inputModelo');
        $año = $this->input->get('inputAño');
        $version = $this->input->get('inputVersion');
        $combustible = $this->input->get('inputCombustible');
        $inputKilometraje = $this->input->get('inputKilometraje');
        $radio5 = $this->input->get('radio5');



        $this->load->library('pagination');

        $cont = count($this->ConModel->get_num_pagina_bus($año, $marca, $modelo,$version,$combustible,$inputKilometraje, $user,$check_val_10,$check_val_mx_4,$check_val_m_4));

        $config['base_url'] = base_url().'resultado';
		$config['total_rows'] = $cont;
        $config['per_page'] = 9;
        $config["uri_segment"] = 2;

        //styling
        $config['num_links'] = 4;
        $config['use_page_numbers'] = true;
        $config['reuse_query_string'] = true;

        $config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		
		$config['first_tag_open'] = '<li class="page-item page-link">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		
		$config['prev_tag_open'] = '<li class="prev page-link"> ';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		
		$config['next_tag_open'] = '<li class="page-item page-link">';
		$config['next_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li class="page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page-item page-link">';
		$config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $post = $this->ConModel->get_busqueda_rapida($config['per_page'], $page, $año, $marca, $modelo,$version,$combustible,$inputKilometraje, $user,$check_val_10,$check_val_mx_4,$check_val_m_4);

        $data = array(
            'est' => 0,
            'doc' => $this->session->userdata('token'),
            'count' => $cont,
            'js' => 1,
            'script' => 'fil1.js',
            'lt' => $post
        );
		$this->load->view("layout/venta_init/header",$data);
        $this->load->view("venta/index");
        $this->load->view("layout/venta_init/footer");

    }


    public function prueba()
    {
        $this->session->sess_destroy();

        if($this->session->userdata('bs')){
            echo 'parama <br>';
            $param = $this->session->userdata('param');
            foreach ($param as $key => $v) {
                echo $key.'<==>'.$v.'<br>';
            }
        }else{
            $url_actual = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            $components = parse_url($url_actual,PHP_URL_QUERY);
            if($components){
                parse_str($components, $results);
                print_r($results); 
                $dato = array('bs' => true, 'param' => $results);
                $this->session->set_userdata($dato);
            }else{
                echo 'hola';
            }
        }
    }

}
