$(document).ready(function () {
    $('#btn_resv').click(function () {
        $('#modal-email').modal();
        $('#rsult_post').hide();
        $('#post_regst').hide();
    });
    $('#topSearch').click(function () {
        alert('Esta es una busqueda rapida por titulo de publicación.');
    });
    $('#frm_resv_serv').submit(function (ev) {
        $('#regst_post').hide();
        $('#post_regst').show();
        ev.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            dataType: 'json',
            ansyc: true,
            data: new FormData(this), // formData en vez de serialize
            contentType: false, // para que jQuery no setee un contentType
            processData: false, // para que jQuery no transforme data en un string
            success: function (data) {
                if (!data.error) {
                    $('#txtr').hide();
                    $('#rsult_post').show();
                    $('#rsult_post').text(data.Mens);
                    //window.setTimeout('location.reload()', 1500);
                } else {
                    $('#txtr').hide();
                    $('#rsult_post').show();
                    $('#rsult_post').text(data.Mens);
                }
            },
            error: function () {
                alert('Disculpe, se presento un error al enviar los datos al servidor, por favor intente más tarde');
            }
        });
    });
    $('.with-caption').magnificPopup({
        type: 'image',
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',

        image: {
            verticalFit: true,
            titleSrc: function (item) {

                var caption = 'Viejo Portal';

                return caption + ' <img src="https://viejoportal.com/directorio/h1.png" width="50" height="28" />';
            }
        },


        gallery: {
            enabled: true
        },



        callbacks: {
            open: function () {
                this.wrap.on('click.pinhandler', '.pin-it', function (e) {

                    // This part of code doesn't work on CodePen, as it blocks window.open
                    // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
                    /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);

                    
                    return false;*/
                });
            },
            beforeClose: function () {
                //this.wrap.off('click.pinhandler');
            }
        }

    });

});