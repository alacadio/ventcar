    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listado de vehículos en venta</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="card-title m-0">Filtrar</h5>
                </div>
            <form action="<?php echo base_url() ?>resultado" method="get">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                          <label for="inputAño">Año</label>
                          <select class="form-control select2bs4" name="inputAño" id="inputAño" required>
                          </select>
                      </div>
                    </div>
                  </div>  
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="inputMarca">Marca</label>
                        <select class="form-control select2bs4" style="width: 100%;" id="inputMarca" name="inputMarca" required>
                          
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="inputModelo">Modelo</label>
                        <select class="form-control select2bs4" style="width: 100%;" id="inputModelo" name="inputModelo" required>
                          
                        </select>                         
                      </div>                        
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="inputVersion">Version</label>
                        <select class="form-control select2bs4" style="width: 100%;" id="inputVersion" name="inputVersion" required>
                          
                        </select>                         
                      </div>                        
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="">Placa:</label>
                        <div class="form-check">
                          <input type="radio" name="radio5" value="1">
                          <label class="form-check-label">Par</label>
                          <input type="radio" name="radio5" value="0">
                          <label class="form-check-label">Impar</label>
                        </div>
                      </div>                        
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="">Rango de precios</label>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="check_val_10">
                          <label class="form-check-label">Hasta $10,000,000</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="check_val_mx_4">
                          <label class="form-check-label">$11,000,000 A $40,000,000</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="check_val_m_4">
                          <label class="form-check-label">Más de $41,000,000</label>
                        </div>
                      </div>                        
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="inputCombustible">Combustible</label>
                        <select class="form-control select2bs4" style="width: 100%;" id="inputCombustible" name="inputCombustible" required>
                          <option value="-1">- Seleccione una opción -</option>
                          <option value="0">Diessel</option>
                          <option value="1">Gasolina</option>
                        </select>                         
                      </div>                        
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="inputKilometraje">Kilometraje</label>
                        <select class="form-control select2bs4" style="width: 100%;" id="inputKilometraje" name="inputKilometraje" required>
                          <option value="0">- Seleccione una opción -</option>
                          <option value="1">Hasta 10Mil Km</option>
                          <option value="2">Entre 10Mil Km a 20Mil Km</option>
                          <option value="3">Entre 21Mil Km a 30Mil Km</option>
                          <option value="4">Entre 31Mil Km a 40Mil Km</option>
                          <option value="5">Entre 41Mil Km a 50Mil Km</option>
                          <option value="6">Entre 51Mil Km a 60Mil Km</option>
                          <option value="7">Mas de 60Mil Km</option>
                        </select>                         
                      </div>                        
                    </div>
                  </div>
                  
                </div>
                <div class="card-footer">
                  <button type="submit" value="Buscar" class="btn btn-primary">Buscar</button>
                </div>
            </form>
            </div>
          </div>
          <div class="col-sm-9">
            <div class="row">          
              <input type="hidden" name="est" id="est" value="<?php echo $est; ?>">
                <!-- /.col-md-6 -->
              <?php if($count > 0): ?>
                <?php 
                  foreach ($lt as $v): ?>
                    <?php if($v->EST == 1): ?>
                      <div class="col-sm-4">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h5 class="card-title m-0"> <?php echo $v->MAR." ".$v->MO; ?></h5>
                            </div>
                            <div class="card-body">
                                <center>
                                  <img src="<?php echo base_url().$v->IMG;?>" alt="" width="200px" height="120px">                                  
                                  <p></p>
                                  <p><span><?php echo $v->A; ?> | <?php echo $v->K; ?></span> Km</p>
                                  <h3>$<?php echo number_format($v->VAL); ?></h3>
                                </center>
                            </div>
                            <div class="card-footer">
                              <center>
                                <a href="<?php echo base_url();?>vehiculo/detalles?id=<?php echo $v->ID; ?>" data-toggle="tooltip" data-placement="top" title="Detalles" class="btn btn-primary">Ver detalles vehículos</a>
                              </center>
                            </div>
                        </div>
                      </div>
                    <?php endif; ?>
                <?php endforeach; ?>
              <?php endif; ?>
                
            </div>
              <?php echo $this->pagination->create_links(); ?>
          </div>
          
        </div>        
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->