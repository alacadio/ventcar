    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Configuración del sistema</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <form action="<?php echo base_url();?>admin/admin/act" method="post" id="act_log_amd">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Credenciales del administrador</h3>
                        <div class="card-tools">
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="inputUsuario">Usuario</label>
                                    <input type="text" class="form-control" name="inputUser" id="inputUsuario" placeholder="Usuario" required value="<?php echo $my_datos->US; ?>">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="inputPass">Contraseña</label>
                                    <input type="password" class="form-control" name="inputPass" id="inputPass" placeholder="Contraseña">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="inputContra" style="color: #fff;"> .</label>
                                    <a href="#" class="btn btn-primary form-control" onclick="fun_clave()" id="inputContra">Generar nueva contraseña</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Datos empresariales</h3>
                        <div class="card-tools">
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputNit">Nit</label>
                                    <input type="number" class="form-control" name="inputNit" id="inputNit" placeholder="Nit" required value="<?php echo $my_datos->DOC; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputEmpresa">Empresa</label>
                                    <input type="text" class="form-control" name="inputEmpresa" id="inputEmpresa" placeholder="Empresa" required value="<?php echo $my_datos->NOM; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required value="<?php echo $my_datos->EMA; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputCelular">Celular</label>
                                    <input type="number" class="form-control" name="inputCelular" id="inputCelular" placeholder="Celular" required value="<?php echo $my_datos->TEL; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputResponsable">Responsable</label>
                                    <input type="text" class="form-control" name="inputResponsable" id="inputResponsable" placeholder="Responsable" required value="<?php echo $my_datos->RES; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Razon">Razón Social</label>
                                    <input type="text" class="form-control" name="Razon" id="Razon" placeholder="Razón Social" required value="<?php echo $my_datos->RAZON; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputCiudad">Ciudad</label>
                                    <input type="text" class="form-control" name="inputCiudad" id="inputCiudad" placeholder="Ciudad" required value="<?php echo $my_datos->CIUD; ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="inputDireccion">Dirección</label>
                                    <input type="text" class="form-control" name="inputDireccion" id="inputDireccion" placeholder="Dirección" required value="<?php echo $my_datos->DIR; ?>">
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Actualizar todos los datos</button>
                    </div>                
                    <!-- /.card-body -->
                </div>
            </form>
        
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Carga base datos vehículos</h3>
                    <div class="card-tools">
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <form action="<?php echo base_url(); ?>admin/import" enctype="multipart/form-data" id="cgr_xlxs" method="post">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="">Cargar listado de base de datos:</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new"><i class="glyphicon glyphicon-paperclip"></i> Select file</span><span class="fileinput-exists"><i class="glyphicon glyphicon-repeat"></i> Change</span><input required accept=".xls, .xlsx" type="file" name="file" id="file"></span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i> Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputAñoI">Año modelo mas antiguo del vehículo </label>
                                    <input type="number" class="form-control" name="inputAñoI" id="inputAñoI" placeholder="Año inicial" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputAñoF">Año modelo mas reciente del vehículo</label>
                                    <input type="number" class="form-control" name="inputAñoF" id="inputAñoF" placeholder="Año final" required value="<?php echo date("Y");?>">
                                </div>
                            </div>
                        </div>
                    </div>                
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Cargar Archivo</button>
                    </div>
                </form>
            </div>

            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Total de vehículos cargados en la base de datos</h3>
                    <div class="card-tools">
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputTotal">Total de vehículos registrados</label>
                                <input type="number" class="form-control"id="inputTotal" placeholder="Total" value="<?php echo $vh; ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputMarca">Total de marcas registradas</label>
                                <input type="number" class="form-control"id="inputMarca" placeholder="Marca" value="<?php echo $ma; ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputVersiones">Total de versiones de vehículos registrados</label>
                                <input type="number" class="form-control"id="inputVersiones" placeholder="Versiones" value="<?php echo $ve; ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputModelo">Total de modelos registrados</label>
                                <input type="number" class="form-control"id="inputModelo" placeholder="Modelo" value="<?php echo $mo; ?>">
                            </div>
                        </div>
                    </div>
                </div>                
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
            </div>


            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Purgar la base de datos</h3>
                    <div class="card-tools">
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                
                <div class="card-body">
                    <form action="<?php echo base_url();?>admin/rgpur/marca" method="post" id="form_reg_pur">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtMarca">Marca a purgar</label>
                                    <input type="text" class="form-control" name="txtMarca" id="txtMarca" placeholder="Marca" value="">
                                    <p></p>
                                    <button class="btn btn-primary">Registrar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <h3>Listado de marcas purgadas</h3>
                        <div class="col-sm-12">
                            <table id="example2" class="table table-striped">
                                <thead>
                                <tr>
                                <th>Marca</th>
                                <th>Estado</th>
                                <th>Datos registrados</th>
                                <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($list_pur as $v): ?>
                                        <tr>
                                            <td>
                                                <?php echo $v->M; ?>
                                            </td>
                                            <td>
                                                <?php if($v->CONT > 0){ echo '<a href="#" class="badge bg-danger"> Con registros </a>';}else{echo '<a href="#" class="badge bg-success"> Sin registros</a>';} ?>
                                            </td>
                                            <td>
                                                <?php if($v->CONT > 0): ?>
                                                    Total registros encontrados: <?php echo '<a href="#" class="badge bg-danger"> '.$v->CONT.' </a>'; ?>
                                                <?php else: ?>
                                                    Total registros encontrados: <?php echo '<a href="#" class="badge bg-success"> 0 </a>'; ?>
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($v->CONT > 0): ?>
                                                    <a href="#" class="btn btn-warning" onclick="modal_del_mar_db('<?php echo $v->M; ?>')" data-toggle="tooltip" data-placement="top" title="Limpiar datos" ><i class="fas fa-broom"></i></a>
                                                    -
                                                <?php endif;?>                                                
                                                <a href="#" onclick="modal_del_mar_pug('<?php echo $v->ID; ?>')" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fas fa-window-close"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
                <!-- /.card-body -->
                <div class="card-footer">
                </div>
            </div>
           
        </div><!-- /.container-fluid -->        
    </div>
    <!-- /.content -->  

    <div class="modal fade" id="modal-danger-marca">
        <div class="modal-dialog modal-lg">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar marca de la purga</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <input type="hidden" name="id_con" id="id_con">
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_m" id="id_m">
                    <h4>Se eliminará esta marca como referencia para limpiar la base de datos.</h4>
                    <h5>¿Desea continuar? </h5>
                    <a href="#" onclick="fun_del_mar_pur()" class="btn btn-danger">Eliminar</a>
                </div>
                <div class="modal-footer justify-content-between">            
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->   

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar marca</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <center>
                    <img src="<?php echo base_url();?>assets/public/logos/error.svg" height="150px" alt="" srcset="">
                  </center>
                              
              <center>
                <h3>Esta apunto de eliminar esta marca con todos sus registros, esta operación no podrá ser restaurada.</h3>
                <h3>¿está seguro de continuar?</h3>
              </center>
              <input type="hidden" name="id_M" id="id_M">
            </div>
            <div class="modal-footer justify-content-between">
                <a href="#" onclick="fun_del_mar_db()" class="btn btn-info">Purgar base de datos</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 