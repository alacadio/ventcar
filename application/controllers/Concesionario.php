<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Concesionario extends CI_Controller
{

    /**
     * Usuario concesionario
     * 
     * @var object
     */
    protected $user;
    /**
     * Cantidad notificaciones
     * 
     * @var Int
     */
    protected $count_not = 0;
    /**
     * 
     */
    
    // --------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ConModel');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));

        if ($this->session->userdata('login')) {

            if($this->session->userdata('car') != 2){
                redirect(base_url().'ingreso');
            }
        } else {
            redirect(base_url().'ingreso');
        }

        $this->user = $this->UserModel->get_user_by_token($this->session->userdata('token'));

        $vh = $this->ConModel->get_list_car_us($this->user->DOC);

        foreach ($vh as $v) {
            $aux = $this->ConModel->get_notificacion_by_id_vh($v->ID);
            if(count($aux) != 0){
                $this->count_not += 1;
                $data = array('notificacion_vista' => 1 );
                $result = $this->ConModel->set_update_notificacion($v->ID,$data);
            }else{
                $this->count_not = 0;
            }
        }
    }


    public function index()
    {
        $ventas = $this->ConModel->get_list_car_com($this->user->DOC);

        $interes = $this->ConModel->get_list_car_int($this->user->DOC);

        $list_car = $this->ConModel->get_list_car_us($this->user->DOC);

        $count_ventas = 0;

        $count_ventas_neg = 0;

        foreach ($ventas as $v) {
            if($v->AP == 1){
                $count_ventas = $count_ventas + 1;
            }else if($v->AP == 2){
                $count_ventas_neg = $count_ventas_neg + 1;
            }
        }

        $data = array(
            'user' => $this->user,
            'ventas' => $ventas,
            'interes' => $interes,
            'neg_venta' => $count_ventas_neg,
            'venta_pos' => $count_ventas,
            'list_car' => $list_car,
            'notificacion' => $this->count_not
        );
        $this->load->view("layout/conc/header",$data);
        $this->load->view("layout/conc/sidebar");
        $this->load->view("user/index_con");
        $this->load->view("layout/conc/footer");
    }

    public function perfil()
    {
        $data = array(
            'user' => $this->user,
            'notificacion' => $this->count_not
        );
        $this->load->view("layout/conc/header",$data);
        $this->load->view("layout/conc/sidebar");
        $this->load->view("user/index_con");
        $this->load->view("layout/conc/footer");
    }

    public function perfil_carro()
    {

        $id = $this->input->get("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        $interes = $this->ConModel->get_list_con_inst($id);

        $data = array(
            'user' => $this->user,
            'car' => $car,
            'img' => $img,
            'inte' => $interes,
            'num' => $this->UserModel->get_num_adm()->TEL,
            'notificacion' => $this->count_not
        );

        $this->load->view("layout/conc/header", $data);
        $this->load->view("layout/conc/sidebar");
        $this->load->view("user/perfil_car_cons");
        $this->load->view("layout/conc/footer");
    }

    //Funciones
    public function update_cons()
    {      
        
        $inputUser = $this->input->post('inputUser');
        $inputPass = $this->input->post('inputPass');

        $inputNIT2 = $this->input->post('inputNIT2');
        $inputNombreConsecionaria2 = $this->input->post('inputNombreConsecionaria2');
        $inputRazon2 = $this->input->post('inputRazon2');
        $inputAsesor2 = $this->input->post('inputAsesor2');
        $inputDireccion2 = $this->input->post('inputDireccion2');
        $inputCiudad2 = $this->input->post('inputCiudad2');
        $inputTelefono2 = $this->input->post('inputTelefono2');
        $inputEmail2 = $this->input->post('inputEmail2');

        $data = array(
            'razon'         => $inputRazon2,
            'nombre'        => $inputNombreConsecionaria2,
            'responsable'   => $inputAsesor2,
            'telefono'      => $inputTelefono2,  
            'email'         => $inputEmail2,
            'direccion'     => $inputDireccion2,
            'ciudad'        => $inputCiudad2
        );

        $login = array();

        $user = $this->UserModel->get_user_by_document($inputNIT2);

        $result = $this->UserModel->set_update_user($inputNIT2, $data);

        if($result){
            if(!empty($_POST['inputPass'])){
                $inputPass = md5($inputPass);
                $login = array(
                    'usuario' => $inputUser,
                    'pass' => $inputPass
                );
            }else{
                $login = array(
                    'usuario' => $inputUser
                );
            }
            $result_l = $this->UserModel->set_update_login($inputNIT2, $login);
            if($result_l){
                $data = array('error' => false, 'Mens' => "¡¡Datos del usuario actualizados de forma exitosa!!");
                echo json_encode($data);
            }else{
                $data = array('error' => true, 'Mens' => "Se presente un error al actualizar los datos de ingreso al sistema");
                echo json_encode($data);
            }
        }else{
            $data = array('error' => true, 'Mens' => "Disculpe, se presento un error al tratar de actualizar los datos, por favor intente más tarde");
            echo json_encode($data);
        }
    }

    public function perfil_car()
    {
        $id = $this->input->get("id");

        $car = $this->ConModel->get_info_car_by_car($id);

        $img = $this->ConModel->get_list_img_by_car($id);

        $interes = $this->ConModel->get_list_con_inst($id);

        $data = array(
            'user' => $this->user,
            'car' => $car,
            'img' => $img            
        );

        $this->load->view("layout/conc/header",$data);
        $this->load->view("layout/conc/sidebar");
        $this->load->view("user/perfil_car_con");
        $this->load->view("layout/conc/footer");
    }

    public function delete_oferta()
    {
        $id = $this->input->post('id');

        $doc = $this->user->DOC;

        $result = $this->ConModel->set_delete_oferta($id,$doc);

        if($result){
            $data = array('error' => false, 'Mens' => 'La oferta ha sido retirada de forma satisfactoria');
            echo json_encode($data);
        }else{
            $data = array('error' => ture, 'Mens' => 'Disculpe, pero la oferta no pudo ser retirada, por favor intente más tarde');
            echo json_encode($data);
        }
    }

     public function registro()
    {
        $id = $this->session->userdata('doc');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta = $this->input->post('inputValorVenta');
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');
        $inputFile1 = $this->input->post('inputFile1');

        $op_valor = 0;

        if($inputValorVenta != 0 || $inputValorVenta != null){
            $op_valor = 1;
        }else{
            $inputValorVenta = $inputValorpromediodeventa;
        }

        $vh = array(
            'marca' => $inputMarca,
            'modelo' => $inputModelo,
            'version' => $inputVersion,
            'year' => $inputAño,
            'kilometraje' => $inputKilometraje,
            'placa' => $inputPlaca,
            'valor_venta' => $inputValorVenta,
            'op_valor' => $op_valor,
            'documento' => $id,
            'activo' => 0,
            'pintura' => $radio1,
            'interior' => $radio2,
            'mecanico' => $radio3,
            'accidente' => $radio6,
            'choque' => $radio4,
            'combustible' => $radio5,
            'descripcion' => $inputDesc,
            'user' => 1
        );
        
        $carro = $this->ConModel->set_register_car($vh);

        $count_file = count($_FILES['inputFile1']['name']);

        if($carro){
            if($count_file > 1){
                $ruta = "./assets/public/img/";

                $this->load->library('upload');
                
                $uploadData = array();
                $config = array();
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']     = FALSE;

                $count_file = count($_FILES['inputFile1']['name']);

                for ($i=0; $i < $count_file; $i++) { 
                    
                    $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile'))
                    {

                    }else{
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['path'] = $fileData['file_path'];
                        $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                        $uploadData[$i]['id_vh'] = $carro;
                    }
                }        

                if(empty($uploadData)){
                    //Insert file information into the database
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                if($count_file != count($uploadData)){
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                $cont = 0;

                $img = array();

                for ($i=0; $i < count($uploadData); $i++) { 
                    $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                    $insert_img = $this->ConModel->set_register_img($img);
                    $cont = $cont + 1;
                }
                
                if($cont == count($uploadData)){
                    $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => false, 'Mens' => 'El vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                    echo json_encode($dato);
                }
            }else{
                $imagen = array(
                    'name' => 'default.png',
                    'img' => './assets/public/logos/icono.svg',
                    'id_vh' => $carro
                );

                $insert_img = $this->ConModel->set_register_img($imagen);

                if($insert_img){
                    $dato = array('error' => false, 'Mens' => '¡¡¡El registro ha sido completado de forma exitosa!!!');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => false, 'Mens' => 'El vehículo fueron registrado de forma exitosa, pero las imágenes no, por favor trate de ingresarlas al actualizar la información.');
                    echo json_encode($dato);
                }
                
            }
        }else{
            $dato = array('error' => true, 'Mens' => 'El vehículo fue registrado de forma exitosa, pero las imagenes NO, por favor proceda a eliminar el nuevo vehículo y trate de registrar nuevamente o trate completar el registro del vehículo en la ventana de actualización.');
            echo json_encode($dato);
        }
    }

    public function delete_post()
    {
        $id = $this->input->post('id');

        $list_img = $this->ConModel->get_list_img_by_car($id);

        $count_img = count($list_img);

        $cont = 0;

        if($count_img > 1){
            foreach ($list_img as $v) {
                if(!unlink($v->IMG)){                
                    $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                    echo json_encode($dato);
                    return;
                    break;
                }
                $cont = $cont + 1;
            }

            if($cont != $count_img){
                $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                echo json_encode($dato);
                return;
            }
        }

        $delete = $this->ConModel->set_delete_car($id);

        if($delete){
            $dato = array('error' => false, 'Mens' => 'Vehículo eliminado de forma exitosa!!!');
            echo json_encode($dato);
        }else{
            $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de eliminar la información del vehículo, pero las imágenes de este han sido eliminadas');
            echo json_encode($dato);
        }
    }

    public function act_post()
    {
        $id = $this->input->post('id_v');
        $inputMarca = $this->input->post('inputMarca');
        $inputModelo = $this->input->post('inputModelo');
        $inputVersion = $this->input->post('inputVersion');
        $inputAño = $this->input->post('inputAño');
        $inputKilometraje = $this->input->post('inputKilometraje');
        $inputPlaca = $this->input->post('inputPlaca');
        $inputMenorValorVenta = $this->input->post('inputMenorValorVenta');
        $inputValorpromediodeventa = $this->input->post('inputValorpromediodeventa');
        $inputMejorvalordeventa = $this->input->post('inputMejorvalordeventa');
        $inputValorVenta;
        $radio1 = $this->input->post('radio1');
        $radio2 = $this->input->post('radio2');
        $radio3 = $this->input->post('radio3');
        $radio6 = $this->input->post('radio6');
        $radio4 = $this->input->post('radio4');
        $radio5 = $this->input->post('radio5');
        $inputDesc = $this->input->post('inputDesc');

        if($inputValorpromediodeventa <= 0){
            $inputValorVenta = $this->input->post('inputValorVenta');
        }else{
            $inputValorVenta = $inputValorpromediodeventa;
        }

        $data = array(
            'marca' => $inputMarca,
            'modelo' => $inputModelo,
            'version' => $inputVersion,
            'year' => $inputAño,
            'kilometraje' => $inputKilometraje,
            'placa' => $inputPlaca,
            'valor_venta' => $inputValorVenta,
            'pintura' => $radio1,
            'interior' => $radio2,
            'mecanico' => $radio3,
            'accidente' => $radio6,
            'choque' => $radio4,
            'combustible' => $radio5,
            'descripcion' => $inputDesc
        );

        $img = $this->ConModel->get_list_img_by_car($id);

        $ext = $_FILES['inputFile1']['name'][0];

        if($ext != null){
            $count_img = count($img);

            $cont = 0;

            foreach ($img as $v) {
                if(!unlink($v->IMG)){                
                    $dato = array('error' => true, 'Mens' => 'Disculpe, se presento un error al tratar de eliminar las imágenes de la publicación, por los tanto esta operación no podrá ser completada con éxito, por favor vuelva a intentar más tarde');
                    echo json_encode($dato);
                    return;
                    break;
                }
                $cont = $cont + 1;
            }

            $delete_img = $this->ConModel->set_detele_img($id);

            if($delete_img){
                $ruta = "./assets/public/img/";

                $this->load->library('upload');
                
                $uploadData = array();
                $config = array();
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']     = FALSE;

                $count_file = count($_FILES['inputFile1']['name']);

                for ($i=0; $i < $count_file; $i++) { 
                    
                    $_FILES['userFile']['name'] = $_FILES['inputFile1']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['inputFile1']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['inputFile1']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['inputFile1']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['inputFile1']['size'][$i];


                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userFile'))
                    {

                    }else{
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['path'] = $fileData['file_path'];
                        $uploadData[$i]['img'] = $ruta.$fileData['file_name'];
                        $uploadData[$i]['id_vh'] = $id;
                    }
                }        

                if(empty($uploadData)){
                    //Insert file information into the database
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                if($count_file != count($uploadData)){
                    $dato = array('error' => false, 'Mens' => $uploadData);
                    echo json_encode($dato);
                    return;
                }

                $cont = 0;

                $img = array();

                for ($i=0; $i < count($uploadData); $i++) { 
                    $img = array('id_vh' => $uploadData[$i]['id_vh'], 'img' => $uploadData[$i]['img'], 'name' => $uploadData[$i]['name']);
                    $insert_img = $this->ConModel->set_register_img($img);
                    $cont = $cont + 1;
                }

                $result = $this->ConModel->set_update_car($id,$data);

                if($result){
                    $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                    echo json_encode($dato);
                }else{
                    $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                    echo json_encode($dato);
                }
            }
        }else{
            $result = $this->ConModel->set_update_car($id,$data);

            if($result){
                $dato = array('error' => false, 'Mens' => 'La información del vehículo ha sido actualizada de forma exitosa');
                echo json_encode($dato);
            }else{
                $dato = array('error' => true, 'Mens' => 'Disculpe, pero se presento un error al tratar de actualizar los datos');
                echo json_encode($dato);
            }
        }
    }

}