##########
Change Log
##########

=================
Version 1.1Alpha
=================

El sistema se encuentra en una versión Alpha la cual puede recurrir en muchos grandes cambios a futuro,
esta versión aun no esta lista para producción

### General Changes
- Agregado carga de los archivos excel al sistema

### Libraries
- Agregado librería Excel

### Helpers
- Utiliza las mismas de la version anterior

### Views
- Vista admin 
- Agregado configuracion.php y su funcionalidad

- Vista auth
- Utiliza las mismas de la version anteriord

- Vista email 
- No agregado

- Vista errors 
- Utiliza las mismas de la version anterior

- Layout
- Utiliza las mismas de la version anterior

- Vista user
- Utiliza las mismas de la version anterior

- Vista venta
- Utiliza las mismas de la version anterior
