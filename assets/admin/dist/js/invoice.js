var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val(); 
  
  $("#frm_send_email").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Enviando la factura",
      text: "Se esta enviando la factura al correo del cliente, por favor espere un momento",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Factura enviada",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  
});

function modal_email(cod) {
    $.ajax({
    url: base_url + "admin/invoice/info",
    type: "post",
    dataType: "json",
    data: { cod: cod },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        $('#cod').val(cod);
        $('#inputCod').val(data.Mens.COD_V);
        $('#inputCliente').val(data.Mens.COMP_NOM+' '+data.Mens.COMP_APE);
        $('#inputEmial').val(data.Mens.COMP_EMA);
        $("#send_email").modal();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
    
}