
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ventcar| Invoice Print</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <link rel="icon" href="<?php echo base_url();?>assets/public/logos/iconoblanco.svg" sizes="32x32" />

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
     <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <img src="<?php echo base_url();?>assets/public/logos/icono.svg" width="50px" height="50px" alt=""> <?php echo $date_invo->NOM; ?>
                    <small class="float-right">Date: <?php echo date("Y-m-d"); ?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong><?php echo $date_invo->NOM; ?></strong><br>
                    Nit: <?php echo $date_invo->NIT; ?><br>
                    <?php echo $date_invo->DIR; ?><br>
                    <?php echo $date_invo->CID; ?><br>
                    Phone: <?php echo $date_invo->TEL; ?><br>
                    Email: <?php echo $date_invo->EMA; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <?php if($invoice->COMP_APE): ?>
                        <strong><?php echo $invoice->COMP_NOM.' - '.$invoice->COMP_APE; ?></strong><br>
                    <?php else: ?>
                        <strong><?php echo $invoice->COMP_NOM; ?></strong><br>
                    <?php endif; ?>
                    <?php echo $invoice->DIR; ?><br>
                    <?php echo $invoice->CID; ?><br>
                    Phone: <?php echo $invoice->COMP_TEL; ?><br>
                    Email: <?php echo $invoice->COMP_EMA; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice #<?php echo $invoice->ID; ?></b><br>
                  <br>
                  <b>Order ID:</b> <?php echo $invoice->COD_V; ?><br>
                  <b>Fecha facturación:</b> <?php 
                        $newDate = date("d/m/Y", strtotime($invoice->FECH));
                        echo $newDate; 
                  ?><br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>                      
                      <th>Marca</th>
                      <th>Modelo</th>
                      <th>Version</th>
                      <th>Placa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td><?php echo $invoice->MA; ?></td>
                      <td><?php echo $invoice->MO; ?></td>
                      <td><?php echo $invoice->V; ?></td>
                      <td><?php echo $invoice->PA; ?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead">Payment Methods:</p>
                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Monto a pagar <?php echo $newDate; ?></p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>$<?php echo $invoice->VAL; ?></td>
                      </tr>
                      <tr>
                        <th>IVA (19%)</th>
                        <td>$<?php echo $iva/100; ?></td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>$<?php
                            echo $iva;
                         ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>
