$(document).ready(function () {
  $("#reg_cons_us").submit(function (ev) {
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
            confirmButtonText: "Aceptar",
            showConfirmButton: false,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando el usuario, por favor espere...",
      showConfirmButton: false
    });
    ev.preventDefault();
  });
  $("#login").submit(function (ev) {
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Bienvenido a ventcar....",
            text: data.Mens,
            confirmButtonText: "Aceptar",
            showConfirmButton: false,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    Swal.fire({
      icon: "info",
      title: "Ingresando al sistema",
      text: "Espere mientras validamos las credenciales",
      showConfirmButton: false,
    });
    ev.preventDefault();
  });
  $("#res_pass").submit(function (ev) {
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Bienvenido a ventcar....",
            text: data.Mens,
            confirmButtonText: "Aceptar",
            showConfirmButton: false,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    Swal.fire({
      icon: "info",
      title: "Reestableciendo contraseña",
      text: "Espere mientras reestablecemos su contraseña",
      showConfirmButton: false,
    });
    ev.preventDefault();
  });
});
