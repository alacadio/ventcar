
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Main Footer -->
  <footer class="main-footer">
      <!-- To the right -->
        <div class="float-right d-none d-sm-inline" style="color: black !important;">
          <a style="color: black !important;" href="https://ventcar.com/terminos-y-condiciones/">Términos y condiciones</a> | <a style="color: black !important;" href="https://ventcar.com/politicas-de-privacidad/">Politicas de privacidad</a> | <a style="color: black !important;" href="https://ventcar.com/politica-de-cookies/">Politicas de cookies</a>        
      </div>
      <!-- Default to the left -->
      <strong>&copy; Todos los derechos reservados por Ventcar <?php echo date("Y"); ?> | <a style="color: black !important;" href="https://masifica.co/">Masifica.co</a></strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
  <script src="<?php echo base_url();?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url();?>assets/admin/dist/js/adminlte.min.js"></script>
  <script src="<?php echo base_url();?>assets/public/js/galery.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

  <script src="<?php echo base_url();?>assets/admin/plugins/select2/js/select2.full.min.js"></script>

  <script src="<?php echo base_url();?>assets/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

  <?php if($js == 1): ?>

    <script src="<?php echo base_url();?>assets/public/js/<?php echo $script; ?>"></script>
  <?php elseif ($js == 2): ?>

    <script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>assets/admin/dist/js/app.js"></script>
  
  <?php endif; ?>  
</body>
</html>
