<!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Perfil <?php echo $nombre. ' - '.$apellido; ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>usuario">Home</a></li>
                <li class="breadcrumb-item active">Perfil</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
             <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="<?php echo base_url();?>assets/admin/img/avatar04.png" alt="User Avatar">
              </div>
              <div class="card-footer">
                <center>
                  <h4>Información de contacto</h4>
                  <h5><?php echo $nombre. ' - '.$apellido; ?></h5>
                </center>
                <div class="row">
                  <div class="col-sm-3 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $publicado; ?></h5>
                      <span class="description-text">Vehículos publicados</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  
                  <div class="col-sm-3 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $vendido; ?></h5>
                      <span class="description-text">Vehículos vendido</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <div class="col-sm-3 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $ema; ?></h5>
                      <span class="description-text">Email</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <div class="col-sm-3">
                    <div class="description-block">
                      <h5 class="description-header"><?php echo $cell; ?></h5>
                      <span class="description-text">Teléfono</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->

            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Listado de vehículos</h3>
                            <div class="card-tools">
                                <a href="#" onclick="modal_new_car()" class="btn btn-success">Publicar vehículo</a>
                                -
                                <a href="#" onclick="modal_act_us(<?php echo $id;?>)" class="btn btn-primary">Editar Perfil</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Año</th>
                                        <th>Estado</th>
                                        <th>Ver</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($car as $v): ?>
                                      <tr>
                                        <td>
                                          <div class="post-thumbnail">
                                              <a href="<?php echo base_url().$v->IMG;?>" class="with-caption image-link">
                                                  <img src="<?php echo base_url().$v->IMG;?>" alt="" width="100px">
                                              </a>
                                          </div>
                                        </td>
                                        <td><?php echo $v->M; ?></td>
                                        <td><?php echo $v->MO; ?></td>
                                        <td><?php echo $v->A; ?></td>
                                        <td>
                                            <?php if($v->EST == 1 ): ?>
                                              <a href="#" class="badge bg-info">En venta</a>
                                            <?php elseif($v->EST == 0 ): ?>
                                              <a href="#" class="badge bg-danger">Pausado</a>
                                            <?php else: ?>
                                              <a href="#" class="badge bg-success">Vendido</a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                          <a target="framename" href="<?php echo base_url();?>usuario/car/perfil?id=<?php echo $v->ID; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                                          <?php if($v->EST != 2 ): ?>
                                            -
                                            <a href="#" class="btn btn-warning" onclick="modal_act_car_us('<?php echo $v->ID; ?>')"><i class="fas fa-edit"></i></a>
                                          <?php endif; ?>
                                        </td>
                                      </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
        
      </div>
      
      <!-- /.content --> 


      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>usuario/user/act" method="post" id="act_us_amd">

                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Datos de contacto</h3>
                    <div class="card-tools">
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputDocumento">Documento*</label>
                          <input type="number" class="form-control" name="inputDocumento" id="inputDocumento" placeholder="Documento" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputNombre">Nombre*</label>
                          <input type="text" class="form-control" name="inputNombre" id="inputNombre" placeholder="Nombre" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputApellido">Apellido</label>
                          <input type="text" class="form-control" name="inputApellido" id="inputApellido" placeholder="Apellido">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputCelular">Celular*</label>
                          <input type="number" class="form-control" name="inputCelular" id="inputCelular" placeholder="Celular" required>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputCiudad">Ciudad*</label>
                          <select id="inputCiudad" name="inputCiudad" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputEmail">Email*</label>
                          <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Datos de ingreso</h3>
                    <div class="card-tools">
                        <a href="#" class="btn btn-primary" onclick="fun_clave()" id="inputbtn">Restablecer contraseña</a>
                    </div>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="inputUser">Nombre de usuario</label>
                          <input type="text" class="form-control" name="inputUser" id="inputUser" placeholder="User" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="inputPass">Contraseña</label>
                          <input type="password" class="form-control" name="inputPass" id="inputPass" placeholder="Nueva contraseña">
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal_act_car">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información del vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>usuario/car/act" method="post" id="act_pos_cli" enctype="multipart/form-data">
                <input type="hidden" name="id_v" id="id_v">
                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Descripción del vehículo</h3>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAñoU" id="Año" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="Marca" name="inputMarcaU" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="Modelo" name="inputModeloU" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="Version" name="inputVersionU" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="number" class="form-control" name="inputKilometrajeU" id="inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlacaU" id="inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5U" value="1" checked id="radio_5_1">
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5U" value="0" id="radio_5_0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMenorValorVenta">Menor valor venta</label>
                          <input type="text" class="form-control" name="inputMenorValorVentaU" id="inputMenorValorVentaU" placeholder="Menor valor venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="hidden" name="inputValorpromediodeventaU" id="pro">
                          <label for="inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="text" class="form-control" name="inputValorpromediodeventaU" id="inputValorpromediodeventaU" placeholder="Valor promedio de venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="text" class="form-control" name="inputMejorvalordeventaU" id="inputMejorvalordeventaU" placeholder="Mejor valor de venta" disabled>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVentaU" id="inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">                      
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" id="radio_2_1" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" id="radio_2_2" value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" id="radio_2_3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2" id="radio_2_4"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" id="radio_2_5" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2" id="radio_2_6"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" id="radio_2_7" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2" id="radio_2_8"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" id="radio_2_9" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2" id="radio_2_10"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1" id="radio_3_1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" id="radio_3_2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3" id="radio_3_3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4" id="radio_3_4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5" id="radio_3_5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6" id="radio_3_6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7" id="radio_3_7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8" id="radio_3_8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9" id="radio_3_9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10" id="radio_3_10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>                    
                    <hr>
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                      <p>Nota: la primera imagen en seleccionar será la primera en mostrar</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Seleccione el listado de imágenes</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1U[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <center>
                      <h3>Listado de imágenes actuales</h3>
                    </center>
                    <hr>
                    <div class="row" id="id_img">
                      
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Actualizar datos</button>
                  </div>
                </div>
                          
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal_pos_car">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Publicar nuevo vehículo</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url();?>usuario/registro" method="post" id="reg_us_car" enctype="multipart/form-data">
                <input type="hidden" name="id_v" id="id_v">
                <div class="card card-outline card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Descripción del vehículo</h3>
                    <!-- /.card-tools -->
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputAño">Año*</label>
                          <select class="form-control select2bs4" name="inputAño" id="inputAño" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMarca">Marca*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputMarca" name="inputMarca" required>
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputModelo">Modelo*</label>
                          <select class="form-control select2bs4" style="width: 100%;" id="inputModelo" name="inputModelo" required>
                            
                          </select>                         
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputVersion">Versión*</label>
                          <select id="inputVersion" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputKilometraje">Kilometraje*</label>
                          <input type="number" class="form-control" name="inputKilometraje" id="inputKilometraje" placeholder="Kilometraje" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label for="inputPlaca">Placa*</label>
                          <input type="text" class="form-control" name="inputPlaca" id="inputPlaca" placeholder="Placa" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                          <div class="form-group">
                              <label for="">Tipo combustible </label>
                              <div class="form-check">
                                <input type="radio" name="radio5" value="1" id="radio_5_1">
                                <label class="form-check-label">Gasolina</label>
                                <input type="radio" name="radio5" value="0" id="radio_5_0">
                                <label class="form-check-label">Diesel</label>
                                
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <h5>Cálculo del precio aproximado de venta del vehículo</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMenorValorVenta">Menor valor venta</label>
                          <input type="text" class="form-control" name="inputMenorValorVenta" id="inputMenorValorVenta" placeholder="Menor valor venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="hidden" name="inputValorpromediodeventa" value="" id="pr">
                          <label for="inputValorpromediodeventa">Valor promedio de venta</label>
                          <input type="text" class="form-control" id="inputValorpromediodeventa" placeholder="Valor promedio de venta" disabled>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="inputMejorvalordeventa">Mejor valor de venta</label>
                          <input type="text" class="form-control" name="inputMejorvalordeventa" id="inputMejorvalordeventa" placeholder="Mejor valor de venta" disabled>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5>¿No estas de acuerdo con el valor aproximado?</h5>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputValorVenta">Valor de venta</label>
                          <input type="number" class="form-control" name="inputValorVenta" id="inputValorVenta" placeholder="Valor de venta">
                        </div>
                      </div>
                    </div>                
                    <div class="row">                      
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado interior de su vehículo:</label>
                              <div class="form-check">
                                <input type="radio" name="radio2" id="radio_2_1" value="1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio2" id="radio_2_2" value="2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio2" id="radio_2_3" value="3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio2" id="radio_2_4"  value="4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio2" id="radio_2_5" value="5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio2" id="radio_2_6"  value="6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio2" id="radio_2_7" value="7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio2" id="radio_2_8"  value="8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio2" id="radio_2_9" value="9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio2" id="radio_2_10"  value="10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="">Califique el estado fisico general:</label>
                              <div class="form-check">
                                <input type="radio" name="radio3" value="1" id="radio_3_1">
                                <label class="form-check-label">1</label>
                                <input type="radio" name="radio3" value="2" id="radio_3_2">
                                <label class="form-check-label">2</label>
                                <input type="radio" name="radio3" value="3" id="radio_3_3">
                                <label class="form-check-label">3</label>
                                <input type="radio" name="radio3" value="4" id="radio_3_4">
                                <label class="form-check-label">4</label>
                                <input type="radio" name="radio3" value="5" id="radio_3_5">
                                <label class="form-check-label">5</label>
                                <input type="radio" name="radio3" value="6" id="radio_3_6">
                                <label class="form-check-label">6</label>
                                <input type="radio" name="radio3" value="7" id="radio_3_7">
                                <label class="form-check-label">7</label>
                                <input type="radio" name="radio3" value="8" id="radio_3_8">
                                <label class="form-check-label">8</label>
                                <input type="radio" name="radio3" value="9" id="radio_3_9">
                                <label class="form-check-label">9</label>
                                <input type="radio" name="radio3" value="10" id="radio_3_10">
                                <label class="form-check-label">10</label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <center>
                      <h3>Imagenes del vehiculo</h3>
                      <p>el grupo de imagenes no puede superar el peso maximo de 5mb</p>
                      <p>Nota: la primera imagen en seleccionar será la primera en mostrar</p>
                    </center>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputFile1">Seleccione el listado de imágenes</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="inputFile1[]" id="inputFile1" multiple>
                              <label class="custom-file-label" for="inputFile1">Seleccionar imagen</label>
                            </div>
                            <div class="input-group-append">                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="estado" id="estado" value="1">
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-block btn-primary">Registrar</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->