    
 <div class="content">
      <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputAño">Año*</label>
                    <select class="form-control select2bs4" name="inputAño" id="Año" required>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputMarca">Marca*</label>
                    <select class="form-control select2bs4" style="width: 100%;" id="Marca" name="inputMarca" required>                
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputModelo">Modelo*</label>
                    <select class="form-control select2bs4" style="width: 100%;" id="Modelo" name="inputModelo" required>                
                    </select>                         
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputVersion">Versión*</label>
                    <select id="Version" name="inputVersion" class="form-control select2bs4" style="width: 100%;" required>
                    </select>
                </div>
            </div>
        </div>
        <center>
            <div id="val">
                <div id="va"></div>
                <a target="framename" href="<?php echo base_url();?>usuario" class="btn btn-primary">Vender vehiculo</a>
            </div>
        </center>
    </div>
</div>