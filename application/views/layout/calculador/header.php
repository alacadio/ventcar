<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>

  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Ventcar | Listado de vehiculos</title>

  <link rel="icon" href=" https://ventcar.com/wp-content/uploads/2020/08/fav-300x267.png" sizes="32x32" />

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/dist/css/adminlte.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/dist/css/style.css">  

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/select2/css/select2.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <div class="content-wrapper">