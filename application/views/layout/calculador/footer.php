
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Main Footer -->
  <footer class="main-footer">
      <!-- To the right -->
        <div class="float-right d-none d-sm-inline" style="a color: black !important;">
          <a href="https://ventcar.com/terminos-y-condiciones/">Términos y condiciones</a> | <a href="https://ventcar.com/politicas-de-privacidad/">Politicas de privacidad</a> | <a href="https://ventcar.com/politica-de-cookies/">Politicas de cookies</a>        
      </div>
      <!-- Default to the left -->
      <strong>&copy; Todos los derechos reservados por Ventcar <?php echo date("Y"); ?> | <a href="https://masifica.co/">Masifica.co</a></strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/dist/js/adminlte.min.js"></script>

<script src="<?php echo base_url();?>assets/admin/plugins/select2/js/select2.full.min.js"></script>

<script src="<?php echo base_url();?>assets/admin/dist/js/calculador.js"></script>
</body>
</html>
