<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ventcar.com</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	
	<link rel="stylesheet" href="<?php echo base_url();?>assets/public/css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!-- Begin page content -->
    <div class="container bg">
      <div class="page-header">
        <h1>En construcción</h1>
	  </div>
	  <center>
		  <div>
				<svg width="250" height="250" viewBox="0 0 512 512" fill="none"
					xmlns="http://www.w3.org/2000/svg">
					<g id="funcion 1">
						<g id="Group">
							<g id="Group_2">
								<path id="Vector"
									d="M232 504C220.536 504 120 503.104 120 472C120 467.048 120 451.96 174.832 443.856L177.176 459.688C149.992 463.704 139.208 469.464 136.48 472.04C142.328 478.176 176.68 488 232 488C287.32 488 321.672 478.176 327.52 472.04C324.792 469.464 314.016 463.704 286.824 459.688L289.168 443.856C344 451.96 344 467.048 344 472C344 503.104 243.464 504 232 504V504Z"
									fill="black" />
							</g>
							<g id="Group_3">
								<path id="Vector_2"
									d="M272 472H192C178.768 472 168 461.232 168 448V416H184V448C184 452.416 187.584 456 192 456H272C276.416 456 280 452.416 280 448V416H296V448C296 461.232 285.232 472 272 472Z"
									fill="black" />
							</g>
							<g id="Group_4">
								<path id="Vector_3" d="M184 136H280V208H184V136Z" fill="#F0BC5E" />
							</g>
							<g id="Group_5">
								<path id="Vector_4" d="M72 240H280V312H72V240Z" fill="#F0BC5E" />
							</g>
							<path id="Vector_5"
								d="M452.568 206.504L440 200.504V384C440 397.232 429.232 408 416 408H48C34.768 408 24 397.232 24 384V112C24 98.768 34.768 88 48 88H296V82.336L310.608 77.184C311.104 75.928 311.624 74.672 312.176 73.416L311.504 72H48C25.944 72 8 89.944 8 112V384C8 406.056 25.944 424 48 424H416C438.056 424 456 406.056 456 384V203.08L452.568 206.504Z"
								fill="black" />
							<g id="Group_6">
								<path id="Vector_6"
									d="M232 400C218.768 400 208 389.232 208 376C208 362.768 218.768 352 232 352C245.232 352 256 362.768 256 376C256 389.232 245.232 400 232 400ZM232 368C227.584 368 224 371.584 224 376C224 380.416 227.584 384 232 384C236.416 384 240 380.416 240 376C240 371.584 236.416 368 232 368Z"
									fill="black" />
							</g>
							<g id="Group_7">
								<path id="Vector_7" d="M64 376H128V392H64V376Z" fill="#F0BC5E" />
							</g>
							<g id="Group_8">
								<path id="Vector_8"
									d="M136 392H120V376H136V392ZM104 392H88V376H104V392ZM72 392H56V376H72V392Z"
									fill="black" />
							</g>
							<path id="Vector_9"
								d="M408 216V328H56V120H296V104H56C47.176 104 40 111.176 40 120V328C40 336.824 47.176 344 56 344H408C416.824 344 424 336.824 424 328V216H408Z"
								fill="black" />
							<g id="Group_9">
								<path id="Vector_10" d="M352 360H416V392H352V360Z" fill="#F0BC5E" />
							</g>
							<g id="Group_10">
								<path id="Vector_11" d="M408 360H424V392H408V360Z" fill="black" />
							</g>
							<g id="Group_11">
								<path id="Vector_12" d="M376 360H392V392H376V360Z" fill="black" />
							</g>
							<g id="Group_12">
								<path id="Vector_13" d="M344 360H360V392H344V360Z" fill="black" />
							</g>
							<g id="rueda">
								<g id="Group_13">
									<path id="Vector_14"
										d="M400 144C382.352 144 368 129.648 368 112C368 94.352 382.352 80 400 80C417.648 80 432 94.352 432 112C432 129.648 417.648 144 400 144ZM400 96C391.176 96 384 103.176 384 112C384 120.824 391.176 128 400 128C408.824 128 416 120.824 416 112C416 103.176 408.824 96 400 96Z"
										fill="black" />
								</g>
								<g id="Group_14">
									<path id="Vector_15"
										d="M504 141.664V82.336L489.392 77.184C488.896 75.928 488.376 74.672 487.824 73.416L494.512 59.424L452.576 17.488L438.584 24.176C437.336 23.624 436.08 23.096 434.816 22.608L429.664 8H370.336L365.184 22.608C363.928 23.104 362.672 23.624 361.416 24.176L347.424 17.488L305.488 59.424L312.176 73.416C311.624 74.664 311.096 75.92 310.608 77.184L296 82.336V141.664L310.608 146.816C311.104 148.072 311.624 149.328 312.176 150.584L305.488 164.576L347.424 206.512L361.416 199.824C362.664 200.376 363.92 200.904 365.184 201.392L370.336 216H429.664L434.816 201.392C436.072 200.896 437.328 200.376 438.584 199.824L452.576 206.512L494.512 164.576L487.824 150.584C488.376 149.336 488.904 148.08 489.392 146.816L504 141.664ZM400 168C369.072 168 344 142.928 344 112C344 81.072 369.072 56 400 56C430.928 56 456 81.072 456 112C456 142.928 430.928 168 400 168Z"
										fill="#F0BC5E" />
								</g>
								<g id="Group_15">
									<path id="Vector_16"
										d="M400 176C364.712 176 336 147.288 336 112C336 76.712 364.712 48 400 48C435.288 48 464 76.712 464 112C464 147.288 435.288 176 400 176ZM400 64C373.528 64 352 85.528 352 112C352 138.472 373.528 160 400 160C426.472 160 448 138.472 448 112C448 85.528 426.472 64 400 64Z"
										fill="black" />
								</g>
							</g>
							<g id="corche_i">
								<g id="Group_16">
									<path id="Vector_17"
										d="M80 184H72V168H80C84.416 168 88 164.416 88 160C88 146.768 98.768 136 112 136H120V152H112C107.584 152 104 155.584 104 160C104 173.232 93.232 184 80 184Z"
										fill="black" />
								</g>
								<g id="Group_17">
									<path id="Vector_18"
										d="M120 216H112C98.768 216 88 205.232 88 192C88 187.584 84.416 184 80 184H72V168H80C93.232 168 104 178.768 104 192C104 196.416 107.584 200 112 200H120V216Z"
										fill="black" />
								</g>
								<g id="Group_18">
									<path id="Vector_19"
										d="M128 184H120V168H128C132.416 168 136 164.416 136 160C136 146.768 146.768 136 160 136H168V152H160C155.584 152 152 155.584 152 160C152 173.232 141.232 184 128 184Z"
										fill="black" />
								</g>
								<g id="Group_19">
									<path id="Vector_20"
										d="M168 216H160C146.768 216 136 205.232 136 192C136 187.584 132.416 184 128 184H120V168H128C141.232 168 152 178.768 152 192C152 196.416 155.584 200 160 200H168V216Z"
										fill="black" />
								</g>
							</g>
							<g id="corche_d">
								<g id="Group_20">
									<path id="Vector_21"
										d="M352 312H344V296H352C356.416 296 360 292.416 360 288C360 274.768 370.768 264 384 264H392V280H384C379.584 280 376 283.584 376 288C376 301.232 365.232 312 352 312Z"
										fill="black" />
								</g>
								<g id="Group_21">
									<path id="Vector_22"
										d="M392 280H384C370.768 280 360 269.232 360 256C360 251.584 356.416 248 352 248H344V232H352C365.232 232 376 242.768 376 256C376 260.416 379.584 264 384 264H392V280Z"
										fill="black" />
								</g>
								<g id="Group_22">
									<path id="Vector_23"
										d="M304 312H296V296H304C308.416 296 312 292.416 312 288C312 274.768 322.768 264 336 264H344V280H336C331.584 280 328 283.584 328 288C328 301.232 317.232 312 304 312Z"
										fill="black" />
								</g>
								<g id="Group_23">
									<path id="Vector_24"
										d="M344 280H336C322.768 280 312 269.232 312 256C312 251.584 308.416 248 304 248H296V232H304C317.232 232 328 242.768 328 256C328 260.416 331.584 264 336 264H344V280Z"
										fill="black" />
								</g>
							</g>
							<g id="Group_24">
								<path id="Vector_25" d="M184 136H200V152H184V136Z" fill="black" />
							</g>
							<g id="Group_25">
								<path id="Vector_26" d="M216 136H280V152H216V136Z" fill="black" />
							</g>
							<g id="Group_26">
								<path id="Vector_27" d="M184 168H280V184H184V168Z" fill="black" />
							</g>
							<g id="Group_27">
								<path id="Vector_28" d="M184 200H280V216H184V200Z" fill="black" />
							</g>
							<g id="Group_28">
								<path id="Vector_29" d="M72 232H280V248H72V232Z" fill="black" />
							</g>
							<g id="Group_29">
								<path id="Vector_30" d="M72 264H88V280H72V264Z" fill="black" />
							</g>
							<g id="Group_30">
								<path id="Vector_31" d="M104 264H280V280H104V264Z" fill="black" />
							</g>
							<g id="Group_31">
								<path id="Vector_32" d="M264 296H280V312H264V296Z" fill="black" />
							</g>
							<g id="Group_32">
								<path id="Vector_33" d="M72 296H248V312H72V296Z" fill="black" />
							</g>
						</g>
					</g>
				</svg>

			</div>
	  </center>
	  <h2>Estamos trabajando en algo maravilloso que pronto llegara...</h2>

	  <footer class="footer">
		<div class="container">
			<p>© Todos los derechos reservados por Ventcar <strong><?php echo date("Y"); ?></strong></p>
		</div>
	  </footer>
    </div>    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </body>
</html>