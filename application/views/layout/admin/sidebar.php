<!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="<?php echo base_url();?>admin" class="brand-link">
        <img src="<?php echo base_url();?>assets/public/logos/iconoblanco.svg" alt="Ventcar Logo" class="brand-image elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">Ventcar</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?php echo base_url();?>assets/admin/dist/img/perfil-del-usuario.png" class="img-circle elevation-2">
          </div>
          <div class="info">
            <a href="<?php echo base_url();?>admin/perfil" class="d-block"><?php echo $my_datos->NOM; ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->            
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  Principal
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/con" class="nav-link">
                <i class="nav-icon fas fa-user-tie"></i>
                <p>
                  Concesionarios
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/cliente" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  Clientes
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>admin/venta" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                  Ventas
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>admin/perfil" class="nav-link">
                <i class="nav-icon fas fa-cog"></i>
                <p>
                  Configuración
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">