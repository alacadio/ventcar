      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Perfil del vehículo en venta</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Perfil</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <div class="content">
        <div class="container">
          <div class="content-fluid">

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Perfil del carro en venta</h3>
              <div class="card-tools"></div>
            </div>
            <div class="card-body">
              <h4>Propietario actual</h4>
              <hr>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                      <label for="inputDoc">Documento</label>
                      <input type="number" class="form-control" id="inputDoc" placeholder="Documento" value="<?php echo $car->DOC;?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                      <label for="inputNombre">Nombre</label>
                      <input type="text" class="form-control" id="inputNombre" placeholder="Nombre" value="<?php echo $car->NOM;?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                      <label for="inputApellido">Apellido</label>
                      <input type="text" class="form-control" id="inputApellido" placeholder="Apellido" value="<?php echo $car->APE;?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                      <label for="inputTelefono">Telefono</label>
                      <input type="number" class="form-control" id="inputTelefono" placeholder="Telefono" value="<?php echo $car->TEL;?>">
                  </div>
                </div>
              </div>
              <hr>
              <h4>Detalles del vehículo</h4>              
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="inputAño">Año</label>
                    <input type="number" class="form-control" id="inputAño" placeholder="Año" value="<?php echo $car->A;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Marca</label>
                      <input type="text" class="form-control" id="inputMarc" placeholder="Marca" value="<?php echo $car->MA;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputModelo">Modelo</label>
                      <input type="text" class="form-control" id="inputModel" placeholder="Modelo" value="<?php echo $car->MO;?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputVersion">Versión</label>
                      <input type="text" class="form-control" id="inputModeo" placeholder="Modelo" value="<?php echo $car->V;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="inputKilometraje">Kilometraje</label>
                    <input type="number" class="form-control" id="inputKilometraje" placeholder="Kilometraje" value="<?php echo $car->K;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="inputPlaca">Placa</label>
                    <input type="text" class="form-control" id="inputPlaca" placeholder="Placa" value="<?php echo $car->P;?>">
                  </div>
                </div>
              </div>
               <hr>
              <h4>Valor venta</h4>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Valor descrito por el vendedor</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="<?php echo $car->VA;?>">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                      <label for="inputMarc">Menor valor de venta (-20% Aprox)</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="8000000">
                  </div>
                </div>
                <div class="col-sm-4">
                <div class="form-group">
                      <label for="inputMarc">Mejor valor de venta (+10% Aprox)</label>
                      <input type="number" class="form-control" id="inputMarc" placeholder="Marca" value="15000000">
                  </div>
                </div>
              </div>

              <hr>
              <h4>Descripción del vehículo</h4>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Califique el estado de pintura de su vehículo:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->PI){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio1" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Califique el estado interior de su vehículo:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->IN_T){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio2" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Califique el estado mecánico general:</label>
                        <div class="form-check">
                          <?php for ($i=1; $i <= 10; $i++) {
                            $check = '';
                            if($i == $car->ME){
                              $check = 'checked';
                            }
                            echo '<input type="radio" name="radio3" '.$check.' value="'.$i.'">&nbsp;';
                            echo '<label class="form-check-label">'.$i.'</label>&nbsp;';
                          }
                          ?>
                        </div>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Ha tenido accidentes de transito:</label>
                        <div class="form-check">
                          <?php 
                              $var1 = ''; $var2 = '';
                              if($car->ACC == 1){
                                $var1 = 'checked';
                              }else{
                                $var2 = 'checked';
                              }
                          ?>
                          <input type="radio" name="radio6" value="1" <?php echo $var1; ?>>
                          <label class="form-check-label">SI</label>
                          <input type="radio" name="radio6" value="0" <?php echo $var2; ?>>
                          <label class="form-check-label">NO</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Ha tenido choques/topes leves:</label>
                        <div class="form-check">
                          <?php 
                              $var1 = ''; $var2 = '';
                              if($car->CHO == 1){
                                $var1 = 'checked';
                              }else{
                                $var2 = 'checked';
                              }
                          ?>
                          <input type="radio" name="radio4" value="1" <?php echo $var1; ?>>
                          <label class="form-check-label">SI</label>
                          <input type="radio" name="radio4" value="0" <?php echo $var2; ?>>
                          <label class="form-check-label">NO</label>
                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Tipo combustible </label>
                        <div class="form-check">
                          <?php 
                              $var1 = ''; $var2 = '';
                              if($car->COM == 1){
                                $var1 = 'checked';
                              }else{
                                $var2 = 'checked';
                              }
                          ?>
                          <input type="radio" name="radio5" value="1" <?php echo $var1; ?>>
                          <label class="form-check-label">Gasolina</label>
                          <input type="radio" name="radio5" value="0" <?php echo $var2; ?>>
                          <label class="form-check-label">Diesel</label>                          
                          
                          
                        </div>
                    </div>
                </div>
              </div>


              <hr>

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                      <label>Descripcion detallada del estado general del vehiculo</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..."><?php echo $car->DES;?></textarea>
                    </div>
                </div>
              </div>
              
              <hr>
              <center>
                <h4>Imágenes del vehículo</h4>
              </center>
              <hr>
                <div class="row">
                    <?php
                      foreach ($img as $v):
                    ?>
                      <div class="col-sm-2 product-image-thumbs">
                        <div class="post-thumbnail product-image-thumb">
                            <a href="<?php echo base_url().$v->IMG;?>" class="with-caption image-link">
                                <img src="<?php echo base_url().$v->IMG;?>" alt="" width="250px">
                            </a>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
            </div>
          </div>

          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Concesionarias interesadas</h3>
              <div class="card-tools"></div>
            </div>
            <div class="card-body">
              <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>NIT</th>
                  <th>Nombre</th>
                  <th>Razón social</th>
                  <th>Valor Ofertado</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                   <?php foreach ($inte as $v): ?>
                    <tr>
                      <td>  
                        <?php echo $v->NIT; ?>                      
                      </td>
                      <td><?php echo $v->NOM; ?></td>
                      <td><?php echo $v->RA; ?></td>
                      <td><?php echo $v->OF; ?></td>
                      <td><?php echo $v->TEL; ?></td>
                      <td><?php echo $v->EMA; ?></td>
                      <td>
                        <a href="#" class="btn btn-success" onclick="fun_oferta_G('<?php echo $car->ID; ?>','<?php echo $num; ?>','<?php echo base_url(); ?>',1,'<?php echo $v->NOM; ?>')" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-check-square"></i></a>
                        -
                        <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Rechazar oferta" ><i class="fas fa-trash"></i></a>
                        <?php if($v->OP_A == 1):?>
                          <?php if($v->OP_G == 1): ?>
                            <p>Esta oferta fue seleccionada como la opcion de venta</p>
                            -<a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Rechazar oferta" ><i class="fas fa-trash"></i></a>
                          <?php else: ?>
                          <?php endif; ?>
                        <?php endif; ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        </div>
      </div>