var base_url;
$(document).ready(function () {
  base_url = $("#base_url").val();

  $("#example1").DataTable({
    responsive: true,
  });

  $(".select2bs4").select2({
    theme: "bootstrap4",
  });

  fun_get_info_db();

  $("#inputAño").change(function () {
    const año = $("#inputAño").val();
    if (año == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/marca",
      data: { fecha: año },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputMarca").empty();
          $("#inputMarca").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Marca, function (i, item) {
            $("#inputMarca").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputMarca").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputMarca").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/modelo",
      data: { fecha: año, marca: marca },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Modelo, function (i, item) {
            $("#inputModelo").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputModelo").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputModelo").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    const modelo = $("#inputModelo").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/version",
      data: { fecha: año, marca: marca, modelo: modelo },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Version, function (i, item) {
            $("#inputVersion").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputVersion").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputVersion").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    const modelo = $("#inputModelo").val();
    const version = $("#inputVersion").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    if (version == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/valor",
      data: { fecha: año, marca: marca, modelo: modelo, version: version },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          console.log(new Intl.NumberFormat("es-MX").format(data.Valor));
          $("#inputValorpromediodeventa").val(
            new Intl.NumberFormat("es-MX").format(data.Valor)
          );
          $("#inputMenorValorVenta").val(
            new Intl.NumberFormat("es-MX").format(data.menos)
          );
          $("#inputMejorvalordeventa").val(
            new Intl.NumberFormat("es-MX").format(data.mas)
          );
          $("#pr").val(data.Valor);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#reg_us_car").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Registro en proceso",
      text: "Se esta registrando, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Registro",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#act_us_amd").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text:
        "Se está actualizando la información del cliente, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      data: $(this).serialize(),
      success: function (data) {
        console.log(data);
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
            confirmButtonText: "Aceptar",
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  $("#act_pos_cli").submit(function (ev) {
    Swal.fire({
      icon: "info",
      title: "Actualización en proceso",
      text:
        "Se está actualizando la información del vehículo, por favor espere...",
    });
    $.ajax({
      url: $(this).attr("action"),
      type: "post",
      dataType: "json",
      ansyc: true,
      data: new FormData(this), // formData en vez de serialize
      contentType: false, // para que jQuery no setee un contentType
      processData: false, // para que jQuery no transforme data en un string
      success: function (data) {
        if (!data.error) {
          Swal.fire({
            icon: "success",
            title: "Actualización",
            text: data.Mens,
          });
          window.setTimeout("location.reload()", 2000);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
    ev.preventDefault();
  });

  bsCustomFileInput.init();

  
  $(".with-caption").magnificPopup({
    type: "image",
    closeBtnInside: false,
    mainClass: "mfp-with-zoom mfp-img-mobile",

    image: {
      verticalFit: true,
      titleSrc: function (item) {
        var caption = "Ventcar";

        return (
          caption +
          ' <img src="' +
          base_url +
          'assets/public/logos/originalhorizontalblanco.svg" width="50" height="28" />'
        );
      },
    },

    gallery: {
      enabled: true,
    },

    callbacks: {
      open: function () {
        this.wrap.on("click.pinhandler", ".pin-it", function (e) {
          // This part of code doesn't work on CodePen, as it blocks window.open
          // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
          /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);

                    
                    return false;*/
        });
      },
      beforeClose: function () {
        //this.wrap.off('click.pinhandler');
      },
    },
  });

  
  
});

function modal_new_car(id) {
  $("#id").val(id);
  $("#modal_pos_car").modal();
}

function fun_get_info_db() {
  $.ajax({
    type: "GET",
    url: base_url + "admin/import/list",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputVersion")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputMarca")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputModelo")
          .append('<option value="0">- Seleccione una opción -</option>')
          .prop("disabled", true);
        $("#inputAño").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Fecha, function (i, item) {
          $("#inputAño").append(
            "<option value=" + item.F + ">" + item.F + "</option>"
          );
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function modal_act_car_us(id) {
  $("#modal_act_car").modal();
  $.ajax({
    url: base_url + "usuario/car/info",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      if (!data.error) {
        let dato = [data.Mens.A, data.Mens.MA, data.Mens.MO, data.Mens.V];
        let input = ["Marca", "Modelo", "Version"];
        let url = ["list/marca", "list/modelo", "list/version"];
        fun_datos_año(data.Mens.A);
        for (let i = 0; i < 3; i++) {
          fun_set_datos_select(input[i], dato, url[i], i);
        }
        fun_datos_año(data.Mens.A);
        $("#a_inputKilometraje").val(data.Mens.K);
        $("#a_inputPlaca").val(data.Mens.P);

        $("#a_inputValorpromediodeventa").val(data.Mens.VA);
        $("#a_inputMejorvalordeventa").val(data.Mens.VA);
        $("#a_inputValorVenta").val(data.Mens.VA);

        $("#id_v").val(data.Mens.ID);
        $("#a_radio_1_" + data.Mens.PI).prop("checked", true);
        $("#a_radio_2_" + data.Mens.IN_T).prop("checked", true);
        $("#a_radio_3_" + data.Mens.ME).prop("checked", true);
        $("#a_radio_4_" + data.Mens.CHO).prop("checked", true);
        $("#a_radio_5_" + data.Mens.COM).prop("checked", true);
        $("#a_radio_6_" + data.Mens.ACC).prop("checked", true);
        $("#a_inputDesc").val(data.Mens.DES);

        $("#id_img").empty();

        $.each(data.img, function (i, item) {
          $("#id_img").append(
            '<div class="col-sm-2 product-image-thumbs"><div class="post-thumbnail product-image-thumb"><a href="' +
              base_url +
              item.IMG +
              '" class="with-caption image-link"><img src="' +
              base_url +
              item.IMG +
              '" alt="" width="250px"></a></div></div>'
          );
        });

        $(".with-caption").magnificPopup({
          type: "image",
          closeBtnInside: false,
          mainClass: "mfp-with-zoom mfp-img-mobile",

          image: {
            verticalFit: true,
            titleSrc: function (item) {
              var caption = "Ventcar";

              return (
                caption +
                ' <img src="' +
                base_url +
                'assets/public/logos/originalhorizontalblanco.svg" width="50" height="28" />'
              );
            },
          },

          gallery: {
            enabled: true,
          },

          callbacks: {
            open: function () {
              this.wrap.on("click.pinhandler", ".pin-it", function (e) {
                // This part of code doesn't work on CodePen, as it blocks window.open
                // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
                /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);
                            
                                                
                                                return false;*/
              });
            },
            beforeClose: function () {
              //this.wrap.off('click.pinhandler');
            },
          },
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function modal_act_us(id) {
  $.ajax({
    url: base_url + "admin/cons/id",
    type: "post",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      console.log(data);
      if (!data.error) {
        $("#inputDocumento").val(data.Mens.DOC);
        $("#inputNombre").val(data.Mens.NOM);
        $("#inputApellido").val(data.Mens.APE);
        $("#inputCelular").val(data.Mens.TEL);
        $("#inputDireccion").val(data.Mens.DIR);
        $("#inputEmail").val(data.Mens.EMA);
        $("#inputUser").val(data.Mens.US);

        loadJSON(function (response) {
          // Parse JSON string into object
          var JSONFinal = JSON.parse(response);
          $("#inputCiudad").append(
            '<option value="0">- Seleccione una opcion -</option>'
          );
          console.log("LL");
          $.each(JSONFinal, function (dato, obj) {
            $.each(obj.ciudades, function (dato, cid) {
              if (cid === data.Mens.CIUD) {
                $("#inputCiudad").append(
                  '<option value="' + cid + '" selected>' + cid + "</option>"
                );
              } else {
                $("#inputCiudad").append(
                  '<option value="' + cid + '">' + cid + "</option>"
                );
              }
            });
          });
        });

        $("#modal-xl").modal();
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", base_url + "assets/public/js/colombia.json", true); // Reemplaza colombia-json.json con el nombre que le hayas puesto
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function fun_clave() {
  $.ajax({
    url: base_url + "clave",
    type: "post",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputPass").val(data.Mens);
        Swal.fire({
          icon: "info",
          title: "Contraseña generada",
          text:
            "Se ha generado una nueva contraseña, esta es su nueva contraseña por favor cópiela y guárdela en un lugar seguro: " +
            data.Mens,
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}
