
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Ventas realizadas</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">Starter Page</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">     

            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Listado de las ventas realizadas</h3>
                <div class="card-tools">
                </div>
                <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>ID Order</th>
                  <th>Fecha Venta</th>
                  <th>Cliente</th>                  
                  <th>Telefono</th>
                  <th>Email</th>
                  <th>Comprador</th>
                  <th>Telefono</th>
                  <th>Email</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($list_vent as $v): ?>
                    
                      <tr>
                        <td>
                          <?php echo $v->COD; ?>
                        </td>
                        <td><?php echo $v->FECH; ?></td>
                        <td><?php echo $v->CL_NOM.' - '.$v->CL_APE; ?></td>
                        <td><?php echo $v->CL_TEL; ?></td>
                        <td><?php echo $v->CL_EMA; ?></td>
                        <td>
                          <?php echo $v->COMP_NOM; ?>
                        </td>
                        <td><?php echo $v->COMP_TEL; ?></td>
                        <td><?php echo $v->COMP_EMA; ?></td>
                        <td>
                          <a target="framename" href="<?php echo base_url();?>admin/invoice?id=<?php echo $v->COD; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                          -
                          <a href="#" class="btn btn-warning" onclick="modal_cancel_vent('<?php echo $v->COD; ?>')" data-toggle="tooltip" data-placement="top" title="Desactivar Publicación" ><i class="fas fa-window-close"></i></a>
                        </td>
                      </tr>                    
                  <?php endforeach; ?>
                </tbody>
              </table>
                </div>
                <!-- /.card-body -->
            </div>

        </div><!-- /.container-fluid -->
        
      </div>
      
      <!-- /.content -->   

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">     

            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Listado de ventas canceladas</h3>
                <div class="card-tools">
                </div>
                <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-striped">
                <thead>
                <tr>
                  <th>ID Order</th>
                  <th>Fecha Cancelación</th>
                  <th>Cliente</th>
                  <th>Telefono</th>
                  <th>Email</th>
                  <th>Comprador</th>
                  <th>Telefono</th>
                  <th>Email</th>                  
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($list_cancel as $v): ?>
                    
                      <tr>
                        <td>
                          <?php echo $v->COD_V; ?>
                        </td>
                        <td><?php echo $v->FECH; ?></td>
                        <td><?php echo $v->CL_NOM.' - '.$v->CL_APE; ?></td>
                        <td><?php echo $v->CL_TEL; ?></td>
                        <td><?php echo $v->CL_EMA; ?></td>
                        <td>
                          <?php echo $v->COMP_NOM; ?>
                        </td>
                        <td><?php echo $v->COMP_TEL; ?></td>
                        <td><?php echo $v->COMP_EMA; ?></td>
                        <td>
                          <a href="#" onclick="modal_info_cancel(<?php echo $v->COD_V; ?>)" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                        </td>
                      </tr>                    
                  <?php endforeach; ?>
                </tbody>
              </table>
                </div>
                <!-- /.card-body -->
            </div>

        </div><!-- /.container-fluid -->
        
      </div>
      
      <!-- /.content -->  

      <div class="modal fade" id="modal-danger">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Cancelar la venta</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
                <center>
                  <img src="<?php echo base_url();?>assets/public/logos/error.svg" height="150px" alt="" srcset="">
                  <h3>Estas a punto de cancelar esta venta, esta acción no podrá ser desecha.</h3>
                  <h3>¿está seguro de continuar?</h3>
                </center>

            <form action="<?php echo base_url();?>admin/cancelar/venta" id="cancel_vnt" method="post">
                <input type="hidden" name="cod_vt" id="cod_vt">
                <div id="mot">
                    <div class="form-group">
                      <label for="inputMotivo">Motivo de la cancelación</label>
                      <input type="text" class="form-control" name="inputMotivo" id="inputMotivo" placeholder="Motivo" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">            
                <a href="#" onclick="modal_view_mot()" id="btn_init_c" class="btn btn-info">Cancelación</a>
                <button type="submit" id="btn_cancel" class="btn btn-warning">Cancelar</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <div class="modal fade" id="cancel_info">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Información de la cancelación</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <center>
                    <div class="form-group">
                      <label>Fecha cancelación: <p id="fech">12/10/2020</p></label>
                    </div>
                  </center>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="inputVendedor">Vendedor</label>
                    <input type="text" class="form-control" name="inputVendedor" id="inputVendedor" placeholder="Vendedor" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="inputComprador">Comprador</label>
                    <input type="text" class="form-control" name="inputComprador" id="inputComprador" placeholder="Comprador" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <center>
                    <div class="form-group">
                      <label for="inputVendedorC">Contactar vendedor</label>
                      <input type="hidden" name="inputVendedorC" id="inputVendedorC">
                      <a href="#" onclick="fun_contact('1')" class="btn btn-success"data-toggle="tooltip" data-placement="top" title="Contactar" ><i class="fab fa-whatsapp"></i></i></a>
                    </div>
                  </center>
                </div>
                <div class="col-sm-6">
                  <center>
                    <div class="form-group">
                      <label for="inputCompradorc">Contactar comprador</label>
                      <input type="hidden" name="inputCompradorC" id="inputCompradorC">
                      <a href="#" onclick="fun_contact('2')" class="btn btn-success"data-toggle="tooltip" data-placement="top" title="Contactar" ><i class="fab fa-whatsapp"></i></i></a>
                    </div>
                  </center>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Motivo de la cancelación</label>
                    <textarea class="form-control" rows="3" id="mov_cancel" placeholder="Motivo cancelacion"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 

