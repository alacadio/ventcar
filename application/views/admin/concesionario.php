      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Consecionarios</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin">Home</a></li>
                <li class="breadcrumb-item active">consecionario</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">

          <div class="card card-outline card-primary">
              <div class="card-header">
                <h3 class="card-title">Registro de nuevo concesionario</h3>
              </div>
              <div class="card-body">
                <form action="<?php echo base_url(); ?>admin/new_cons" method="post" id="new_cons_amd">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputNIT">NIT*</label>
                                <input type="number" class="form-control" id="inputNIT" name="inputNIT" placeholder="NIT" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputNombreConsecionaria">Nombre Consecionaria*</label>
                                <input type="text" class="form-control" id="inputNombreConsecionaria" name="inputNombreConsecionaria" placeholder="Nombre Consecionaria" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputRazon">Razón Social*</label>
                                <input type="text" class="form-control" id="inputRazon" name="inputRazon" placeholder="Razón Social" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputAsesor">Asesor/Gerente*</label>
                                <input type="text" class="form-control" id="inputAsesor" name="inputAsesor" placeholder="Asesor/Gerente" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputDireccion">Direccion</label>
                                <input type="text" class="form-control" id="inputDireccion" name="inputDireccion" placeholder="Direccion" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputCiudad">Ciudad</label>
                                <input type="text" class="form-control" id="inputCiudad" name="inputCiudad" placeholder="Ciudad" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputTelefono">Celular</label>
                                <input type="number" class="form-control" id="inputTelefono" name="inputTelefono" placeholder="Celular" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputEmail">Email</label>
                                <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.Formulario -->                
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
              </div>
          </div>
          
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h3 class="card-title">Listado de Consecionarios</h3>
              <div class="card-tools">
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-striped">
                <thead>
                <tr>
                  <th>NIT</th>
                  <th>Nombre</th>
                  <th>Razón social</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Fecha Registro</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($list_con as $v): ?>
                      <tr>
                        <td>
                          <?php echo $v->DOC;?>
                        </td>
                        <td><?php echo $v->NOM; ?></td>
                        <td><?php echo $v->RAZON; ?></td>
                        <td><?php echo $v->TEL; ?></td>
                        <td><?php echo $v->EMA; ?></td>
                        <td>
                          <?php echo $v->FECH; ?>
                        </td>
                        <td>
                         <a target="framename" href="<?php echo base_url();?>admin/con/perfil?id=<?php echo $v->DOC;?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" ><i class="fas fa-eye"></i></a>
                        -
                        <a href="#" class="btn btn-warning" onclick="modal_act_cons('<?php echo $v->DOC;?>')" data-toggle="tooltip" data-placement="top" title="Editar" ><i class="fas fa-edit"></i></a>
                        -
                        <?php if($v->EST != 1): ?>
                          <a href="#" class="btn btn-success" onclick="habilitar_us('<?php echo $v->DOC;?>')" data-toggle="tooltip" data-placement="top" title="Habilitar" ><i class="fas fa-check-square"></i></a>
                        <?php else: ?>
                          <a href="#" class="btn btn-danger" onclick="modal_del_cons('<?php echo $v->DOC;?>')" data-toggle="tooltip" data-placement="top" title="Eliminar" ><i class="fas fa-trash-alt"></i></a>
                        <?php endif; ?>
                        </td>
                      </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>

        </div><!-- /.container-fluid -->
        
      </div>      
      <!-- /.content -->



      <div class="modal fade" id="modal_act">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Actualizar información del concesionario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="<?php echo base_url(); ?>admin/cons/act" method="post" id="edt_cons_amd">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputNIT2">NIT*</label>
                                <input type="number" class="form-control" id="inputNIT2" name="inputNIT2" placeholder="NIT" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputNombreConsecionaria2">Nombre Consecionaria*</label>
                                <input type="text" class="form-control" id="inputNombreConsecionaria2" name="inputNombreConsecionaria2" placeholder="Nombre Consecionaria" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputRazon2">Razón Social*</label>
                                <input type="text" class="form-control" id="inputRazon2" name="inputRazon2" placeholder="Razón Social" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputAsesor2">Asesor/Gerente*</label>
                                <input type="text" class="form-control" id="inputAsesor2" name="inputAsesor2" placeholder="Asesor/Gerente" required value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputDireccion2">Direccion</label>
                                <input type="text" class="form-control" id="inputDireccion2" name="inputDireccion2" placeholder="Direccion" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputCiudad2">Ciudad</label>
                                <input type="text" class="form-control" id="inputCiudad2" name="inputCiudad2" placeholder="Ciudad" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputTelefono2">Celular</label>
                                <input type="number" class="form-control" id="inputTelefono2" name="inputTelefono2" placeholder="Celular" required value="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputEmail2">Email</label>
                                <input type="email" class="form-control" id="inputEmail2" name="inputEmail2" placeholder="Email" required value="">
                            </div>
                        </div>
                    </div>
                    <!-- /.Formulario -->                
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal-danger">
        <div class="modal-dialog modal-xl">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar o desactivar usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <input type="hidden" name="id_con" id="id_con">
              </button>
            </div>
            <div class="modal-body">
              <h4>Podrá eliminar o desactivar un concesionario en específico, solo tenga en cuenta que:</h4>
              <ul>
                <li>
                    <p>Si da click en eliminar, los datos del concesionario, así como sus registros quedaran permanentemente eliminados y esta acción no podrá deshacerse</p>
                </li>
                <li>
                    <p>Si da click en deshabilitar, los datos se mantendrán solo el concesionario no podrá ingresar nuevamente hasta ser habilitado nuevamente</p>
                </li>
              </ul>
              <a href="#" onclick="fun_del_cons(1)" class="btn btn-danger">Eliminar</a>
              <a href="#" onclick="fun_del_cons(2)" class="btn btn-danger">Deshabilitar</a>
            </div>
            <div class="modal-footer justify-content-between">            
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->