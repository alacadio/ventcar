var base_url;

$(document).ready(function () {
  base_url = $("#base_url").val();

  $('[data-toggle="tooltip"]').tooltip();

  $(".with-caption").magnificPopup({
    type: "image",
    closeBtnInside: false,
    mainClass: "mfp-with-zoom mfp-img-mobile",

    image: {
      verticalFit: true,
      titleSrc: function (item) {
        var caption = "Ventcar";

        return (
          caption +
          ' <img src="' +
          base_url +
          'assets/public/logos/originalhorizontalblanco.svg" width="50" height="28" />'
        );
      },
    },

    gallery: {
      enabled: true,
    },

    callbacks: {
      open: function () {
        this.wrap.on("click.pinhandler", ".pin-it", function (e) {
          // This part of code doesn't work on CodePen, as it blocks window.open
          // Uncomment it on your production site, it opens a window via JavaScript, instead of new page
          /*window.open(e.currentTarget.href, "intent", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 50) + ",top=" + 100);

                    
                    return false;*/
        });
      },
      beforeClose: function () {
        //this.wrap.off('click.pinhandler');
      },
    },
  });

  $(".select2").select2();

  $(".select2bs4").select2({
    theme: "bootstrap4",
  });

  fun_ofer(base_url);

  fun_get_info_db();

  $("#inputAño").change(function () {
    const año = $("#inputAño").val();
    if (año == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/marca",
      data: { fecha: año },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputMarca").empty();
          $("#inputMarca").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Marca, function (i, item) {
            var marca = item.F;
            $("#inputMarca").append(
              '<option value="' + marca + '">' + marca + "</option>"
            );
          });
          $("#inputMarca").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputMarca").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/modelo",
      data: { fecha: año, marca: marca },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputModelo").empty();
          $("#inputModelo").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Modelo, function (i, item) {
            $("#inputModelo").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputModelo").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  $("#inputModelo").change(function () {
    const año = $("#inputAño").val();
    const marca = $("#inputMarca").val();
    const modelo = $("#inputModelo").val();
    if (año == 0) {
      return;
    }
    if (marca == 0) {
      return;
    }
    if (modelo == 0) {
      return;
    }
    $.ajax({
      type: "GET",
      url: base_url + "list/version",
      data: { fecha: año, marca: marca, modelo: modelo },
      dataType: "json",
      success: function (data) {
        if (!data.error) {
          $("#inputVersion").empty();
          $("#inputVersion").append(
            '<option value="0">- Seleccione una opción -</option>'
          );
          $.each(data.Version, function (i, item) {
            $("#inputVersion").append(
              '<option value="' + item.F + '">' + item.F + "</option>"
            );
          });
          $("#inputVersion").prop("disabled", false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: data.Mens,
          });
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  });

  const formatterPeso = new Intl.NumberFormat("es-CO", {
    style: "currency",
    currency: "COP",
    minimumFractionDigits: 0,
  });
  var pes = $("#_val").val();
  console.log(formatterPeso.format(pes));
  $("#cop").text(formatterPeso.format(pes));

  bsCustomFileInput.init();
});

function modal_oferta() {
  $("#modal-lg").modal();
}

function fun_ofer(url) {
  const est = $("#est").val();
  if (est > 0) {
    $.ajax({
      url: url + "vehiculo/detalles/ofer",
      type: "post",
      dataType: "json",
      data: { id: est },
      success: function (data) {
        if (data.error) {
          if (data.Mens) {
            $("#inputValor").val(data.Mens.OF);
          } else {
            $("#inputValor").val(0);
            console.log("Entro");
          }
          $("#text_oferta").text(data.EST);
        }
      },
      error: function () {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      },
    });
  }
}

function fun_ofertar() {
  const valor = $("#inputValor").val();
  const id = $("#est").val();
  if (valor <= 0) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text:
        "Disculpe, pero la operación no puede ser ejecutada por que el valor de la oferta no puede ser menor o igual a 0, por favor ingrese un nuevo valor",
    });
    return;
  }

  $.ajax({
    url: base_url + "vehiculo/ofertar",
    type: "post",
    dataType: "json",
    data: { id: id, val: valor },
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "Oferta Valida",
          text: data.Mens,
        });
        window.setTimeout("location.reload()", 2000);
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text:
            "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_oferta_G(id, tel, url, e, nom) {
  var mens;
  url += "admin/con/car/perfil?id=" + id;
  if (e == 1) {
    mens =
      "Hola ventcar, he decidido aceptar la oferta ofrecida por el concesionario " +
      nom +
      " al vehículo " +
      url +
      ", por favor comuníquese con mi persona por este medio para ultimar detalles";
  } else if (e == 2) {
    mens =
      "¡¡¡Felicidades!!!, su oferta realizada al vehículo " +
      url +
      ", fue aceptada por el vendedor del vehículo, por favor comuníquese con nosotros por este medio para ultimar detalles";
  } else if (e == 3) {
    mens =
      "Hola ventcar, este vehículo " +
      url +
      " me interesa, ¡¡Agendemos una cita para checar el vehículo!!";
  }
  var whatsapp =
    "https://api.whatsapp.com/send?phone=" + 57 + tel + "&text=" + mens;
  window.open(whatsapp, "_blank");
  fun_noti_db(id);
  return false;
}

function fun_noti_db(id) {
  $.ajax({
    type: "GET",
    url: base_url + "admin/registro/notificacion",
    data: { id: id },
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        Swal.fire({
          icon: "success",
          title: "¡¡¡Notificado!!!",
          text: data.Mens,
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}

function fun_get_info_db() {
  $.ajax({
    type: "GET",
    url: base_url + "list/marca/index",
    dataType: "json",
    success: function (data) {
      if (!data.error) {
        $("#inputMarca").append(
          '<option value="0">- Seleccione una opción -</option>'
        );
        $.each(data.Mens, function (i, item) {
          console.log(item.M);
          $("#inputMarca").append(
            "<option value=" + item.M + ">" + item.M + "</option>"
          );
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: data.Mens,
        });
      }
    },
    error: function () {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text:
          "Disculpe exitio un error de servidor al enviar los datos, intente mas tarde.",
      });
    },
  });
}
